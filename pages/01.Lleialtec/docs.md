---
media_order: lleialtec-mapa-2.png
taxonomy:
    category:
        - docs
child_type: docs
visible: true
---

### Pla de Sobirania Tecnològica per l'acció comunitària

#### Qui som i què fem? 

Lleialtec és la comissió de tecnologia de la [Coordinadora d'Entitats per la Lleialtat Santsenca](https://www.lleialtat.cat/coordinadora/) (CELS) i neix amb l'objectiu de vetllar per l'apoderament digital de les persones, col·lectius i entitats del barri. 

* **Proposem** a l'assemblea de la CELS eines tecnològiques lliures i ètiques per gestionar l'equipament, comunicar-nos, crear contingut, emmagatzemar dades...
* **Implementem** la infraestructura tecnològica a l'equipament de La Lleialtat i acompanyem als  membres de la CELS en l'adopció de les eines que consensuem implementar
* **Documentem** la feina feta per tal que el projecte sigui replicable en altres espais comunitaris

#### Per què ho fem?

Ens adonem que, ara per ara, la Lleialtec és la comissió més transversal de la CELS, ja que totes les comissions, -en major o menor mesura-, tenen necessitats tecnològiques. A més, detectem que, municipals o no, **tots els espais d'acció comunitària tenen necessitats semblants**: publicació de les d'activitats, inscripcions, reserva de sales i conseqüent facturació, comunicació interna i externa, entre d'altres. 

I, tot i que observem que **els perfils professionals dels equipaments encara no contemplen cap figura que gestioni, -de forma integral-, les necessitats tecnològiques**, creiem fermament que a tots els espais comunitaris hi hauria d'haver personal especialitzat per planificar, implementar i formar. Però, en general, no hi ha massa consciència digital i es fan servir eines privatives de forma sistemàtica pel simple fet de ser gratuïtes o populars. 

Es diu que _«si és gratis, el producte ets tu...»_ i és cert en funció del model de negoci del producte o servei. Facebook seria una eina falsament gratuïta perquè l'activitat que generen les usuàries beneficia a l'empresa, tant que cotitza en borsa. Però la gran majoria de programari lliure, a més de transparent, és gratuït de debò: ni es paga amb diners ni es paga amb dades, és un bé comú gràcies a la seva llicència. I només cal aprendre a instal·lar-lo i usar-lo. Conclusió: _«si és gratis però no un bé comú, molt probablement el producte siguis tu»._

A la jungla digital, protegir és protegir-se, i en **la vetlla per la salut digital i la lluita contra el capitalisme cognitiu**, veiem pràctiques del segle XXI que podríem anomenar «Tecnologia de les cures». I és per això, per protegir les veïnes de pesticides tecnològics, per contribuir a una societat digital més lliure i ètica que, des de Lleialtec, proposem i implementem aquest Pla de Sobirania Tecnològica per l'Acció Comunitària.

#### Com ho fem?

Divulgant i formant; implementant infraestructures lliures i documentant-les per a la  seva replicabilitat:

* **Divulgació**, per fer comprensible a la ciutadania la importància, al segle XXI, de les tecnologies lliures, ètiques i descentralitzades
* **Formació**, per tal fomentar l'alfabetització digital i l'aprenentatge al llarg de la vida
* **Infraestructura**, per vetllar per la Sobirania Tecnològica als barris i ser propietàries de les nostres dades i xarxes
* **Documentació**, per possibilitar la rèplica i la millora de la feina feta, apoderant així a altres espais i projectes
