---
title: 'Diuen, diuen...'
taxonomy:
    category:
        - docs
visible: true
---

Recull de notícies on se'ns menciona, es parla de nosaltres, o fem entrevistes:

#### 2019

* ["La Lleialtat Santsenca crea una Snap!Arcade"](https://beteve.cat/ciencia-i-tecnologia/maquina-snap-arcade-programari-lliure-lleialtat-santsenca/). Notícia del projecte a la web de Betevé Sants-Montjuïc. Publicat a l'octubre de 2019

* [«Kill Bill Gates»](https://autodefensainformatica.radioalmaina.org/autodefensa-informatica-34-kill-bill-gates/). Entre altres mini-killers, menció a la Lleialtec (32:50-33:43) a Autodefensa Informática, un programa de ràdio Almaina (Granada). Publicat al setembre de 2019

* [La Lleialtat a la fresca –especial Festa Major de Sants](https://sants.org/2019/08/30/la-lleialtat-a-la-fresca-especial-festa-major-de-sants/). Notícia de la presentació de la Snap!Arcade durant les festes majors de Sants. Publicat a l'agost de 2019

* ["Formació en tecnologies lliures i ètiques"](https://www.barcelona.cat/barcelonaciencia/ca/noticia/formacio-en-tecnologies-lliures-i-etiques_832589). Notícia a la web de l'Ajuntament relacionant les formacions d'estiu amb la iniciativa "B4rcelona Ci3nc1a". El mateix text surt també a la web de l'Ajuntament, a la [secció de Districte Sants-Montjuïc](https://ajuntament.barcelona.cat/sants-montjuic/ca/noticia/formacio-en-tecnologies-lliures-i-etiques_832589?hootPostID=afa870a6a8e3f113789e5700d0a6fb26). Juny de 2019

Per promocionar les formacions d'estiu, vam proposar un article titutat ["La Lleialtat Santsenca, un equipament lliure en consolidació"](http://documents.lleialtat.cat/LleialTec/Impulsem-2018/bcn-digital-19.pdf) per al blog de la Barcelona Digital. I no sabem ben bé perquè però no es va publicar

* ["El programari és intangible però també l'hauríem de pensar en termes de consum responsable"](http://punttic.gencat.cat/article/el-programari-es-intangible-pero-tambe-lhauriem-de). Entrevista a la Xarxa Punt TIC de la Generalitat de Catalunya. Publicat al maig de 2019

* ["#BerenaTec"](https://issuu.com/laburxa/docs/laburxa_226). A La Burxa. Article publicat a l'abril de 2019

#### 2018
* ["Introducció al programari lliure i a la tecnologia feminista"](https://ajuntament.barcelona.cat/centrescivics/ca/activitat/introduccio-al-programari-lliure-i-la-tecnologia-feminista). A la web de l'Ajuntament de Barcelona. Notícia de la xerrada emmarcada en el projecte "La ciutat de les dones" de la Biennal de pensament "Ciutat Oberta". En col·laboració amb Dones Tech. Novembre de 2018

* ["LleialTEC, cas d'estudi"](https://digitalsocial.eu/ca/case-study/56/lleialtec). A la web de "Innovació Social Digital", programa europeu en el marc de l'Horizon2020. Juliol de 2018

* ["La Lleialtat, un equipament lliure"](https://www.laburxa.org/2018/06/la-lleialtat-un-equipament-lliure/). A La Burxa. Article publicat al juny de 2018

* ["Un futur comú en programació"](https://directa.cat/un-futur-comu-en-programacio/). Article a La Directa. Publicat al maig de 2018

* ["Lleialtec: vetllant per l'apoderament digital de les persones i col·lectius del barri de Sants"](https://ajuntament.barcelona.cat/digital/ca/blog/el-lleialtec-vetllant-per-lapoderament-digital-de-les-persones-i-collectius-del-barri-de-sants). Al blog de la Barcelona Digital de l'Ajuntament. Publicat al maig de 2018