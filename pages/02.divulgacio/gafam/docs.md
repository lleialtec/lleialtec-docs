---
title: GAFAM
taxonomy:
    category:
        - docs
visible: true
---

Al novembre de 2017, l'associació francesa [La Quadrature du net](https://www.laquadrature.net/), va emprendre una campanya per visibilitzar els despropòsits de les grans empreses tecnològiques vers les seves usuàries. En traduïm el text:


#### Acció de grup contra les GAFAM


Les GAFAM (Google, Apple, Facebook, Amazon, Microsoft) ens fan **pagar els seus serveis amb les nostres llibertats**.

La nostra **llibertat de consciència**, les deixa accedir als detalls del nostre esperit per manipular-nos de manera individualitzada i automatitzada. La nostra **vida privada** i la nostra intimitat, sense la qual ja no podem construir-nos a nosaltres mateixes. 

Aquest contracte és **il·lícit**: en democràcia, ningú vol vendre les seves llibertats fonamentals. Fem que, a partir d'ara, la llei prohibeixi que un servei sigui remunerat amb **dades personals**. 

Per recuperar les nostres llibertats, el **25 de maig**, La Quadrature du Net ha iniciat **accions col·lectives** amb 12.000 persones contra cadascuna de les GAFAM.

##### Google    
[Text](http://documents.lleialtat.cat/LleialTec/GAFAM/gafam1-google.pdf) traduït al català i difós al [berenatec](https://tec.lleialtat.cat/divulgacio/berenatec) de gener 2019

##### Apple    
[Text](http://documents.lleialtat.cat/LleialTec/GAFAM/gafam2-apple.pdf) traduït al català i difós al [berenatec](https://tec.lleialtat.cat/divulgacio/berenatec) de febrer 2019

##### Facebook    
[Text](http://documents.lleialtat.cat/LleialTec/GAFAM/gafam3-facebook.pdf) traduït al català i difós al [berenatec](https://tec.lleialtat.cat/divulgacio/berenatec) de març 2019

##### Microsoft
[Text](http://documents.lleialtat.cat/LleialTec/GAFAM/gafam5-microsoft.pdf) traduït al català per [xaloc](https://fedi.xaloc.space/users/xaloc) i difós al [berenatec](https://tec.lleialtat.cat/divulgacio/berenatec) d'abril 2019


#### Cronologia de la campanya

* **Novembre de 2017**, La Quadrature du Net inicia la campanya anti-GAFAM 
* **Gener de 2018**, participants del 34c3 munten la web [GAFAM info](https://www.gafam.info/) per donar suport al projecte: es crea [un repositori per facilitar la traducció](https://github.com/gafam/gafam-poster-translations) dels pòsters, i un [subdomini per a trobar-los i imprimir-los fàcilment](https://library.gafam.info/)
* **Febrer de 2018**, es possibilita l'edició online dels pòsters a [GAFAM on Weblate](https://hosted.weblate.org/projects/gafam/translations/)
* **Abril de 2018**, La Quadrature du Net inicia una [acció de grup contra les GAFAM](gafam.laquadrature.net) on analitza cadascuna de les empreses
* **Maig de 2018**, es presenta la [denúncia col·lectiva](https://www.laquadrature.net/2018/05/28/depot_plainte_gafam/) que firmen 12.000 persones. I els pòsters ja s'han traduït a 16 idiomes
* **Octubre de 2018**, [actualització](https://www.laquadrature.net/2018/10/10/nos-plaintes-contres-les-gafam-avancent/) sobre la denúncia col·lectiva 
* **Novembre de 2018**, [campanya anual de recollida de fons]( https://www.laquadrature.net/2018/11/15/soutenons-notre-internet/) per seguir lluitant contra les GAFAM. I publicació d'un [vídeo divulgatiu](https://video.lqdn.fr/videos/watch/dd3db1ae-f7a9-41cb-8db5-c8371977b880). Es poden traduir els subtítols en [aquest pad](https://pad.lqdn.fr/p/SRT_soutenons_notre_internet)


---8<------

[**Traducció**](http://documents.lleialtat.cat/LleialTec/GAFAM/gafam-quadrature-a4.pdf) al català: LleialTec, comissió tecnològica de La Lleialtat Santsenca

[**Text original**](https://gafam.laquadrature.net/) en francès 

[**Cartells**]( https://ptrace.gafam.info/unofficial/pdf/black/lqdn-gafam-poster-ca-black.pdf) en català

**Cronologia** d'elaboració pròpia a partir de les dades publicades a la pàgina [GAFAM info](https://www.gafam.info/), la pàgina de la [campanya](https://gafam.laquadrature.net/) i la de [La Quadrature](https://www.laquadrature.net/)

---8<------



