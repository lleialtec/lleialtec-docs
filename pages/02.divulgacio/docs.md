---
title: Divulgació
taxonomy:
    category:
        - docs
child_type: docs
visible: true
---

Som conscients que les persones no donem valor a les coses que no entenem o amb les que no estem familiaritzades. I tot i que la majoria de persones usem dispositius digitals i, per tant, moltíssim programari, **poques són les persones que reflexionen sobre perquè fan servir un programari o altre, o sobre qui l'ha fet i perquè**, o sobre com pot donar tants diners una cosa que és «gratis»...

En aquest primerenc «boom digital» de principis de segle, trobem que el que s'incrementen són els dispositius i els programes, però no la consciència digital de les persones que en fan ús. És per això que, de tant en tant, proposem **taules obertes on tractar temes concrets**.

A les [portes obertes](http://documents.lleialtat.cat/Imatges/Jornades/Portes%20obertes%202017/Diptic_Lleialtat.pdf), en vam dinamitzar dues: una sobre el propi model tecnològic de la Lleialtat; i l'altra, sobre les connexions entre el la comunitat del programari lliure i les iniciatives de l'Economia Social i Solidària (ESS). I, durant novembre i desembre, en vam dinamitzar dues més: una sobre tecnologia Blockchain i una altra sobre programari lliure i ESS.

Arrel del projecte [Equipaments Lliures](https://tec.lleialtat.cat/equipaments-lliures), hem iniciat els [BerenaTec](https://tec.lleialtat.cat/divulgacio/berenatec), berenars divulgatius on, amb tec i teca, es convida a les veïnes a conèixer tecnologies lliures i privatives, i debatre sobre sobirania tecnòlogica. 
