---
title: '[BerenaTec}'
taxonomy:
    category:
        - docs
visible: true
---

**De desembre 2018 a maig 2019**, -i dins del projecte [Equipaments Lliures](https://tec.lleialtat.cat/equipaments-lliures)-, estem duent a terme els **[#BerenaTec](https://santsenques.cat/tag/berenatec)**. 

Els berenars tecnològics consisteixen en trobades informals que serveixen per apropar al veïnat **conceptes i projectes de sobirania tecnològica**, a més de continguts sobre els abusos de les grans tecnològiques. D'una banda, volem fer visibles iniciatives relacionades amb la sobirania tecnològica: xarxes socials ètiques, drets i economies digitals, eines alternatives a les de cibervigilància, cultura lliure, etc. A cada berenar, presentarem alguna d'aquestes temàtiques.

* Consulta el [cartell genèric](http://documents.lleialtat.cat/LleialTec/BerenaTec/berenatec-vertical-0-lleuger.png) dels berenars
* Consulta el fulletó informatiu dels berenars. [Anvers](http://documents.lleialtat.cat/LleialTec/BerenaTec/berenatec-anvers-lleuger.png) del fulletó; [revers](http://documents.lleialtat.cat/LleialTec/BerenaTec/berenatec-revers-lleuger.png) del fulletó
* Consulta l'[entrada al blog](https://www.lleialtat.cat/berenatec-%e2%94%80trobades-de-sobirania-tecnologica-a-lhora-del-te/) de La Lleialtat

D'altra banda, i per tal de copsar la importància de la sobirania tecnològica, volem posar en evidència els **abusos de les GAFAM** (Google, Apple, Facebook, Amazon, Microsoft). És per això, que a cada berenar, també presentem [continguts traduïts al català](https://tec.lleialtat.cat/divulgacio/gafam) del projecte francès La Quadrature du net. 

Els berenars es duen a terme de desembre del 2018 a maig del 2019, **cada primer dimecres de mes, de 17h a 19h, a [la Lleialtat Santsenca](http://lleialtat.cat/)**. Excepcionalment, el berenar de gener no és farà el dimecres 2 sinó el 9; i el de maig, no es farà l'1 sinó el 8. 

Us hi esperem a totes i tots!


##### BerenaTec 2018-2019:
**Dimecres 8 de maig de 2019: "Drets digitals, drets humans"**. A càrrec de ~~[Drets Digitals](http://www.dretsdigitals.cat/)~~  Al final, Drets digitals no podrà venir: des de Lleialtec proposem  documental sobre discriminació algorítmica i un debat sobre els nostres drets digitals.
* [Cartell](http://documents.lleialtat.cat/LleialTec/BerenaTec/berenatec-vertical-6-drets-digitals-lleuger.png)
* Contingut (properament)

**Dimecres 3 d'abril de 2019: "Cibervigilància i alternatives lliures"**. A càrrec de [Surt del Cercle](http://www.surtdelcercle.cat/)  
* [Cartell](http://documents.lleialtat.cat/LleialTec/BerenaTec/berenatec-vertical-5-surtdelcercle-lleuger.png)
* [Contingut](https://surtdelcercle.cat/presentacions/berenartec2019/)

**Dimecres 6 de març de 2019: "Economia Social i Solidària i Programari Lliure"**. A càrrec de [Lleialtec](https://tec.lleialtat.cat/)
* [Cartell](http://documents.lleialtat.cat/LleialTec/BerenaTec/berenatec-vertical-4-ess-pl-lleuger.png)
* [Contingut](http://documents.lleialtat.cat/LleialTec/BerenaTec/presentacio-mst-berenatec.pdf) 

**Dimecres 6 de febrer de 2019: "Apoderament digital municipal"**. A càrrec de [Apoderament Digital](http://apoderamentdigital.cat/) 
* [Cartell](http://documents.lleialtat.cat/LleialTec/BerenaTec/berenatec-vertical-3-apoderament-lleuger.png)
* [Contingut](http://apoderamentdigital.cat/)

**Dimecres 9 de gener de 2019: "Eines lliures per al bé comú"**. A càrrec de [Komun](http://komun.org/) i matrocinat per [DonesTech](http://donestech.net/) 
* [Cartell](http://documents.lleialtat.cat/LleialTec/BerenaTec/berenatec-vertical-2-komun-lleuger.png)
* Contingut 
* Més info a [Komun](https://komun.org/ca/blog/presentacion-de-komun-en-barcelona), a [Surt del Cercle](https://surtdelcercle.cat/index.php?view=blog&id=40) i a [la Confederació](https://la.confederac.io/t/2on-berenar-tec-a-la-lleialtat-santsenca/25355)

**Dimecres 5 de desembre de 2018: "Xarxes socials lliures i ètiques"**. A càrrec de [Lleialtec](https://tec.lleialtat.cat/) i matrocinat per [DonesTech](http://donestech.net/) 
* [Cartell](http://documents.lleialtat.cat/LleialTec/BerenaTec/berenatec-vertical-1-fediverse-lleuger.png)
* [Contingut](https://tec.lleialtat.cat/divulgacio/fediverse)
