---
title: 'Seguretat digital'
published: false
taxonomy:
    category:
        - docs
visible: true
---

![](seguretat-digital.png)

### Context

En el context de la tecnologització de la vida quasi totes nostres comunicacions i interaccions estan mitjançades per les màquines. 
Hem aconseguit poder parlar més ràpid, amb més gent i independentment de la distància. Totes les extenses infraestructures que fan possible la vida quotidiana d'una societat tan complexa com la nostra estan regides per les tecnologies de la informació. Ens agradi més o menys, la majoria d'aquestes infraestructures dependen de les empreses privades.

En  aquest context és cada cop més important prendre consciència dels processos globals així com del funcionament més detallat sobre les nostres activitats digitals i els dispositius que utilitzem. Quines són? Com funcionen? Qui les construeix? I si les meves dades estan al núvol, on esta això?

La seguretat digital és un camp de recerca i pràctica que es dedica a la protecció de les nostres comunicacions i continguts amb el fi de garantir la privadesa als individus o grups, així com la sobirania tecnològica a nivell global.

Normalment parlem de la protecció de dades, que són tots els continguts que fiquem dins dels dispositius, com per exemple textos, fotos, comentaris, converses, contactes, etc. Però també hem de parlar de les metadades, que són tota la informació sobre les nostres dades com el moment, la duració, tipus i detecció del dispositiu, geolocalització, remitent, destinatari... Les metadades són les que contextualitzen les dades dins d'una realitat concreta i identificable.

### Objectius
* protecció de dades
  * emmagatzemades
  * en moviment
  * en execució / processament
* privadesa, sensació de seguretat, protecció, llibertat
* sobirania tecnologica
* elements a protegir :
  * dades
  * metadades


### Seguretat holística:
* seguretat digital
* seguretat física
* seguretat psico-social 
  * balanç positiu entre les amenaçes externes percebudes i la capapacitat (coneixement i estrategies) de defensa
  * es totalment subjectiu
  * dinámic - canvia en el temps - prioritzem diferent les necessitats segons quin moment
  * la sensació de seguretat no es un valor total, sino varia segons el ámbit (podem tenir seguredad economica, pero no afectiva...)
  * impacte psicologic: en moments d'amenaça o crisi (pot ser després d'una vulneració de privacitat) no som capaços de prendre bones decisions; millior prevenir que mitigar
  * impacte social: falta de percepció positiva de la seguretat/privadesa al nivell social pot portar a conductes d'autocensura, rupturas del teixit col·lectiu, projecció d'agressivitat a l'interior del grup (crear "enemics interns")

### Com abordar aquest camp

La informació dels riscos que comporten les tecnologies de vegades es presenten amb un to fatalista, sense cap escapatòria, i les possibles solucions, quan s'esmenten, solen sobreestimar-se pel que fa a la facilitat de fer el canvi. De la mateixa manera, en molts tallers de seguretat o "criptoparties", la relació és gairebé mèdica: vostè té aquesta malaltia (vigilància) i per guarir-se ha de seguir aquesta recepta (eines). Però altre cop, aquestes receptes, o de vegades simplement llistes, són impossilbes de seguir individualment i sense més suport. I això genera una sensació de por per ser vulnerable als riscos, i de culpabilitat per no fer prou per protegir-se'n.

Davant aquesta por, típicament trobem dues reaccions:
* Bloqueig: no em puc protegir, millor no faig res, millor m'adapto a la norma.
* Temeritat: no em puc protegir, així que ignoro els riscos i m'exposo totalment.

Cap de les dues reaccions és desitjable. Per molt que a algú li pugui semblar una reacció valenta, la temeritat té data de caducitat, sigui per patir els danys de les amenaces o per esgotament. Apostem per una tercera opció:
* Anàlisi: no estic protegit/da, però puc canviar aquesta situació pas a pas.

Així, analitzant l'entorn, descobrint riscos i prioritzant els canvis, podem aconseguir, pas a pas, millorar la nostra situació. Pas a pas, perquè ningú pot canviar els seus hàbits d'un dia per l'altre, i prioritzant, per començar maximitzant els resultats. A part, si busquem suport en allò coŀlectiu, aquests processos de canvi es poden fer molt més lleugers i profitosos: compartint dubtes, experiències, investigant plegades i animant-nos a continuar.

### Barreres i contraarguments

Davant de la informació sobre la dominació a través de la tecnologia, dels monopolis, de la deriva centralitzadora, i, en general, del panorama distòpic en què fa temps que estem immerses, moltíssimes persones responen amb arguments gravats al seu subconscient per evitar fer-hi res. Alguns dels arguments més repetits són:

> No tinc res a amagar

> Jo no sóc interessant, no sóc ningú

_( negació, autocensura, menyspreu a un mateix )_

> Ja ho saben tot de mi

_( nihilisme, derrota, renúncia a la llibertat )_

Podem respondre a aquestes sentències des de dos punts de vista: en negatiu, en el sentit de «si renuncies, mira què passa», i en positiu, en sentit de «mira què hi tenim a guanyar»

#### Motius en negatiu

En la dimensió individual, les conseqüències poden ser moltes:
1. Cap al futur, a mans de qui poden acabar les teves dades? Imagina que els governs en qui havies confiat prenen una deriva totalitària. Imagina que Google canvia de mans, de president, acumula massa poder, o que un govern feixista el sotmet a canvi de més quota de mercat. "Imagina".
2. Les deteccions de delinqüents per anàlisi de dades, d'aquells que suposadament sí que tenen alguna cosa a amagar, falla poc, però falla. Veure "paradoxa dels falsos positius en [castellà](https://es.wikipedia.org/wiki/Paradoja_del_falso_positivo) o en [anglès](https://en.wikipedia.org/wiki/Base_rate_fallacy#Example_2:_Terrorist_identification)"
3. Com deia Snowden «dir que no m'importa el dret a la privacitat perquè no tinc res a amagar, és com dir que no m'importa la llibertat d'expressió perquè no tinc res a dir». I és que sempre tindrem alguna cosa a dir, o voldrem conservar el dret d'expressar-nos. De la mateixa manera, sempre hi haurà coses que preferirem mantenir privades i tenir aquesta possibilitat en el futur.
4. Si ja ho saben tot de tu, és sobre el teu «jo» fins el dia d'avui. Pots continuar regalant anys de detalls de la teva intimitat i identitat a entitats en qui no confies, o bé pots començar avui mateix a recuperar terreny i guanyar llibertat.
5. De qui no tens res a amagar? Ho mostraries voluntàriament tot a la teva família? Als teus veïns? Companys de feina... o superiors? A qualsevol que passés pel carrer? Als lladres comuns? A les forces de l'ordre o a governs autoritaris? A multinacionals monopolistes? Ben mirat, probablement només compartiries «qualsevol cosa» amb algú amb qui tinguessis total confiança, i justament aquestes persones mai t'ho exigiran ni et faran xantatge si és que respecten la teva autonomia. Qui s'alimenta de les nostres dades ni ens respecta com a iguals, ni confia en nosaltres (ni mereix la nostra), ni ens vol deixar escollir.
6. Si no tens res a amagar perquè no ets culpable de res, llavors tampoc no tens res a ensenyar, ni ningú té dret de violar la teva intimitat!
7. El fet de saber-se vigilat indueix actituds submises a la norma de qui vigila. Genera el que entenem com autocensura, ens convertim en la nostra pròpia vigilant. De la mateixa manera vigilem les persones que ens envolten, estenent així una xarxa de vigilància mantinguda per la por. Per exemple: «no pengis això al Facebook, que ho veuen tot!». Això genera [espirals de silenci](https://ca.wikipedia.org/wiki/Espiral_del_silenci), on totes les opinions són silenciades excepte les que defensen al poder. Com a resutat, se silencien les minories, dissidències, i amb elles totes les opcions de canvi i millora. En canvi, no seria millor fer servir plataformes on poguéssim compartir el que vulguéssim amb exclusivament les persones que vulguéssim? «Ostres, si ho veiessin això els corruptes, farien una llei per prohibir-ho!». Un debat públic perquè sigui just s'ha de poder tenir en privat i en seguretat. Consultar també la teoria del panòptic de Foucalt:
    * [Des de la psicologia, en castellà](https://psicologiaymente.net/social/teoria-panoptico-michel-foucault)
    * [Aplicat a la societat digital, a The Guardian, i en anglès](https://www.theguardian.com/technology/2015/jul/23/panopticon-digital-surveillance-jeremy-bentham)
    * [Explicant la teoria i l'article anterior, a Taringa, en castellà](https://www.taringa.net/posts/reviews/20093546/Ser-tu-propio-carcelero-La-teoria-del-Panoptico-digital.html)

* dimensió coŀlectiva
     * co-responsabilitat: activistes, periodistes, descendència
     * co-responsabilitat: monopolis
          * contra la lliure competència
          * la info es poder, i estem alimentant la bèstia
          * coŀlaboren amb NSA i cia (no pun intended hahah)
          * Capitalisme de la Vigilancia, sistema economic basat en la feina no remunerada dels "usuaris", la mercancia son les seves dades, els clients: les empreses de publicitat, etc
          * big data: neuromarketing, psicografia i "democràcia"
          * modelar les subjectivitats, engenieria social
    
#### Motius en positiu
   * La privacitat habilita:
       * llibertat de pensament (critic). Amb una opinio pública homogènia i omnipresent, potser no sempre voldràs que tothom ho sàpiga tot de tu, potser no vols explicar algunes coses aixi d'entrada a tothom.
       * llibertat d'associació. Hi ha coŀlectius poc acceptats socialment. Potser en algun moment no cal que un cercle social ho conegui tot d'altres cercles. Amics del poble, de la uni, familiars, associacio lúdica, política, esportiva, companys de feina, etc. Fins i tot inclos els grups activistes de tamátiques socialment acceptades com el feminisme pateixen una repressió forta i han de protegir la seva privacitat.
   * Sobirania de la informació
       * Les meves dades personals són meves, em descriuen, defineixen i condicionen. Si no les controlo, no controlo la meva vida.
       * Per defecte tot ha de ser privat: jo trio què compartir amb qui en cada moment.
       * Sobirania tecnològica: necessito confiar en qui gestiona la infraestructura que emmagatzema i transporta les meves dades.
   * Eslògan cypherpunk: transparència pel poderós, privacitat per al feble.
   * consciencia global: impacte ecologic i civilitzatori
   * empoderament personal

### Model d'amenaça:

* conceptes
   * agents (aliats, neutrals, enemics)
   * actius (béns a protegir)
   * risc (probabilitat vs dany)    * a nivell personal
     * intern ?  - falta de coneixements, factors emocionals, economics que poden contribuir a la perdua de privadesa
     * extern ? - qui i amb quins motius ? a titol personal o dins del marc del control massiu?
* a nivell de grup:
   * interna ?
   * externa ?
* a nivell de societat / civilització
   * ecologic
   * economic
   * cultural
* Riscos
   * anàlisi de riscos: l'entorn, agents, béns
   * percepció dels riscos
   * agilitat + ??

### Elements:
font: [https://gitlab.com/criptica/inseguretat-smartphone/blob/master/PITCHME.md#perills](https://gitlab.com/criptica/inseguretat-smartphone/blob/master/PITCHME.md#perills)

* Perills al dispositiu 
   * Físics: robatori, malfunció 
     * Robatori: accés al mail i a totes les aplicacions
     * Sabotatge: no disponibilitat i pèrdua de dades (?)
     * No reparable, obsolescència
   * Hardware
     * accés directe entre components (GSM → memòria)
     * forats de seguretat en el processador
     * components no desconnectables (micròfon, mòdems diversos) → kill switch https://puri.sm/shop/librem-5/
   * Sistema operatiu
     * Bloatware espia, privatiu, pesat
     * Backdoors: PlayStore n'és una
     * Codi privatiu: teclat, cerques
     * Actualitzacions de seguretat
   * Aplicacions
     * Jocs que demanen molts permisos
     * Aplicació espia per algú proper
     * Aplicació legítima enverinada
     * Errors no arreglats
     * Tracking, fingerprinting (anuncis, webs)
* Perills a la xarxa
   * Xarxa local
      * Wifi
      * 3G
    * Xarxa d'accés + troncal
       * DPI (deep packet inspection)
       * censura
       * no neutralitat: vídeos gratuïts, línies ràpides
    * Servidors finals
       * forats de seguretat (veure "perills al dispositiu")
       * tràficants de dades
       * generació de perfils
       * censura
* Agents possibles
   * Un particular malintencionat
   * Investigador privat
   * Policia amb o sense ordre jodicial
   * Màfies amb recursos
   * Traficants de dades personals autònoms
   * Empreses que fan negoci amb dades
   * Agències d'espionatge governamentals
* Idees
   * Comunicar com de secreta és una informació
   * Xifrar els discos durs
   * Contrasenyes fortes i úniques
   * Usar VPN o Tor
   * Wi-Fi: no WPS, WPA2, contrasenya canviada
   * Sistema operatiu lliure
   * Evitar serveis privatius i centralitzats