---
title: Alternatives
published: false
taxonomy:
    category:
        - docs
visible: true
---

Aquí deixem un directori de recursos: eines alternatives als serveis privatius; lliures i respectuoses amb les seves usuàries:

## Recursos externs
   * Directori d'alternatives [Switching Software](https://switching.software/) ha recuperat el contingut de [l'antiga Switching Social](https://switching.software/about-this-site/)
   * Directori d'alternatives [Prism Break](https://prism-break.org/en/all/)
   * Directori d'alternatives [alternativeTo](https://alternativeto.net/) Compte que no distingeixen entre programari lliure, privatiu o open source
   * Servidors d'aplicacions o serveis lliures: [Framasoft](https://framasoft.org/?m=lite)
   * Directori d'aplicacions lliures per a Android: [Fossdroid](https://fossdroid.com/)

## Ordinador
* Sistemes operatius lliures
   * [Linux Mint](https://linuxmint-installation-guide.readthedocs.io/ca/latest/)
   * [Ubuntu (edicions alternatives)](https://www.ubuntu.com/download/ubuntu-flavours)
   * [Debian](https://www.debian.org/)
* Navegador
   * [Firefox](https://www.mozilla.org/firefox)
   * [Chromium](https://www.chromium.org/getting-involved/download-chromium)
* Edició d'imatge, disseny, creació 3D, publicació
   * Retoc d'imatges amb [Gimp](https://www.gimp.org/)
   * Dibuix vectorial amb [Inkscape](https://inkscape.org/)
   * Pintura digital amb [Krita](https://krita.org/)
   * Multifunció (àudio, vídeo, disseny 3D) amb [Blender](https://www.blender.org/)
   * Maquetació amb [Scribus](https://www.scribus.net/)
   
## Telèfon mòbil
* Mercat d'aplicacions lliures: [F-Droid](https://f-droid.org/)
* Descarregador d'aplicacions de Google Play: [Yalp store](https://fossdroid.com/a/yalp-store.html)
* Mercat d'aplicacions, lliures i no lliures: [Aptoide](http://www.aptoide.com/)
* Mapes: [OsmAnd](https://f-droid.org/en/packages/net.osmand.plus/) i [Maps.me](https://fossdroid.com/a/maps.html)
* Missatgeria
   * [Signal](https://signal.org/android/apk/). Segura però centralitzada, necessita nº telf.
   * [Conversations](https://fossdroid.com/a/conversations.html) (xmpp/jabber), federada i confidencial però revela metadades.
   * [Riot.im](https://fossdroid.com/a/vector.html) (matrix.org), federada i confidencial però revela metadades i xifrat en proves
   * [Kontalk](http://kontalk.org/) (basada en xmpp)
* Sistema operatiu Android sense Google: [LineageOS](https://wiki.lineageos.org/devices/) (successor de cyanogenMod)
* Sistema operatiu Android sense Google: [Omnirom](https://www.omnirom.org/) *fork* de cyanogenmod després de [*certs problemes*](https://www.theregister.co.uk/2013/10/16/android_custom_roms_splinter_over_openness/)
* El mòbil amb més hardware lliure i software lliure (KDE/GNOME/Debian/Matrix) [Librem 5](https://puri.sm/shop/librem-5/)

## Alternatives a Google
#### Drive
   * **Nextcloud**
     * Tria una proveïdora: [https://nextcloud.com/providers/](https://nextcloud.com/providers/)
     * [Disroot](https://disroot.org/es/services/nextcloud)
     * [Indie hosters](https://indie.host/)
   * **Owncloud** (versió no comunitària de nextcloud) [https://owncloud.org/hosting-partners/](https://owncloud.org/hosting-partners/)

#### Docs
   * **JetPad**: amb formatat [https://jetpad.net/](https://jetpad.net/)
   * **Collabora** LibreOffice: amb formatat però sense instaŀlacions públiques. Info des de [Collabora](https://www.collaboraoffice.com/solutions/collabora-office/) i des de [Nextcloud](https://nextcloud.com/collaboraonline/)
   * **CryptPad**: xifrat però públic amb enllaç [https://cryptpad.fr/](https://cryptpad.fr/)
   * **ProtectedText**: xifrat amb una contrasenya separada [https://www.protectedtext.com/](https://www.protectedtext.com/)
   * Pads clàssics, fàcils, públics:
       * [https://pad.lleialtat.cat/](https://pad.lleialtat.cat/)		
       * [http://etherpad.guifi.net](http://etherpad.guifi.net)
       * [http://pad.lamardebits.org/](http://pad.lamardebits.org/)
       * [https://pad.frontlinedefenders.org/](https://pad.frontlinedefenders.org/)
       * [https://pad.riseup.net/](https://pad.riseup.net/)
       * [https://pad.codigosur.org/](https://pad.codigosur.org/)
       * [https://antonieta.vedetas.org](https://antonieta.vedetas.org)
       * [https://mensuel.framapad.org/](https://mensuel.framapad.org/)
       
#### Search
   * NO **Google**, trafica amb dades, és un règim dictatorial digital
   * NO **Bing**, Microsoft trafica amb dades, és un règim dictatorial digital
   * NO **Yandex**, trafica amb dades, és un règim dictatorial digital
   * NO **Ecosia**, planta arbres a canvi de vendre la teva identitat als mercats de tràfic de dades, màrketing i manipulació social
   * SÍ **Duckduckgo** [https://duckduckgo.com/](https://duckduckgo.com/) Cercador. És una empresa dels USA però respecta la privacitat dels usuaris. Guanya diners amb anuncis no personalitzats.
   * SÍ **StartPage/Ixquick** [https://www.startpage.com/](https://www.startpage.com/)  Metacercador sobre Google. És una empresa europea però respecta la privacitat dels usuaris. Guanya diners amb anuncis no personalitzats.
   * SÍ **Searx**. És un metacercador sobre 70 cercadors (codi lliure)
       * [https://searx.me](https://searx.me)
       * [https://searx.org](https://searx.org)
       * [https://searx.laquadrature.net/](https://searx.laquadrature.net/)
       * [https://framabee.org/](https://framabee.org/)
       * [https://search.disroot.org/](https://search.disroot.org/)
      
#### Maps
   * **OpenStreetMaps**
       * Estàndard amb 3 capes: [https://www.openstreetmap.org/node/5426862347](https://www.openstreetmap.org/node/5426862347)
       * Opentopomap: corbes de nivell i muntanya [https://opentopomap.org/#map=13/41.34112/1.70151](https://opentopomap.org/#map=13/41.34112/1.70151)
       * Umap: crea un mapa personalitzat
           * [https://framacarte.org](https://framacarte.org)
           * [http://umap.openstreetmap.fr](http://umap.openstreetmap.fr)
   * **Institut Cartogràfic i Geològic de Catalunya**
       * Instamaps: crea un mapa personalitzat [https://www.instamaps.cat/](https://www.instamaps.cat/)
       * Mapa polític/topogràfic i de satèl·lit [http://www.icc.cat/vissir3/index.html?ryKiLHg78](http://www.icc.cat/vissir3/index.html?ryKiLHg78)
       * Altres [http://betaportal.icgc.cat/](http://betaportal.icgc.cat/) (ALERTA, xifrat mal configurat)
* Indicacions
   * Amb transport públic, a catalunya: [https://mou-te.gencat.cat](https://mou-te.gencat.cat)
   * A peu, bici o cotxe [https://www.openstreetmap.org/directions](https://www.openstreetmap.org/directions?engine=graphhopper\_bicycle\&route=41.3464%2C1.6995%3B41.5113%2C1.7016#map=11/41.4286/1.7104)

#### Mail
##### El problema del correu gratuït

* anuncis
* control social
* falta de privacitat
* funcionalitats no estàndards
* trenquen la federació amb servidors petits o alternatius

##### Els diners

* subscripció periòdica
* donacions
* intercanvi no monetari

##### Els estàndards

* penjar-enviar correus: SMTP
* descarregar-llegir correus: IMAP, POP
* xifrat client-servidor: TLS, STARTLS
* xifrat servidor servidor: DKIM, TLS
* xifrat extrem-extrem: PGP, DIME
* nova proposta: [darkmail](https://darkmail.info/)

##### Usabilitat del xifrat

* Clients de correu amb extensions (thunderbird, mutt, k9mail, etc.)
* PGP amb [leap](https://leap.se/)
* PGP amb [p≡p](https://www.pep.security/)
* propostes de xifrat incompatibles entre elles: tutanota, protonmail, mail1click
* cerca local més difícil quan els correus estan xifrats

##### Les proveïdores
_Llistem algunes que coneixem que tenen registre obert_

* Coŀlectius activistes, per donacions
    * [us] [RiseUp](https://riseup.net)
    * [it] [Autistici/Inventati](https://autistici.org)
    * [us] [Aktivix](https://aktivix.org/)
* Empreses cooperatives
    * [de] [Posteo](https://posteo.de/)
* Empreses centrades en la privacitat
    * [de] [Tutanota](https://tutanota.com/)
    * [ch] [Protonmail](https://protonmail.com/)
    * [us] [Lavabit](https://lavabit.com/)
* De proximitat
    * [cat] [Pangea](https://pangea.org)
    * [cat] [La mar de bits](https://lamardebits.org)

Llistes més completes:
* [Riseup: Radical servers](https://riseup.net/en/security/resources/radical-servers)
* [Privacy Tools](https://privacytoolsio.github.io/privacytools.io/#email)
* [Privacy-Conscious Email Services](https://www.prxbx.com/email/)

## Fedivers
* Introduccions generals a les xarxes socials federades
   *  [Presentació web](https://surtdelcercle.cat/presentacions/free-soft-day-2018/) de @surtdelcercle
   *  [Viquipèdia: Fedivers](https://ca.wikipedia.org/wiki/Fedivers)
   *  [Viquillibres: Fediverse](https://ca.wikibooks.org/wiki/Fediverse)
   *  [Subapartat en aquesta web](https://tec.lleialtat.cat/divulgacio/fediverse)
* Punts d'entrada al Fedivers, xarxa de xarxes federades:
   *  Introduccions als diversos programaris i notícies: [fediverse.party](https://fediverse.party/)
   *  Estadístiques d'instàncies: [fediverse.network](https://fediverse.network/)
   *  Estadístiques d'instàncies: [the-federation.info](https://the-federation.info/)
   *  [Node de Santsenques](https://santsenques.cat/) per a facilitar l'accés de les veïnes del barri, i a espais i projectes afins als valors de la Lleialtat. És una Pleroma. 
* Xarxes socials alternatives a Facebook
   * **Diaspora**
       * [servidors disponibles](https://the-federation.info/diaspora)
       * [web del projecte](https://diasporafoundation.org/)
   * **Hubzilla**
       * [servidors disponibles](https://the-federation.info/hubzilla)
       * [web del projecte](https://project.hubzilla.org/)
* Xarxes socials alternatives a Twitter
   * **GnuSocial** / **Quitter**. És la xarxa més antiga, i data del 2 de juny de 2008
       * [servidors disponibles](https://fediverse.kranglabs.com/)
       * [web del projecte](https://gnu.io/social/)
   * **Pleroma**. És la xarxa federada més preocupada per la privacitat. Es comenta que és més lleugera que Mastodon i s'inspira de GNUsocial en la seva aparença, tot i que permet una aparença Mastodon
       * [servidors disponibles](http://distsn.org/pleroma-instances.html)
       * [web del projecte](https://pleroma.social/)
   * **Mastodon**. És la xarxa federada més coneguda però una xarxa més i nascuda al 2016. Es posa en qüestió la seva governança (amb exemples com la desfederació amb GNUsocial/OStatuts) i la seva aparença de les tres columnes que hi ha voluntat de canviar a una aparença més GNUsocial o Pleroma
       * [servidors disponibles](https://instances.social/) 
       * [web del projecte](https://joinmastodon.org/)
* Xarxes socials alternatives a Instagram (imatge)
   * **[Quit.im](https://quit.im/)**
   * **[PixelFed](https://pixelfed.org/)**
* Xarxes socials alternatives a Youtube (vídeo)
   * **[Peertube](https://joinpeertube.org/es/)**
* Xarxes socials alternatives a Soundcloud o Spotify (àudio)
   * **[Funkwhale](https://funkwhale.audio/)**. De l'antiga GrooveShark :)
* Xarxes socials alternatives a Blogger (escriptura)
   * **[Write.us](https://write.as/)**
   * **[Plume](https://joinplu.me/)**
* Xarxes socials alternatives a Meetup o Facebook Events (trobades)
   * **[Get Together](https://gettogether.community/)**
   * **[Mobilizon](https://joinmobilizon.org/)**


