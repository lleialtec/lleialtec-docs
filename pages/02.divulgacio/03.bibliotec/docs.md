---
title: BiblioTec
taxonomy:
    category:
        - docs
visible: true
---

Hem adquirit **12 llibres** que reflexionen sobre la cultura digital i animem a clubs de lectura a llegir-los i debatre sobre ètica tecnològica. Fes una ullada als títols! 

* [En el acuario de Facebook](https://www.traficantes.net/libros/en-el-acuario-de-facebook)

* [Ídolos. ¿La red es libre y democrática? ¡Falso!](https://www.traficantes.net/libros/idolos)

*  [Cultura digital y movimientos sociales](https://www.traficantes.net/libros/cultura-digital-y-movimientos-sociales) 

*  [Las mil caras de anonymous](https://www.traficantes.net/libros/las-mil-caras-de-anonymous) 

*  [Por una cultura libre](https://www.traficantes.net/libros/por-una-cultura-libre) 

*  [El Kit de la lucha en Internet](https://www.traficantes.net/libros/el-kit-de-la-lucha-en-internet) 

*  [Ciberactivismo](https://www.traficantes.net/libros/ciberactivismo-0) 

*  [En defensa de la conversación. El poder de la conversación en la era digital](http://aticodeloslibros.com/index.php?id_product=106&controller=product) 

*  [La tragedia del copyright](https://www.traficantes.net/libros/la-tragedia-del-copyright) 

*  [El entusiasmo](https://www.traficantes.net/libros/el-entusiasmo-1) 

*  [Cultura libre digital](https://www.traficantes.net/libros/cultura-libre-digital) 

*  [El enemigo conoce el sistema](https://www.megustaleer.com/libros/el-enemigo-conoce-el-sistema/MES-106841) 

A més, Críptica ens va regalar un exemplar de ["Resistències digitals"](https://www.criptica.org/blog/resistencia-digital/). Merci, compis! 
