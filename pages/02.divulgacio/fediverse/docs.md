---
title: FEDIVERSE
taxonomy:
    category:
        - docs
visible: true
---

#### INTRO
El o la [**FEDIVERSE**](https://fediverse.party/) (FEDeration unIVERSE) és la **xarxa de xarxes socials lliures i ètiques**. Hi ha moltes xarxes socials a la FEDIVERSE i moltes són de microblogging (Mastodon, Pleroma, GNUsocial, etc. ). Però també federen altres eines, com Pixelfed, Funkwhale i Peertube, que serveixen per emmagatzemar i compartir imatges, música i vídeos, respectivament. I no cal tenir un compte a cada lloc: les usuàries d'eines federades poden interactuar entre elles.

Serveis de xarxes asocials privatives com poden ser Twitter o Facebook, no incorporen aquesta federació i, per tant, **una usuària de Twitter no pot interactuar amb una usuària de Facebook**; i viceversa. A més, les xarxes lliures de la FEDIVERSE han estat creades per poder ser autogestionades per les seves usuàries, i no pas per cap corporació amb ànim de lucre.

#### LLIBRET
Per tal d'explicar què és i com funciona la FEDIVERSE i, sobretot, per tal d'animar a les veïnes de tots els barris a conèixer xarxes realment socials i lliures, hem creat un text en català anomenat **"FEDIVERSE, xarxes lliures i ètiques"**. El podeu trobar en aquests formats:

* [a4](http://documents.lleialtat.cat/LleialTec/Fediverse/fediverse-xarxes-lliures-a4.pdf). Per imprimir a doble cara, imprimir imparells i després parells, o a la inversa. 

* [a4-landscape](http://documents.lleialtat.cat/LleialTec/Fediverse/fediverse-xarxes-lliures-a4-landscape.pdf). S'imprimeix i es doblega. Queda en forma de llibret a5. Per imprimir a doble cara, imprimir imparells i després parells, o a la inversa.~~ Avís: és maquetació de guerrilla. Aviat farem un taller de [Scribus](https://www.scribus.net/) i el maquetarem com cal ;)~~ De moment, tirem de LibreOffice 

* [pad traduïble](https://pad.lleialtat.cat/p/fediverse-ca). Hem creat un pad per a la versió en català i animem a totdon i tothom a traduïr-lo. Actualització: gràcies a qui l'estigui traduïnt al [gallec](https://pad.lleialtat.cat/p/fediverse-gl) :)

* [a5](http://documents.lleialtat.cat/LleialTec/Fediverse/fediverse-xarxes-lliures-a5.pdf). Hem imprès 100 exemplars d'aquesta versió del llibret en a5 de cara al [SobTec](sobtec.cat/), on participem d'una xerrada titulada "Història i estat actual de les xarxes socials lliures i distribuïdes" :) Aquests 100 exemplars ens han costat 121 iuros (IVA inclòs). 
 
#### ESDEVENIMENTS
5 de desembre de 2018. Al [berenatec](https://tec.lleialtat.cat/divulgacio/berenatec) de desembre, que va ser el primer de tots, va tenir per tema la presentació del llibret. 

2 de març de 2019. La xerrada al [Sobtec](http://sobtec.cat/calendari.html) la vam fer **a 6 mans i en tres parts**: la primera part és un [vídeo](https://peertube.cat/videos/watch/a1824b1f-aee0-4768-9e92-47cd01b69a37) introductori que en presenta el context imperant ;) la segona és una [cronologia](https://tec.lleialtat.cat/subsites/un-passeig-pel-fediverse/) anomenada "Un passeig pel Fediverse", que ens explica la història de les xarxes socials lliures; i la tercera, són unes [diapos](http://documents.lleialtat.cat/LleialTec/Fediverse/presentacio-llibret-sobtec-19.pdf) que expliquen el llibret **"FEDIVERSE, xarxes lliures i ètiques"**. Per llegir més sobre aquest tema, vegeu l'entrada titulada ["Vam estar al Sobtec 19"](https://surtdelcercle.cat/index.php?view=blog&id=44) al blog de Surt del Cercle. 

15 de març de 2019. Accidentalment, -i per tal de fer conèixer l'existència i auge d'eines de comunicació ètiques-, vam acabar participant a ["Comun_ESS"](https://comunicarestransformar.net/), una trobada sobre la comunicació de les entitats de l'Economia Social i Solidària a nivell de l'estat espanyol. 

També al març, com l'autor va acceptar posar el contingut també a Peertube, vam posar subtítols en català i espanyol al seu vídeo ["Mastodon & Fediverse: Explained"](https://peertube.social/videos/watch/d9bd2ee9-b7a4-44e3-8d65-61badd15c6e6). 

#### SANTSENQUES
A Lleialtec, participem de la FEDIVERSE a través d'una instància o node de Pleroma que s'allotja a la Lleialtat. I convidem a les veïnes de Sants, especialment les persones i entitats de la CELS, a crear-se un compte en aquest node. Sigueu benvingudes a [**Santsenques**](https://santsenques.cat/) i saludeu-nos virtualment a @lleialtec@santsenques.cat :)

Si teniu qualsevol dubte o simplement voleu conèixe'ns, passeu a saludar físicament a [l'aula lliure de la Lleialtat](https://tec.lleialtat.cat/infraestructura/aules-lliures). Hi som **cada primer i tercer dilluns de mes** i estarem encantades de donar-vos un cop de mà per participar del node creant un compte per vosaltres i/o per la vostra entitat o col·lectiu. 

A més, si sou membres de la CELS, també us podem donar suport per resoldre qualsevol qüestió tècnica o conceptual que tingui a veure amb la participació digital a la Lleialtat, com fer funcionar [l'àgora](https://tec.lleialtat.cat/infraestructura/agora). 
