---
title: Impressores
---

### Instal·lació i configuració HPLIP per a estació Ubuntu Mate 16.04

Configuració per a l'impressora _HP Laserjet Pro MPF M130fn_ a una _Ubuntu 16.04_,
tot i que es pot estendre per a d'altres impressores _HP_, així com altres sistemes operatius.

Seguirem les instruccions de la pàgina web: [HP Linux Imaging and Printing](https://developers.hp.com/hp-linux-imaging-and-printing/install/install/index)

###### Pas 1: Descàrrega

Ens baixem el paquet *HPLIP* de la [pàgina de descàrregues](https://developers.hp.com/hp-linux-imaging-and-printing/gethplip)

Al nostre cas és: `hplip-3.17.11.run`

###### Pas 2: Instal·lació i configuració  

Obrim una terminal, ens canviem al directori a on es troba la descàrrega i l'executem amb l'ordre:

```
usuari@recepcio1:~$ cd Baixades/
usuari@recepcio1:~/Baixades$ bash hplip-3.17.11.run
```
	
![Imatge de la terminal](ExecuteHPLIP.png)
	
* INSTALLATION MODE: automàtic `a`.
* DISTRO/OS CONFIRMATION: si detecta la nostra distro responem amb `y`.
* ENTER USER PASSWORD: escrivim la contrasenya.
* INSTALLATION NOTES: comprovem que s'acompleixen els requeriments demanats. Amb Ubuntu demana que tinguem activats certs respositoris.

Comprovem que al menys existeixen les següents línies al `/etc/apt/sources.list`:

```
deb http://es.archive.ubuntu.com/ubuntu/ xenial main restricted
deb http://es.archive.ubuntu.com/ubuntu/ xenial universe
deb http://es.archive.ubuntu.com/ubuntu/ xenial multiverse
```
	
* SECURITY PACKAGES: acceptem que s'instal·li el perfil d'*hplip*.
* MISSING DEPENDENCIES: acceptem que s'instal·lin tots els paquets necessaris per resoldre dependències.
* Podríem tenir instal·lada una altre versió d'aquest programa, en aquest cas acceptem que l'esborri i instal·li la nova

```
HPLIP-3.16.3 exists, this may conflict with the new one being installed.
Do you want to ('i'= Remove and Install*, 'q'= Quit)?    :
```

![Versió anterior trobada al sistema](VersioAnteriorInstalada.png)

* COFFE TIME: escollim sol/tallat/cigaló...
* HPLIP UPDATE NOTIFICATION: Acceptem comprovar si hi ha actualitzacions d'_HPLIP_
* RESTART OR RE-PLUG IS REQUIRED: Si estem instal·lant mitjançant wireless/ethernet podem escollir `i` (_ignore/continue_)
* PRINTER SETUP: per exemple gràfica _GUI_
	
###### Pas 3: Execució

![Deteccio de l'impressora]()
	
* Seleccionem el tipus de connexió per a l'impressora. Al nostre cas *Network/Ethernet/Wireless*
* Podria no detectar la impressora i en aquest cas entrem a *Show Advanced Option* i escollim la detecció manual indicant la seva adreça IP
* ALTRES: Podem configurar que l'icona d'HP que es mostra a l'escriptori no es mostri sempre.

----------------

###### Pas 4 (opcional :D) **Troubleshooting**:

* Versió adient de *HPLIP* o impressora suportada? Comprova-ho en aquest [enllaç](https://developers.hp.com/hp-linux-imaging-and-printing/supported_devices/index)
* *Selinux* o *AppArmor* en mode permissiu o deshabilitat.

## Scanner

```hp-plugin```

And follow the instruccions.


