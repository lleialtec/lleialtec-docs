---
title: 'Venda d''entrades '
published: true
taxonomy:
    category: docs
    tag: ''
visible: true
---

##Pretix Installation

Primero probamos una instalación manual de Pretix pero nos daba errores. Así pues instalamos la versión Docker.

Ha sido una instalación facil, siguiendo los pasos uno por uno de [la propia documentación de Pretix](https://docs.pretix.eu/en/latest/admin/installation/docker_smallscale.html).

La única cosa que hemos cambiado ha sido la configuración del proxy ya que el servidor donde alojamos entrades.lleialtat.cat corre un apache2.

###Apache proxy config for Pretix
```
<VirtualHost *:80>
	ServerName entrades.lleialtat.cat  
	ServerAdmin info@tec.lleialtat.cat
	Redirect / https://entrades.lleialtat.cat/
</VirtualHost>

<VirtualHost *:443>
	ServerName entrades.lleialtat.cat
    ServerAlias *.entrades.lleialtat.cat
    DocumentRoot /var/www/html

    ProxyRequests Off
    ProxyPreserveHost On
    RemoteIPHeader X-Forwarded-For

    SSLEngine on
    SSLCertificateFile            /etc/letsencrypt/live/entrades.lleialtat.cat/cert.pem
    SSLCertificateKeyFile         /etc/letsencrypt/live/entrades.lleialtat.cat/privkey.pem
    SSLCertificateChainFile       /etc/letsencrypt/live/entrades.lleialtat.cat/fullchain.pem
    Header set Strict-Transport-Security "max-age=31536000" env=HTTPS

    ErrorLog ${APACHE_LOG_DIR}/pretix_error.log
    CustomLog ${APACHE_LOG_DIR}/pretix_access.log combined

    <Location />
    	Order allow,deny
        Allow from all               
		ProxyPass http://localhost:8345/ retry=0 timeout=30
        ProxyPassReverse http://localhost:8345/
	</Location>
</VirtualHost>
```