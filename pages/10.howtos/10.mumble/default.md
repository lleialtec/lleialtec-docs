---
title: Mumble
taxonomy:
    category:
        - docs
visible: true
---

## Our set up.
dom0 is a linux server that hosts LXC containers with LXD. We run an nginx server on dom0 to forward traffic to the different containers. We install letsencrypt certs on dom0 for the nginx proxy. Traffic forwared from nginx to the containers is not encrypted.

Now we want to install a mumble-server and the client mumble-web in a lxc container. We will use iptables on dom0 to forward traffic to the mumble-server as is required by the mumble client. Nginx will listen on port 443 and forward we traffic to the web-mumble client also running in the mumble container. Here's how to do it.

## Install the mumble server
After creating our new mumble container, we enter and
```
apt-get install mumble-server
```
Make a strong SuperUser password and save it some where safe.

Well, that was easy!

## Certificates
Log into dom0 and install letsencrypt certs for the domain you want. They will probable now be here `/etc/letsencrypt/live/mumble.your_domain.com/`

This certificate is good for the nginx server and the mumble server. Bonus! But, we need a way to copy the certificate to the mumble container.

Edit a new file `/usr/local/bin/copy_cert_to_mumble.sh`

```
#!/bin/bash

# Dereference the symbolic links
cp --dereference /etc/letsencrypt/live/mumble.your_domain.comt/fullchain.pem /etc/letsencrypt/mumble.fullchain.pem
cp --dereference /etc/letsencrypt/live/mumble.your_domain.com/privkey.pem /etc/letsencrypt/mumble.privkey.pem

# Push the files to the mumble container
lxc file push /etc/letsencrypt/mumble.fullchain.pem mumble/etc/letsencrypt/mumble.fullchain.pem
lxc file push /etc/letsencrypt/mumble.privkey.pem mumble/etc/letsencrypt/mumble.privkey.pem

# Restart the mumble server
lxc exec mumble service mumble-server restart
```
Certs get renewed every copule of months. So, `chmod +x copy_cert_to_mumble.sh` and set up a cron job to run this, say, twice a week?

Don't forget to create the destination directory `/etc/letsencrypt/` in the muble container.

## Forwarding traffic

### Firewall
We need to configure the firewall on dom0 to route traffic to our mumble container. Leave the container and enter in dom0.

iptables rules are
```
-A PREROUTING -i ens18 -p tcp -m tcp --dport 64738 -j DNAT --to-destination <your_container_ip>
-A PREROUTING -i ens18 -p udp -m udp --dport 64738 -j DNAT --to-destination <your_container_ip>
```
You should now be able to use your mumble server with the standard client installed on you PC.

### nginx proxy
We have a nginx server running on dom0. Nginx serves various websites (in other containers), and we will configure it to server the mumble-web that (we haven't installed yet) in our mumble container.

First we can edit `/etc/hosts` and include an entry like this
```
10.179.84.40    mumble
```
Replace 10.179.84.40 for your mumble container IP

Now create `/etc/nginx/sites-available/mumble`
```
server {
    listen      80;
    server_name mumble.your_domain.com;
    rewrite     ^(.*)   https://mumble.your_domain.com$1 permanent;
}
server {
        listen 443 ssl;
        server_name mumble.your_domain.com;
        ssl_certificate /etc/letsencrypt/live/mumble.your_domain.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/mumble.your_domain.com/privkey.pem;

        location / {
                proxy_pass http://mumble:64737;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection $connection_upgrade;
        }
}
map $http_upgrade $connection_upgrade {
        default upgrade;
        '' close;
}
```

## mumble-web
The mumble client can be overwhelming for new users. [Mumble-web](https://github.com/Johni0702/mumble-web) makes a great alternative.

Login to your mumble container.

```
apt-get install curl
curl -sL https://deb.nodesource.com/setup_13.x | bash -
apt-get install -y nodejs
npm install -g mumble-web
```
npm installs mumble-web in `/usr/lib/node_modules/mumble-web/`

### websockify

Nginx will forward traffic to websockify.
Log into the mumble container..

```
apt install websockify
```
Test it with
```
websockify --ssl-target --web=/usr/lib/node_modules/mumble-web/dist 64737 localhost:64738
```
You should be able to browse to https://mumble.your_domain.com

We want websockify to start when the container starts, and we don't want to run it as root user.

```
apt-get install supervisor
adduser --system --shell=/bin/bash --no-create-home  mumble-web
```
### supervisor config
Edit `/etc/supervisor/conf.d/mumblewebsock.conf`
```
[program:mumblewebsock]
command = websockify --ssl-target --web=/usr/lib/node_modules/mumble-web/dist 64737 localhost:64738
directory = /tmp
user = mumble-web
```
And restart supervisord

Good luck!