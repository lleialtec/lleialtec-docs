---
title: Enigmail
published: true
visible: true
---

**Manual bàsic d'Enigmail**
===========================

<span id="anchor"></span>Índex:

[R](#ResSeg)[esum sobre seguretat
informàtica](#ResSeg)

[Resum sobre
criptografia](#ResCri)

[Resum sobre
contrassenyes](#ResCon)

[Instal·lació i configuració bàsica
d'Enigmail](#5InsConEni)

[Pas 1, Instal·lar
Enigmail](#6Pas1)

[Pas 2, ](#7Pas2)[ ](#7Pas2)[Configurar
Enigmail](#7Pas2)

[Pas ](#8Pas2A)[2.A](#8Pas2A)[, ](#8Pas2A)[I](#8Pas2A)[nstal·lar GnuPG2
(condicional)](#8Pas2A)

[Pas 3, ](#9Pas3)[ Crear el parell de
claus](#9Pas3)

[Pas 4, ](#10Pas4)[ ](#10Pas4)[Generar Certificat de Revocació
(Opcional)](#10Pas4)

[Pas 5, ](#11Pas5)[ Configurar Preferències
d'Enigmail(Opcional)](#11Pas5)

[Pas 6, ](#12Pas6)[ ](#12Pas6)[Establir Xifratge i Signatura per
defecte](#12Pas6)

[Xifrar i signar
correus](#13XifSigCor)

[Aconseguir la clau pública d'un
contacte](#14AcoClauPubCon)

[Opció ](#15OpcioA)[A: ](#15OpcioA)[A](#15OpcioA)[djunta a un
missatge](#15OpcioA)[ (I explicació de xifrar i signar
missatges)](#15OpcioA)

[Opció B: Baixada d'un servidor de
claus](#16OpcioB)

[B.1: Pujar clau pública a
servidors](#17B1)

[Opció ](#18OpcioC)[C: Importar des del
“](#18OpcioC)[Porta-retalls](#18OpcioC)[”](#18OpcioC)

[Consells](#19Con)

<span id="anchor-1"></span>RESUM SOBRE SEGURETAT INFORMÀTICA:

La seguretat informàtica:

-És un conjunt de *valors*, tècniques i polítiques, un procés constant,
que mai és infalible. Remarquem l'aspecte cultural de la seguretat:
sense assimilar els seus valors tota política i tècnica és inútil.

-Tracta de minimitzar la vulnerabilitat dels sistemes i de la informació
que contenen, fent que el cost de modificar o d'accedir indegudament a
un recurs sigui més alt que el seu valor.

<span id="anchor-2"></span>RESUM SOBRE CRIPTOGRAFIA:

“Criptografia” ve del grec, i més o menys vol dir “art de fer escriptura
oculta”. La criptografia permet dues funcionalitats molt potents: el
“*xifratge*” i la “*signatura digital*”.

XIFRATGE: Ofereix la propietat de *confidencialitat:* El contingut del
missatge és accessible només als usuaris autoritzats (qui té la clau per
desxifrar el missatge).

Hi ha dos tipus bàsics de xifrat: Simètric i asimètric.

-Simètric: Utilitza la mateixa clau per xifrar i desxifrar.

Es basa en l'ús d'una sola *clau secretament compartida* entre qui xifra
el missatge i qui el desxifra.

L'intercanvi de la clau és un problema pels usuaris que no estan al
mateix lloc, perquè si s'envia es pot interceptar.

La quantitat de claus secretament compartides d'un usuari és tan gran
com la quantitat d'usuaris amb els que es comunica individualment.

Els algorismes de xifrat simètric solen ser més potents (més ràpids i
segurs) que els d'asimètric. L'estàndard actual és l'AES (“Advanced
Encryption Standard”, també conegut com a “Rijndael”).

-Asimètric: Utilitza una clau (la clau pública) per xifrar, i una altra
clau (la clau privada) per desxifrar. Les dues claus estan relacionades
matemàticament, formant una “parella natural”.

Cada usuari té una parella de claus, pública i privada. La pública la
pot saber tothom, per exemple publicada en servidors de claus, però la
privada només l'ha de saber el seu propietari.

La clau pública del receptor l'utilitzen els emissors per xifrar els
missatges.

La clau privada del receptor l'utilitza el receptor per desxifrar tals
missatges.

L'algorisme de xifrat asimètric estàndard actualment és l'RSA (“Rivest,
Shadir i Adleman”, cognoms dels seus creadors).

A part, el xifrat asimètric també es fa servir de manera quotidiana per
*signar/verificar* digitalment els missatges, funcionalitat que es pot
fer servir sola o juntament amb la de *xifrar/desxifrar*.

SIGNATURA DIGITAL: Ofereix tres propietats molt importants:
*Autenticació*, *integritat* i *no-repudi*.

-Autenticació: Qui ha escrit el missatge és qui diu ser.

-Integritat: El missatge no ha sigut modificat després d'haver-se
signat.

-No-repudi: L'emissor del missatge no se'n pot desdir, davant de
tercers, de ser l'autor de tal missatge.

­<span id="anchor-3"></span>RESUM SOBRE CONTRASSENYES:

*Política de contrassenyes*: Normes de creació, protecció i renovació de
bones contrassenyes en una organització.

Creació:

Una bona contrassenya no conté paraules de diccionari (en cap idioma),
ja que es poden descobrir fàcilment amb tècniques de “força bruta”
(provar totes les combinacions possibles).

Una bona contrassenya no usa només lletres majúscules o minúscules, o
només números, o només caràcters especials, ja que es redueixen les
combinacions i la fortalesa de la contrassenya.

Una bona contrassenya no usa informació personal, de cap tipus. Això vol
dir que no conté noms de familiars, mascotes, dates de naixement,
números de comptes, o qualsevol dada identificativa.

Protecció:

Una bona contrassenya no està escrita en un suport sense xifrar. L'òptim
és que no estigui escrita enlloc, ni xifrat ni “en pla” (no xifrat).

Una bona contrassenya no és enviada per correu electrònic, sigui xifrat
o no (ENGINYERIA SOCIAL). *L'enginyeria social és la intrusió de
sistemes mitjançant la manipulació de persones.*

Una bona contrassenya no és comunicada a ningú per telèfon (ENGINYERIA
SOCIAL).

Una bona contrassenya no és comunicada a NINGÚ, ni se'n parla amb NINGÚ
(ENGINYERIA SOCIAL). Només l'ha de saber qui l'ha creada, no l'ha de
comunicar ni a tècnics ni a sa pròpia mare.

Una bona contrassenya no té pistes per poder-la recordar i esbrinar
(Exemple: Una nota a l'agenda recordant-te sobre què va la
contrassenya).

Una bona contrassenya no és utilitzada en diferents sistemes. Per
sistema s'enten ordinadors o programes o qualsevol cosa que necessiti de
contrassenya.

Una bona contrassenya no és recordada per les aplicacions que la usen.
Per tant, l'òptim és introduir-la cada cop que s'ha de fer servir,
encara que sigui incòmode.

Renovació:

Una bona contrassenya no és la creada per defecte. Per tant, s'ha de
canviar immediatament quan es comença a fer servir el sistema.

Una bona contrassenya no ha de fer-se servir durant molt de temps, el
límit sent més o menys 6 mesos. Els algorismes per descobrir
contrassenyes són cada cop més ràpids i eficients.

Per resumir, una bona contrassenya:

-És una cadena de caràcters composada de lletres minúscules i
majúscules, números i caràcters especials.

-No té cap lògica aparent.

-És molt llarga, d'al menys 20 caràcters.

-Només està al cap de qui l'ha creat.

-És renovada freqüentment, cada 6 mesos com a mínim.

L'òptim és crear-la aleatòria o pseudoaleatòriament.

Per emmagatzemar-la el millor és fer un exercici de memòria, evitant
guardar-la escrita, ja sigui xifrada o en pla, i evitant també deixar-se
pistes per recordar-la.

<span id="anchor-4"></span>INSTAL·LACIÓ I CONFIGURACIÓ BÀSICA D'ENIGMAIL

Partim de la suposició que fem servir un sistema GNU/Linux, una
distribució com el Debian o l'Ubuntu (o el Lubuntu), i que ja tenim
instal·lat el Thunderbird/Icedove.

El que hem de fer és instal·lar Enigmail, un “plugin”\* per
Thunderbird/Icedove que permet xifrar i signar digitalment els correus
(li dóna capacitats criptogràfiques).

<span id="anchor-5"></span>**Pas 1, Instal·lar Enigmail: **Per fer això
obrim l'Icedove, i cliquem al menú (el botó amb tres línies grises
horitzontals, a dalt a la dreta) i cliquem a “Add-ons”, la opció que té
la icona d'una peça de trencaclosques. En català es diria “Components” o
similar:

![](enigmail-000.png)

\* Nota: Un “plugin”, o “add-on”, és un component opcionalment
instal·lable d'un programa, una extensió que li afegeix característiques
específiques, com, en aquest cas, criptografia.

Aleshores, a la pestanya que s'ha obert, “Add-ons Manager” (“Gestor de
Components”), a l'entrada de text per buscar (a dalt a la dreta, amb la
icona de lupa), cliquem a l'espai en blanc i escribim “Enigmail”:

![](enigmail-001.png)

Ara premem la tecla Enter (o cliquem la lupa), i esperem que s'omplin
els resultats (hi ha d'haver connexió a internet). Hauria de sortir el
següent o similar:

![](enigmail-002.png)

Cliquem al botó “Install” (“Instal·lar”) -amb icona d'un disquet
(“floppy disk”)- que està a la fila de l'“Enigmail 1.8.2”\*, i esperem
que es descarregui el contingut del plugin i s'instal·li. Aleshores surt
això:

![](enigmail-003.png)

Ens demana que reiniciem l'Icedove per a que la instal·lació es
completi. Cliquem a on diu “Restart now” (“Reiniciar ara”), i es
reinicia.

\*Nota: La versió “Enigmail 1.8.2” és la que surt per defecte al juny de
2016, en altres èpoques segurament canviarà.

<span id="anchor-6"></span>**Pas 2, Configurar Enigmail: **Després de
reiniciar-se, surt aquesta finestra:

![](enigmail-004.png)

Deixem seleccionat “Start setup now” (“Començar configuració ara”), i
cliquem al botó “Next” (“Següent”).

Apareixerà això:

![](enigmail-005.png)

Seleccionarem “I prefer a manual configuration (recommended for
experts)” (“Prefereixo una configuració manual (recomanada per a
experts)”), i cliquem al botó “Next”.

Si et surt la següent finestra o similar (depenent de la versió i de
l'idioma) hauràs de fer el **Pas 2.A **abans de fer el **Pas 3**:

![](enigmail-006.png)

<span id="anchor-7"></span>**Pas 2.A, Instal·lar GnuPG2 (condicional):
**La finestreta que surt diu que fem servir una versió obsoleta d'un
programa necessari, i recomana fer sevir la versió actual.** **Per
entendre aquest pas, s'ha de saber que l'Enigmail és un programa de la
capa gràfica, que fa servir programes de criptografia de la capa no
gràfica, capa que està a sota de la gràfica i és usada per ella. El
programa principal que fa servir per sota es diu GnuPG, que vol dir GNU
Privacy Guard (Guardià de la Privacitat de GNU). Aquest és el programa
que realment fa la feina criptogràfica, i és utilitzat per l'Enigmail
per xifrar i signar correus.

El que passa és que les noves versions d'Enigmail depenen de la versió 2
del GnuPG (el “GnuPG2”), i no de la primera. Algunes instàncies de
Debian (o d'Ubuntu o Lubuntu, etc. -basades en Debian-) ja tenen GnuPG2
instal·lat, però altres no. Si la teva instància no la té instal·lada,
l'Enigmail t'ho dirà amb la finestreta mostrada just ara, i el millor
serà que instal·lis el GnuPG2.

Per instal·lar el GnuPG2, hauràs d'obrir una terminal\* (la típica
pantalla fosca amb lletres) i escriure la següent comanda:

“

sudo apt-get install gnupg2

“

Et demanarà la teva contrassenya, i, si tens permissos d'administrador
(si no, envia'ns un correu i t'ajudarem!), començarà a instal·lar el
GnuPG2. Potser et demana que confirmis la instal·lació; si és així,
escriu una “y” i apreta “Enter”. Quan el GnuPG2 ja estigui instal·lat,
l'Enigmail l'hauria de reconèixer automàticament.

\*Nota: Per obrir una terminal, en Ubuntu o Lubuntu, pots polsar a la
vegada les tecles “Ctrl”, “Alt” i “T”. També pots obrir una terminal pel
menú del sistema, buscant “Eines del Sistema”-&gt;”Terminal” o similar.

La següent és una imatge d'una terminal en un Debian 8 on s'ha
instal·lat el GnuPG2:

![](enigmail-007.png)

<span id="anchor-8"></span>Pas 3, Crear el parell de claus: Ara crearem
el parell de claus que farem servir per xifrar i signar correus.
Aquestes claus són la clau privada i la clau pública, i estaran
assignades a una “Identitat”. Normalment, aquesta identitat serà el nom
del correu que farem servir, per exemple “prova@prova.prova”. La clau
privada s'ha de protegir amb moltíssima cura.

Haver triat configurar manualment l'Enigmail vol dir que tenim més
possibilitats de configuració, ens donarà més flexibilitat. Per exemple,
la clau privada, si haguéssim elegit una de les dues opcions de
configuració més fàcils (“standard” o “extended” en comptes de
“manual”), l'hauríem de protegir amb contrassenya obligatòriament. Això,
si ja tens el disc dur xifrat, pot resultar una molèstia, innecessària
per dues raons: perquè cada cop que haguessis de desxifrar o signar un
correu hauries d'introduir tal contrassenya i perquè la clau es suposa
que ja està protegida si el disc dur està xifrat. Si no tens el disc dur
xifrat és “moralment” obligatori guardar la clau privada amb una
contrassenya (guardar la clau privada amb contrassenya és en sí xifrar
la clau només, no tot el disc dur), ja que si no està protegida
qualsevol pot desxifrar els teus missatges xifrats, o fer-se passar per
tu (signant missatges amb la teva clau privada). Poder guardar la clau
privada amb contrassenya no és una opció disponible només a l'hora de
crear el parell de claus: després si vols pots posar o treure
contrassenyes a la clau privada tants cops com vulguis.

Després de clicar “Next” a la pantalla d'elegir tipus de configuració (o
haver clicat “OK” a l'avís de GnuPG) surt aquesta finestra, anomenada
“Enigmail Setup Wizard” (“Assistent de Configuració d'Enigmail”):

![](enigmail-008.png)

Clicarem al botó “Key Management” (“Gestió de Claus”), per configurar
les claus. Ara mateix configurar les claus significa simplement crear un
parell de claus per la nostra identitat. Quan haguem acabat de crear les
claus, si volem, podrem clicar al botó “Preferences” (“Preferències”)
per configurar altres coses; després en parlarem.

Al clicar a “Key Management” sortirà el següent:

![](enigmail-009.png)

Aquesta finestra és el que es coneix com a “Enigmail Key Management”, o
sigui, “Gestor de Claus de l'Enigmail”. Després explicarem com
s'utilitzen algunes de les seves funcionalitats bàsiques, però ara ens
centrarem en generar les claus per la nostra identitat. Anem al menú
“Generate” (“Generar”) i cliquem a l'opció “New Key Pair” (“Nou Parell
de Claus”).

Sortirà la finestra anomenada “Generate OpenPGP Key” (“Generar Clau
OpenPGP”):

![](enigmail-010.png)

Aquesta finestra és la que farem servir per generar el parell de claus
(privada i pública, “germanes” relacionades matemàticament).

A dalt hi ha un selector tipus llista d'identitats, “Account / User ID”
(“Compte / ID d'Usuari”). Si tenim més d'un compte o identitat d'usuari,
haurem d'elegir la identitat per la que volem crear el parell de claus.

A sota del selector hi ha una opció tipus “checkbox” (“caixa de marcar”,
més o menys), que diu “Use generated key for the selected identity”, o
sigui, “Fer servir la clau generada per la identitat seleccionada”. Ho
deixarem marcat.

A sota d'aquest hi ha un altre “checkbox”, i és una opció súmament
***IMPORTANT***. Es diu “No passphrase”, o sigui, “Sense contrassenya”.
Si la marquem, no protegirem la clau privada amb contrassenya. Com hem
senyalat abans, si tenim el disc dur xifrat, podem equilibrar la balança
entre seguretat i comoditat cap a aquesta última i marcar la opció, ja
que la clau ja estarà protegida pel xifratge del disc dur. Si no tenim
el disc dur xifrat, tenim dues opcions: 1. Xifrar el disc dur i no posar
contrassenya a la clau. 2. Posar contrassenya a la clau. Hi ha una
tercera opció que és no posar contrassenya, però és el pitjor que es
podria fer si el disc dur no està xifrat: seria com no fer servir
criptografia, en realitat.

Per tant, si tenim el disc dur xifrat marquem el checkbox i passem a
veure el següent. Si no tenim el disc dur xifrat, escribim una
contrassenya (mireu el “[**RESUM SOBRE CONTRASSENYES**](#ResCon)**”
**exposat amunt).

A sota hi ha una línia anomenada “Comment” (“Comentari”). Millor NO hi
escribim pistes sobre la contrassenya, si n'haguéssim posat. En realitat
millor no posar-hi res.

Més a sota hi ha un selector de pestanyes, amb dues pestanyes: “Key
expiry” (“Expiració de clau”) i “Advanced...” (“Avançat...”).

A “Key Expiry” podem posar el número d'anys, mesos o dies (seleccionant
“years”, “months” o “days” a la llista) en què volem que “expiri” la
clau, o també podem marcar el checkbox “Key does not expire” (“La clau
no expira”) per a que no “expiri”. Aquesta última opció no és gens
recomanable. El més recomanable, sobretot utilitzant identitats
d'organitzacions, és elegir un temps més o menys curt per a que expiri,
per exemple, que expiri en 2 anys. La data d'expiració es pot
actualitzar sempre que es vulgui i es tingui la clau privada, per tant,
no us espanteu! Posar una data d'expiració raonablement curta és una
mesura bona, per “obligar-te” a mantenir la clau privada i el certificat
de revocació (després en parlem) en lloc segur i actualitzar-ne l'estat
als servidors de claus (després en parlem!).

A “Advanced...” podem elegir el tipus d'algorisme de xifrat que voldrem
fer servir. Les dues propietats de tal algorisme són “Key size” (“Mida
de la clau”), en número de bits, i “Key type” (“Tipus de clau”):

![](enigmail-011.png)

Si deixem “Key size” a “4096” i “Key type” a “RSA” tindrem l'algorisme
més fort que ens ofereix aquesta versió d'Enigmail.

Ara llegim el que posa a sota de tot, la nota en vermell. Diu que la
generació pot trigar varis minuts a completar-se, que no treiem
l'aplicació mentre ho fa, i diu que si surfegem per internet o fem
operacions intenses de lectura i escriptura amb el disc dur la generació
anirà més ràpid. Per exemple, podríem escoltar música i escriure un
article mentre es genera. El temps de generació depèn de l'ordinador,
però pot arribar a ser bastant llarg.

Cliquem a “Generate Key” i esperem a que es generi mentre fem de les
nostres.

<span id="anchor-9"></span>**Pas 4, Generar Certificat de Revocació
(Opcional): **Quan acabi de generar-se ens avisarà amb un “pop-up”
(finestreta que surt automàticament per avisar-nos), aquest:

![](enigmail-012.png)

Ens diu que s'ha completat la generació de la clau per la nostra identitat,
i que tal identitat es farà servir per signar. Recomana amb moltes ganes
que creem el que es coneix com a “Certificat de Revocació” de la nostra
clau. Aquest certificat es pot fer servir per invalidar la clau, per
exemple, si la perdem o ens la roben. És molt bona idea crear-lo, i
guardar-lo en un lloc segur per si de cas. Si et roben la clau privada,
o la perds, o no te'n recordes de la contrassenya per accedir a la clau,
la gent que tingui la teva clau pública no estarà massa temps amb la
teva clau errònia. Per tant, clicarem a “Generate Certificate” (“Generar
Certificat”), i sortirà una finestra on podrem elegir el directori on
guardar el certificat. Millor crear un directori especial al nostre
directori personal, on emmagatzemar certificats de revocació, i guardar
el certificat allà. Per exemple, a una carpeta anomenada “certificats”:

![](enigmail-013.png)

Després de clicar a “Save” (“Guardar”), sortirà això:

![](enigmail-014.png)

Diu que s'ha creat bé, i ens recomana transferir-lo a un lloc segur, com
un CD o un llapis USB. Cliquem a “OK”, i ara anirem un altre cop a la
finestra “Enigmail Key Management”, la qual haurà canviat:

![](enigmail-015.png)

Veiem que hi ha un sol registre a la llista. En aquest cas, el seu
“Name” (“Nom”) és “prova &lt;prova@prova.prova&gt;”, i “Key ID” (“ID de
Clau”) és “D4B35C13”. És el parell de claus per la nostra identitat.

Està en negreta, i això vol dir que en tenim tant la clau privada com la
clau pública (normal, ja que és la nostra identitat!).

Quan tinguem claus de contactes normalment seran només les seves claus
públiques, i no estaran en negreta.

Quan apareixin en cursiva voldrà dir que estan expirades o revocades.

De moment podem tancar la finestra ”Enigmail Key Management”, perquè ara
configurarem unes poques coses més abans de posar-nos a xifrar,
desxifrar, signar i verificar missatges.

<span id="anchor-10"></span>Pas 5, Configurar Preferències d'Enigmail
(Opcional): Tornem a la finestra “Enigmail Setup Wizard”, i cliquem a
“Preferences”.

![](enigmail-016.png)

Cliquem a “Display Expert Settings and Menus” (“Mostrar Menús i
Configuracions Expertes”), i apareixeran noves pestanyes i opcions:

![](enigmail-017.png)

Una opció interessant, si s'ha optat per protegir la clau privada amb
contrassenya, és la de “Remember passphrase for X minutes of idle time”
(“Recordar la contrassenya durant X minuts d'inactivitat”). La opció és
bastant autodefinidora, i farà que un cop hagis posat la contrassenya
per desxifrar missatges, la guardi en memòria durant tant de temps per a
que no l'hagis de tornar a posar cada cop que canvies a un altre correu
xifrat per llegir-lo.

També, si abans no teniem el GnuPG2 instal·lat i per tant l'hem
instal·lat, però surt la línia “GnuPG was found in /usr/bin/gpg” (“El
GnuPG s'ha trobat a /usr/bin/gpg”) en comptes de “GnuPG was found in
/usr/bin/gpg2”, cliquem a “Override with” (“Sobrescriure amb”), i
després escribim “/usr/bin/gpg2” a la línia en blanc. També podríem
buscar pel botó “Browse...”, però és més ràpid escriure-ho.

Ara, la pestanya més interessant és “Keyserver” (“Servidor de Claus”):

![](enigmail-018.png)

Aquí hi ha llistats els servidors de claus que farem servir. Un servidor
de claus és un ordinador connectat a internet que guarda claus públiques
de vàries identitats. Després hi podrem pujar la nostra clau pública, a
algun d'aquests (o a tots). De moment no cal tocar-hi res, però està bé
saber que existeix, per si la nostra organització fa servir un servidor
de claus propi, posar-lo a la llista.

Cliquem “Cancel” (“Cancel·lar”), per sortir d'aquesta finestra de
configuració. Ara, a la finestra anomenada “Enigmail Setup Wizard”,
cliquem a “Finish” (“Acabar”), perquè ja hem configurat l'Enigmail!

<span id="anchor-11"></span>Pas 6, Establir Xifratge i Signatura per
defecte: L'últim pas relacionat amb la configuració és el de dir-li a
l'Enigmail que volem xifrar i signar tots els missatges automàticament,
sense haver d'especificar-ho a cada missatge que enviem. Si hem d'enviar
un correu a algú de qui no hem pogut trobar la clau pública (als
servidors de claus o publicada a qualsevol altre lloc, o comunicada
directament a nosaltres) haurem de resignar-nos a enviar un correu sense
xifrar; per fer això podem desactivar el xifratge mentre escrivim el
missatge. Després veurem com. Ara fem això de “per defecte”: anem al
menú principal (les tres barres horitzontals grises), i cliquem a
“Preferences” (“Preferències”), i allà a “Account Settings”
(“Configuració del Compte”):

![](enigmail-019.png)


Aleshores surt una finestra anomenada “Account Settings”, i allà podem
veure això:

![](enigmail-020.png)

Aquí es veuen els comptes que tenim a la nostra sessió d'usuari. En aquest
cas, només tenim “prova@prova.prova”, llistat a l'esquerra. Dintre seu
hem de clicar la línia que diu “OpenPGP Security” (“Seguretat
OpenPGP”)\*, i a la dreta hem de buscar la part en negreta on diu
“Message Composition Default Options” (“Opcions Predeterminades de la
Composició de Missatges”), i marcar els dos checkboxes, “Encrypt
messages by default” (“Xifrar els missatges per defecte”) i “Sign
messages by default” (“Signar missatges per defecte”):

![](enigmail-021.png)

És important que també tinguem marcada la opció “Encrypt draft messages
on saving” (“Xifrar esborranys al guardar-los”).

Ara cliquem a “OK” i sortim i ja està tot configurat!

\*Nota: OpenPGP és l'algorisme/protocol principal que usa el GnuPG per
la criptografia. OpenPGP és la implementació “Open” del “PGP”, que vol
dir “Pretty Good Privacy”, o “Privacitat Prou Bona”.

<span id="anchor-12"></span>XIFRAR I SIGNAR CORREUS

Per a enviar un correu xifrat i/o signat, si hem instal·lat i configurat
Enigmail tal i com hem explicat, es fa així:

Primer de tot, si no es té la clau pública del receptor, s'ha
d'aconseguir. La clau pública es pot aconseguir de vàries maneres:

<span id="anchor-13"></span>Aconseguir la clau pública d'un contacte:

<span id="anchor-14"></span>Opció A: Adjunta a un missatge (I explicació
de xifrar i signar missatges). Explicarem aquesta opció junt amb
l'explicació de xifrar i signar missatges. Per a adjuntar la teva clau
pública en un missatge, comences a escriure un missatge:

![](enigmail-022.png)

![](enigmail-023.png)

Seguidament cliques al botó “Attach My Public Key” (“Adjuntar la meva clau
pública”), amb icona de clip:

Fixa't que aquesta barra d'eines és pròpia d'Enigmail. Com l'hem
configurat que per defecte xifri i signi, els botons corresponents estan
polsats ja.

T'hauria de quedar així:

![](enigmail-024.png)

Si et surt així:

![](enigmail-025.png)

No et preocupis, surt així perquè has activat manualment el xifratge i
signatura digital (has clicat els botons corresponents).

Ara simplement escrius el destinatari, i, si vols, un missatge i un
assumpte (COMPTE: ELS ASSUMPTES MAI ES XIFREN!!!), i l'envies -clicant a
“Send” (“Enviar”), a dalt a l'esquerra-:

![](enigmail-026.png)

En aquest cas, per simplificar, l'emissor i el receptor són el mateix,
però normalment no seria així.

A part, si el receptor fos diferent de l'emissor i no en tinguéssim la
clau, no podríem xifrar el correu, només signar-lo.

Segurament et sortirà això:

![](enigmail-027.png)

De moment deixem-ho com està, marcant el checkbox “Use the selected
method for all future attachments”, perquè no surti un altre cop, i
cliquem a “OK”.

Mirem el correu, i veiem que hem rebut el missatge!:

![](enigmail-028.png)

L'obrim, i veiem que l'ha desxifrat, i que la signatura és bona (ja la
tenim perquè és la nostra):

![](enigmail-029.png)

Ho veiem a la franja verda que diu “Enigmail Decrypted message; Good
signature from prova &lt; prova@prova.prova &gt;” (“Enigmail Missatge
desxifrat; Signatura bona de prova &lt; prova@prova.prova &gt;”).

Si els missatges rebuts són xifrats i signats, i si tenim la clau
pública de l'emissor, ens sortirà sempre aquesta franja verda amb un
missatge similar.

També podem veure el contingut original del missatge, sense desxifrar i
amb totes les metadades (dades tals com l'emissor, el receptor, data,
etc.), clicant a “Other Actions-&gt;View Source” (“Altres
Accions-&gt;Veure Font”):

![](enigmail-030.png)

![](enigmail-031.png)

I surt el missatge original, a la imatge de la dreta:

Abaix del missatge “normal” (no a la font, encara que també hi sigui)
veiem que hi ha adjuntat un fitxer, on diu “1 attachment:
0xD4B36C13.asc.pgp” (“1 fitxer adjunt: 0xD4B36C13.asc.pgp” ). El fitxer
“0xD4B36C13.asc.pgp” és el que conté la clau pública de la identitat
“prova@prova.prova”. Cliquem a “Save” (“Guardar”) i surt la finestreta
per guardar. Guardem el fitxer en un lloc segur, per exemple, ens creem
una carpeta anomenada “enigmail”:

![](enigmail-032.png)

Cliquem a “Save” de la nova finestreta, i ja hem guardat la clau
pública!

Ara, per poder-la fer servir a l'Enigmail, hem d'importar al programa el
fitxer que acabem de guardar. Per fer això, anem a “Enigmail Key
Management”:

![](enigmail-033.png)

I allà cliquem a “File-&gt;Import Keys from File”:

![](enigmail-034.png)

Busquem el fitxer amb la clau pública, que hem guardat, en aquest cas, a
la carpeta “enigmail” de la carpeta personal (“prova”), el seleccionem i
acceptem, clicant a “Open” (“Obrir”):

![](enigmail-035.png)

Sortirà l'avís:

![](enigmail-036.png)

Diu que la clau ha estat importada. Com que ja la teniem (era la nostra,
per fer una prova), no canvia res.

<span id="anchor-15"></span>**Opció B: Baixada d'un servidor de claus.
**Aquesta opció és bastant freqüent.

<span id="anchor-16"></span>B.1: Pujar clau pública a servidors. Abans
d'explicar com baixar una clau pública d'un servidor de claus,
explicarem com pujar-ne una.

Per pujar una clau pública a un servidor de claus, anem al menú
“Enigmail”, i allà cliquem a “Key Management” (“Gestió de Claus”).

![](enigmail-037.png)

Surt la finestreta “Enigmail Key Management”, i cliquem amb el botó dret
a la identitat de la que volem pujar la clau pública, i cliquem a
“Upload Public Keys to Keyserver” (“Pujar les Claus Públiques al
Servidor de Claus”):

![](enigmail-038.png)

Aleshores surt la finestreta “Select Keyserver” (“Seleccionar Servidor
de Claus”):

![](enigmail-039.png)

Aquí està la llista de servidors de claus que vem veure quan
configuràvem l'Enigmail. Hem d'elegir un dels servidors per pujar la
clau. A l'exemple elegim “keys.gnupg.net”:

![](enigmail-040.png)

Ara cliquem a “OK”, i puja la clau pública:

![](enigmail-041.png)

Sortirà l'”Enigmail Key Management” (“Gestió de Claus de l'Enigmail”), i hem
d'anar a “Search for Keys” (“Buscar Claus”):

![](enigmail-042.png)

Ara surt la finestreta “Select Keyserver” un altre cop, però per buscar,
en comptes de per pujar, claus públiques. Busquem un correu a l'atzar,
per exemple “rms@gnu.org”:

![](enigmail-043.png)

Cliquem a “OK” i comença a buscar, i quan acaba sortirà una cosa similar
a això:

![](enigmail-044.png)

Normalment la clau que ens interessa serà la que va ser creada fa menys
temps. En aquest cas hi ha dues claus creades al “2014-06-16”, per tant
les seleccionem les dues:

![](enigmail-045.png)

I cliquem “OK”, i passa el següent:

![](enigmail-046.png)

Aquí ens avisa que ha importat la clau “2A8E4C02”. Cliquem a “OK” i
tornem a veure l'”Enigmail Key Management”, ara amb més contingut:

![](enigmail-047.png)

Ara ja hem baixat una clau pública d'un servidor de claus! Ja podem
comunicar-nos amb el món exterior.

<span id="anchor-17"></span>Opció C: Importar des del “Porta-retalls”\*.
Aquesta opció és la més “manual”. Per entendre aquesta opció hem de
saber com és realment una clau pública. Per veure'n una, podem anar a la
finestra “Enigmail Key Management”:

![](enigmail-048.png)

I allà, clicar amb el botó dret sobre una identitat, per exemple
“prova@prova.prova”, i seleccionar la opció “Copy Public Keys to
Clipboard” (“Copiar Claus Públiques al Porta-retalls”):

![](enigmail-049.png)

![](enigmail-050.png)

Surt l'avís que diu que s'ha copiat:

\*Nota: “Porta-retalls”, en anglès “Clipboard”, és el lloc virtual on hi
ha el que tenim “copiat”, i des del qual “enganxem”. Per exemple, quan
seleccionem un text i fem “Ctrl+C” per copiar-lo i “Ctrl+V” per
enganxar-lo.

![](enigmail-051.png)

Ara obrim un editor de text qualsevol, o simplement comencem a escriure un
missatge al propi Icedove/Thunderbird, i enganxem el contingut del
porta-retalls, que és la clau pública en sí, el que es veu a la imatge
de la dreta d'aquesta pàgina:

Doncs el mètode per agafar una clau pública a partir del porta-retalls
és simplement copiar el text de la clau pública, tal com la tenim a la
imatge, i anar a l'”Enigmail Key Management” i clicar a “Import Keys
from Clipboard” (“Importar Claus des del Porta-retalls”):

![](enigmail-052.png)

Aleshores surt l'avís que ens pregunta si realment ho volem fer, i hi
cliquem a “Import”:

![](enigmail-053.png)

I surt l'avís que diu com ha anat la importació:

![](enigmail-054.png)

En aquest cas, com que ja teniem la clau pública de “prova@prova.prova”, no
hi ha hagut cap canvi, però ara ja sabem com importar manualment una
clau pública des del porta-papers!

A part, a la [opció A](#15OpcioA) hem explicat com xifrar i signar
missatges, per tant, ja podem començar a mantenir converses per correu
electrònic realment privades!

 <span id="anchor-18"></span>Consells:

Utilitzar comptes de correu de servidors segurs!

Fer còpies de seguretat de les claus!

Destruir les dades de forma segura!

Fer servir contrassenyes el més segures possible!

Xifrar i signar sempre!

No posar informació comprometedora als assumptes dels missatges xifrats!


