---
title: 'Sincronització de calendaris digitals'
published: false
metadata:
    Keywords: 'calendari, calendar, caldav, davdroid, thunderbird, lightning, nextcloud'
---

## Objectiu

Configurar un calendari remot de la Lleialtat al portàtil i al mòbil per rebre avisos, llegir i escriure activitats més còmodament que a través de la web del núvol. Tot aquest procediment és orientat a persones amb usuària al núvol de Lleialtat, però el funcionamet és equivalent a altres instaŀlacions de Nextcloud i aplicacions que segueixin els mateixos estàndards. Si vols aconseguir un compte de Nextcloud, pots consultar la nostra [llista d'alternatives lliures](../formacio/eines-lliures-per-la-privacitat-i-la-llibertat).

## Calendaris digitals

L'estàndard d'internet per a calendaris ([ICS](https://ca.wikipedia.org/wiki/ICalendar)) permete que diverses aplicacions puguin entendre's describint activitats, reunions o trobades amb lloc, hora, recordatoris i confirmació d'assistència. És un simple arxiu de text que es pot adjuntar als correus electrònics, o que pot servir per importar i exportar esdeveniments entre aplicacions. Hi ha un estàndard que permet desar calendaris en un [servidor](https://ca.wikipedia.org/wiki/Client-servidor) i que diverses aplicacions [clients](https://ca.wikipedia.org/wiki/Client-servidor) puguin afegir-hi, editar o esborrar cites, el [CalDAV](https://ca.wikipedia.org/wiki/Client-servidor).

## A la Lleialtat

El [núvol de la Lleialtat](https://n.lleialtat.cat), que és una instaŀlació de l'aplicació web [Nextcloud](https://n.lleialtat.cat), a part de repositori i sincronització de documents, també permet la gestió de calendaris compartits, així com la sincronització i edició remota. És compatible amb els estàndards "DAV": WebDAV pels fitxers, CardDAV pels contactes, CalDAV pels calendaris. L'agenda pública d'activitats és accessible a través del núvol amb aquest protocol, així com la de lleialtec. Això permet que els diversos grups dins de la CELS comparteixin calendaris amb permisos d'edició per a membres o admins, o de només de lectura per a la resta.


## Primer pas: Consultem el calendari al núvol

Entrem al núvol.

![núvol-1](n-1.png)

Cliquem a la icona de calendari en la barra superior.

![núvol-2](n-2.png)

No ens apareix cap calendari, així que n'hem d'activar. Aquí farem servir el de lleialtec, disponible almenys per a membres de lleialtec, però podríem activar el calendari "agenda-lleialtat", disponinble per a tota la CELS. Per a veure'l a la graella, cliquem a sobre del seu nom.

![núvol-3](n-3.png)

… i veiem que ens apareixen les properes activitats de lleialtec.

![núvol-4](n-4.1.png)

També podem veure que aviat hi haurà una install party!

![núvol-6](n-6.png)

Ens podríem quedar aquí, però volem tenir accés als calendaris sense haver d'obrir el navegador. A les aplicacions clients els necessitarem donar les credencials (usuària i contrasenya) i un enllaç especial. Per trobar aquest enllaç fem clic al menú del calendari en concret i després cliquem a "Enllaç". Segur que serà més llarg que la caixeta, és imporant tenir-ho en compte a l'hora de copiar-lo.

![núvol-4](n-4.2.png)

![núvol-5](n-5.png)

## Calendari sincronitzat al mòbil

Qui fa servir Google pot estar acostumat a tenir-ho tot sincronitzat entre dispositius sense haver de configurar res, però això té un preu molt alt, entre el que hi ha la nostra llibertat. Per exemple, no podem fer servir la nostra infraestructura (com el núvol) i a més sempre en té una còpia sense xifrar per analitzar-la i poder-nos manipular a nosaltres i les persones del voltant. A més, al seu sistema operatiu de mòbils, Android, no hi ha per defecte l'estàndard obert CalDAV. Per tal que l'aplicació de calendari que tinguem instaŀlada mostri les entrades que tenim en remot al núvol, necessita una aplicació que sàpiga parlar CalDAV i li tradueixi la informació. Aquesta app, a Android es diu [DAVDroid](https://fossdroid.com/a/davdroid.html).

### Part 1: instaŀlem DAVDroid

Si tens F-Droid instaŀlada al mòbil, pots saltar-te aquest paràgraf. Descarreguem el rebost d'aplicacions lliures per a Android des del seu [lloc web](https://f-droid.org), a través del navegador web del mòbil. Comprovem que hi estem accedint a través d'HTTPS, amb la S, i que no té cap avís de seguretat (no volem instaŀlar-ne cap imitació!). L'adreça de descàrrrega és https://f-droid.org/FDroid.apk.

![fdroid-1](f-1.png)

Obrim f-droid, i si és la primera vegada que ho fem, busquem com actualitzar els dipòsits. Això vol dir que l'aplicació de bones a primeres no sap quines aplicacions hi ha disponible, i que ho ha de preguntar a cada dipòsit configurat (per defecte el propi del projecte FDroid i el de GuardianProject). Un cop actualitzada, ens sortirà una llista de novetats i aplicacions disponibles lliures per instaŀlar gratuïtament i amb garanties de seguretat i privacitat.

Busquem una pàgina com la següent. Si ens en surt una d'aparença més simple, pot ser que haguem d'actualitzar f-droid a través d'ella mateixa (quin lio!).

![fdroid-2](f-2.png)

Cerquem alguna cosa com "caldav", "davdroid" o "calendar", i seleccionem l'aplicació DAVDroid.

![fdroid-3](f-3.png)

![fdroid-4](f-4.png)

### Part 2: configurem DAVDroid

Exectucada l'aplicació, ens sortirà una pantalla demanant que contribuïm econòmicament al projecte. Si teniu un sou fix és una bona idea fer-ho a la següent nòmina, un cop hagueu comprovat com de necessària és. Després, veurem una pantalla buida. En el meu cas, ja hi tenia configurat un altre compte.

![davdroid-1](dav-1.png)

Toquem la creu per afegir un compte nou. Aquí farem servir una versió curta de l'enllaç especial que hem trobat abans al núvol, més l'usuària i contrasenya també del mateix núvol. En el nostre cas, serà `https://n.lleialtat.cat/remote.php`. En altres instaŀlacions de Nextcloud, només caldria canviar el domini, però altres servidors compatibles amb CalDAV (com Baikal), segurament tindran adreces URL diferents. 

![davdroid-2](dav-2.png)

Un cop ens hem pogut autenticar, hem de posar-li un nom local al compte. Si volem fer servir la funcionalitat de convidar altres persones a reunions o activitats, és important que aquest nom sigui un correu nostre vàlid. Sinó, un nom simple com "personal" o el vostre nom ja val.

![davdroid-3](dav-3.png)

Un cop acabats, ens porta a la pantalla principal on podem comprovar que tenim un compte més configurat!

![davdroid-4](dav-4.png)

Per tal d'activar-lo, toquem el rectangle i ens mostrarà una pantalla amb la llista de calendaris i llibretes de contactes disponibles al servidor.

![davdroid-5](dav-5.png)

El que farem serà activar només un calendari, en aquest cas, el de lleialtec, i forçar una primera sincronització manual. Per a fer-ho hem de tocar la icona d'actualització, el cercle format per dues fletxes en arc.

![davdroid-6](dav-6.png)

Aquest procés podria trigar uns quants segons o fins i tot minuts si el calendari és molt i molt gran. És important assegurar-nos que s'ha acabat de sincronitzar abans de buscar resultats.

### Pas 3: Comprovem els resultats amb una app de calendari

Ara qualsevol aplicació de calendari que ho suporti, podrà llegir el calendari "lleialtec", que és el que hem decidit seguir. Si l'aplicació per defecte del vostre Android no funciona bé o no té aquesta opció, podeu instaŀlar [Simple Calendar](https://fossdroid.com/a/simple-calendar.html) des de F-Droid. Només cal activar l'opció "CALDAV sync" als "Ajustaments" de l'aplicació i triar quin dels calendaris disponibles volem fer servir. No obstant això, jo he fet servir l'aplicació de calendari per defecte de LineageOS, una versió d'Android comunitària i respectuosa amb les llibertats dels usuaris.

Obrint-la, ja podem veure les properes activitats de lleialtec. Comprovem que veiem la mateixa [Install Party](https://www.lleialtat.cat/install-party-surt-amb-el-gnu-linux-instal-lat/) programada per dissabte i una presentació del Fedivers a la [jornada organitzada per Caliu](https://caliu.cat/dlp2018/)!

![andcal-1](andcal-1.png)

![andcal-2](andcal-2.png)

![andcal-3](andcal-3.png)

## Calendari sincronitzat al portàtil

Igual que hem fet amb Android, volem una aplicació que sàpiga fer de client de CalDAV per poder parlar amb el núvol, que fa de servidor del mateix protocol. Ho farem amb una extensió del Thunderbird/Icedove, el client de correu, però es podria fer amb l'aplicació de Calendari de Gnome, amb la de KDE, o amb qualsevol altra que implementi aquest protocol. Des de lleialtec us recomanem configurar-vos el Thunderbird per més motius relacionats amb el correu electrònic.

### Instaŀlem el complement de calendari: Lightning/Iceowl

Obrim el Thunderbird, cliquem al menú i obrim la pàgina de "Complements".

![thunderbird-1](th-1.png)

Cerquem per "calendar" o "lightning" a "Complements disponibles" i l'instaŀlem, així com el paquet d'idioma que preferim. Aquesta instaŀlació també es podria fer a través del vostre gestor de paquets de la vostra distribució de Linux.

Un cop instaŀlada l'extensió, podem obrir-la clicant la icona de calendari que ha aparegut a la dreta de la barra superior, o bé anant al menú i clicant a "cites i tasques" → "calendari".

![thunderbird-2](th-2.png)

Un cop oberta, veiem la graella i espais en blanc. En aquest cas he deshabilitat i pixelat els calendaris que ja tenia afegits.

![thunderbird-3](th-3.png)

Llavors, fem clic dret a l'espai en blanc amb títol "Calendari" i seleccionem "Calendari nou".

![thunderbird-4](th-4.png).

Ens pregunta si volem que el calendari sigui en local, desconnectat, o si s'ha de sincronitzar per xarxa. Cliquem "a  la xarxa)

![thunderbird-5](th-5.png)

Aquí li hem de dir l'estàndard que farem servir, que ja sabem que és CalDAV, i a més li haurem de dir les dades necessàries per accedir al calendari. Ara és important que al camp de "Ubicació" hi introduïm l'enllaç sencer que apareix al núvol, no només la versió curta acabada amb "remote.php". Serà una cosa així com `https://domini.tld/remote.php/dav/calendars/nom-usuaria/nom-calendari/`

![thunderbird-6](th-6.png)

Seguidament, ens demanarà un nom per al calendari, un color per a identificar-ne les activitats visualment, i un correu electrònic per associar al calendari. Tot i que podria ser que canviés, ara mateix el Lightning fa servir el correu electrònic del compte del núvol en comptes del correu que li posem aquí. En tot cas, altre cop: només és un problema si tenim diversos comptes de correu sincronitzats i si volem fer servir la funció de convidar contactes a activitats a través del correu.

![thunderbird-7](th-7.png)

Un cop sincronitzat, apareixeran els propers esdeveniments automàticament amb el color escollit. Tenim el format de graella que es pot configurar com a mes complet, diverses setmanes, setmana o dia, i el panell format agenda, que llista per odre de data d'inici un esdeveniment sota l'altre.

![thunderbird-8](th-8.png)

## Comprovant la sincronització

Idea: modifiquem una activitat en una aplicació (la web, la del mòbil o la del portàtil) i comprovem els canvis en les altres.

Modificarem l'esdeveniment de la Install Party des del Thunderbird, al portàtil, i per fer-ho, farem doble clic sobre ella en la graella o en la llista-agenda. Ens apareixerà una pantalla amb les dades actuals: títol, lloc, hores i dates d'inici i fi.

![thunderbird-9](th-9.png)

Però no ens explica res més. Així que busquem el text de descripció de l'activitat i l'enganxem. Cliquem a "Desa" i tanquem.

![thunderbird-10](th-10.png)

Seguidament, obrim la web del núvol al navegador, i visitem el calendari. Tornem a veure l'activitat en concreta, hi cliquem un cop i després a "Més". Ja podem veure el camp de descripció amb la informació que acabem d'afegir-hi! Això vol dir que les altres subscriptores al calendari també rebran de seguida aquests canvis.

![nuvol-6](n-6.png)
![nuvol-8](n-8.png)

Des del mòbil, si han passat uns 10 minuts o bé si hem forçat una actualització, podem obrir l'activitat del calendari i també hi podem veure la nova informació!

![andcal-4](andcal-4.png)

Cal tenir en compte que els canvis que realitzem en un client es pujaran al moment al servidor, però els canvis només arriben als altres clients quan aquests pregunten al servidor per canvis. L'interval de consulta és configurable almenys en aquestes dues aplicacions (calendari de LineageOS i de Thunderbird) i sol fixar-se a 10, 15 o 30 minuts. A diferència de les aplicacions corporatives, aquí podem decidir quant ens comuniquem amb els servidors i per què.

