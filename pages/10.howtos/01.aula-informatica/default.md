---
title: 'Aula informàtica'
taxonomy:
    category: docs
    tag: ''
visible: true
---

### FogProject
* [https://wiki.fogproject.org/wiki/index.php](https://wiki.fogproject.org/wiki/index.php)
* [https://github.com/FOGProject/fogproject](https://github.com/FOGProject/fogproject)

Learn about network booting [http://networkboot.org/](http://networkboot.org/)

Hay dos modalidades de instalación.

1. Fog server with one interface.

[https://wiki.fogproject.org/wiki/index.php?title=Using_FOG_with_an_unmodifiable_DHCP_server/_Using_FOG_with_no_DHCP_server](https://wiki.fogproject.org/wiki/index.php?title=Using_FOG_with_an_unmodifiable_DHCP_server/_Using_FOG_with_no_DHCP_server)

_Explicar diferencia_

2. Fog server with two network cards


Vamos a instalar un servidor Fog con dos tarjetas de red. Una en la red existente, y otra como gateway de la red nueva del aula que crearemos.

#### Sistema oprtativo
Bajar e instalar debian8. No instalado debian9 porque hemos escogido instalar FogProject estable y requiere php5. Debian9 lleva php7, y falla la installfog.sh

Guardamos una partición a parte para las imágenes

![](fog%20disk%20partitions.png)

#### Download and Install Fog

```
wget http://downloads.sourceforge.net/project/freeghost/FOG/fog_1.2.0/fog_1.2.0.tar.gz
tar xzf fog_1.2.0.tar.gz
cd fog_1.2.0/bin
./installfog.sh
```

Cuando termine la instalación, configura la seguna red

edit /etc/network/interfaces

```
# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug eth0
iface eth0 inet dhcp

auto eth1 
iface eth21 inet static
address 192.168.2.1
netmask 255.255.255.0
#dns-nameservers 8.8.8.8 
```

El fogserver ip será el 192.168.2.1 y el gateway de los PCs del aula será el 192.168.2.1

##### Install fog
[https://wiki.fogproject.org/wiki/index.php?title=Installation#Installer](https://wiki.fogproject.org/wiki/index.php?title=Installation#Installer)

```
cd /opt
wget http://downloads.sourceforge.net/project/freeghost/FOG/fog_1.2.0/fog_1.2.0.tar.gz
tar xzf fog_1.2.0.tar.gz
cd fog_1.2.0/bin
./installfog.sh
```

```
System type? Debian [2]
What type of installation would you like to do? [N]
What is the IP address to be used by this FOG Server? 192.168.2.1
Would you like to setup a router address for the DHCP server? [Y/n] Y
What is the IP address to be used for the router on the DHCP server? [] 192.168.2.1
Would you like to setup a DNS address for the DHCP server and client boot image? [Y/n] Y
What is the IP address to be used for DNS on the DHCP server and client boot image? [80.58.61.250] (este IP nos sale automáticamente)
If you are not sure, select No. [y/N] y
What network interface would you like to use? eth1
Would you like to use the FOG server for DHCP service? [Y/n] Y
This version of FOG has internationalization support, would  
  you like to install the additional language packs? [y/N] y
Would you like to donate computer resources to the FOG Project? [y/N] (como veas)
Are you sure you wish to continue (Y/N) Y
```

El servidor Fog es además el router del aula. Todo el trafico que generan los PCs hacía el exterior pasan por ello. Hace falta configurar NAT.

Edita /etc/firewall.sh

```
/sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
/sbin/iptables -A FORWARD -i eth0 -o eth1 -m state  --state RELATED,ESTABLISHED -j ACCEPT
/sbin/iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT
```

```
chmod +x /etc/firewall.sh
```

Haz que se cargan cuando bota el servidor.

Edita /etc/rc.local y añade al finar

```
/etc/firewall.sh
exit 0
```

Ahora se instala fog bajando los paquetes que faltan.

Si ha funcionado puedes empezar a gestionar los PCs

Login via la interfaz web de gestión.

Usa `ip add sh eth0` para averiguar el IP externo del servidor y conectar a `http://<ip_servidor>/fog`

Los PCs conectados a la red del aula pueden acceder a http://192.168.2.1/fog

### El BIOS

Hay que repasar la configuración del BIOS en todos los PCS asegurando que tienen arranque por red. Busca parámetros como NIC/PXE, WakeOnLAN, Remote server.

Cada BIOS es un mundo. Paciencia!

![](checking-bios.jpg)

