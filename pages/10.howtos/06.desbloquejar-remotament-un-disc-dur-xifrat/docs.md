---
title: 'Desbloquejar remotament un disc dur xifrat'
published: false
taxonomy:
    category:
        - docs
visible: true
---

Context
=======
Treballarem sobre una màquina amb Debian orientada a fer de servidor. En altres distribucions, recomanem llegir igualment el readme de Debian per a cryptsetup "README.remote", que es pot trobar [aquí](http://ftp.es.debian.org/debian/pool/main/c/cryptsetup/cryptsetup_1.7.2-5_amd64.deb)

Assumim una instaŀlació "full disk encryption" (FDE) on al disc dur hi ha només dues particions: una per a boot, en clar, on hi instaŀlarem un servidor ssh lleuger, el Dropbear, i l'altra xifrada amb LUKS. Vegeu ["full disk encryption"](article-nou.md) per a aconseguir aquest entorn.

Motivació
=========
Xifrar els discos durs dels servidors és essencial per garantir la confidencialitat en cas de patir un atac amb accés físic al maquinari, com un robatori. El secret criptogràfic en una instaŀlació així resideix en la contrasenya amb què es xifra el disc, i per tant, no pot estar al mateix disc en clar, és obvi. Això demana introduir el secret a cada arrencada, i per tant, a priori presència física.

Tanmateix, els servidors autogestionats no són immunes a la inestabilitat de la xarxa elèctrica i és habitual que caiguin. Alhora, hi ha actualitzacions i situacions que requereixen reiniciar la màquina, i és desitjable poder-ho fer a distància. L'objectiu, per tant, és poder arrencar un servidor caigut remotament, sense haver d'accedir a la ubicació física amb tot el cost de temps en desplaçament i d'organització per l'admin i la indisponibilitat del servei per les usuàries.

Solució
=======
Instaŀlar un petit servidor ssh al sistema operatiu temporal contingut a la partició boot. Si hi podem accedir, podrem introduir el secret a distància i així iniciar l'arrancada habitual del sistema operatiu complet.

Part I: BIOS
============
Per tal de poder-nos recuperar de caigudes d'electricitat, ens hem d'assegurar que un cop estabilitzat el subministrament elèctric, l'ordinador es tornarà a engegar. És possible que la BIOS de la teva màquina no ofereixi aquesta opció, i per tant, restar vulnerable a fallades elèctriques. Tot i així per a reboots voluntaris la Part II continua sent d'utilitat. 

Mètode
------
Reinicia l'ordinador i accedeix a la configuració de la BIOS. En cas que sigui reciclat i demani una contrasenya que desconeguis, consulta [com reiniciar la contrasenya de la BIOS](article-nou). En el nostre ordinador la marca és "American Megatrends", de l'any 2006. Podem trobar l'opció dins de "Power Manager Setup" --> "Restore on AC Power Loss". D'entre les opcions "On/Off/Last State" triem "On". Després salvem la configuració polsant F10 i confirmant "Ok".

Comprovació
-----------
Des d'algun menú de la BIOS o al menú de selecció del Grub, desconnecta el cable d'alimentació de la torre i torna'l a connectar. El resultat esperat és que s'engegui sol de nou, carregui el Grub, triï l'entrada desitjada (Debian en mode normal) automàticament o al cap d'uns segons, i acabar rebent el missatge "Please unlock disk sdXY_crypt:", on X serà una lletra i Y un número natural. Si no es compleix això, no podràs desbloquejar el teu servidor després d'una caiguda elèctrica fent servir aquest mètode.

Part II: Dropbear
=================
Aquesta part està basada en la documentació que Debian aporta sobre cryptsetup, en concret el fitxer '/usr/share/doc/cryptsetup/README.remote'. En cas de dubte o incompletesa, consulteu aquesta guia. S'instaŀla com a part del paquet "cryptsetup".

Requeriments
------------
Necessitem:

 * dropbear: servidor ssh lleuger. Hi establirem una connexió remota xifrada.
 * busybox: conjunt d'eines Unix equivalent al GNU Coreutils. Ens ofereix la interactivitat i les eines d'entrada sortida necessàries per introduir la clau.

Així que fem
```
apt update
apt install busybox dropbear
```

Situació inicial
----------------

Quan hagi acabat d'instaŀlar dropbear, aquest haurà:

 * generat un parell de claus RSA i DSS, per a poder xifrar la connexió ssh.
 * afegit a si mateix al initramfs i generat la nova imatge, per tant es carregarà automàticament.
 * generat un parell de claus per a l'usuària root.

Clau d'usuari
-------------

Per tal que dropbear ens otorgui accés a una consola, necessitem un certificat ssh. No suporta autenticació per contrasenya i, en tot cas, seria molt mala idea intentar-ho. Per a tal efecte, podem fer servir el que ja ha generat dropbear i ha deixat a '/etc/initramfs-tools/root/.ssh/id_rsa'. Tanmateix, podem generar el nostre propi i pujar-lo al servidor.

Generem la clau en local:

```
user@local:~$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user/.ssh/id_rsa): /home/user/.ssh/server-unlock_id-rsa
Enter passphrase (empty for no passphrase): CONTRASENYA GENERADA A PARTIR DE MOLTES PARAULES I QUE DESAREM A UN GESTOR DE CONTRASENYES
Enter same passphrase again: LA MATEIXA CONTRASENYA
Your identification has been saved in /home/user/.ssh/server-unlock_id-rsa.
Your public key has been saved in /home/user/.ssh/server-unlock_id-rsa.pub.
The key fingerprint is:
3f:49:24:ca:9a:f6:55:f7:8c:58:96:db:ef:ab:7b:04 user@local
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|                 |
|        . .      |
|     . . o   E   |
|      o S o = .  |
|     o   + * * . |
|    +   . = o =  |
|   . . .   .   o |
|      .      o+o+|
+-----------------+

```
Nota: aquesta comanda ha generat dos fitxers, `server-unlock_id-rsa` (clau privada) i `server-unlock_id-rsa.pub` (clau pública). Al servidor hi hem de penjar la pública, i ens hem d'assegurar de mantenir la privada sota clau.

Si volem evitar que aparegui el nostre nom d'usuària i de la màquina local a la clau, ho podem esborrar sense problema amb un editor de text. Si la clau pública generada té una pinta així:
```
user@local:~$ cat /home/user/.ssh/server-unlock_id-rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCt1fAYcDx98Yv8CH7m+Y7CnMF6GXqiOWLifutvRCYPH3VZt0UDwnujJGjcWxv4ti1EMTcLj39X1hWhrGx3XQxHf8bxkD5JdC65J/NzEGrLR6KkLi/BJuhNIL8rOdDAMhFzM3AGXjkkEMCWSArX4YCqFFOux3zvDlIrDUFwzfbbjmWhe/CHJm6cl/2AaaWshXz5blmP6ISQCXgKfyHrhrrybzBCyyEfeheHpXimY2k0mh+CGpRf8xIWeHGU2JfO5VffW0E5qyz4G6ipXV9RmT94DmW1pRH/p6YDk1YnBVtqaLmSMo+GYMfHXqkFoT98dSCGvRI3V0S6BQoEXxeZNGJb user@local
```
Li podem treure rastre deixant-la així:
```
user@local:~$ cat /home/user/.ssh/server-unlock_id-rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCt1fAYcDx98Yv8CH7m+Y7CnMF6GXqiOWLifutvRCYPH3VZt0UDwnujJGjcWxv4ti1EMTcLj39X1hWhrGx3XQxHf8bxkD5JdC65J/NzEGrLR6KkLi/BJuhNIL8rOdDAMhFzM3AGXjkkEMCWSArX4YCqFFOux3zvDlIrDUFwzfbbjmWhe/CHJm6cl/2AaaWshXz5blmP6ISQCXgKfyHrhrrybzBCyyEfeheHpXimY2k0mh+CGpRf8xIWeHGU2JfO5VffW0E5qyz4G6ipXV9RmT94DmW1pRH/p6YDk1YnBVtqaLmSMo+GYMfHXqkFoT98dSCGvRI3V0S6BQoEXxeZNGJb
```
En cas que el servidor quedès compromès o fos sotstret, la clau pública no revelaria el nom d'usuari de la nostra estació de treball, fet que ens podria identificar-nos unívocament. Us animem a coŀlaborar en aquesta secció per evitar deixar altres rastres que puguin comprometre la nostra privacitat: Dropbear escriu logs amb IP? hora?

Un cop generada la clau a la nostra estació i opcionalment "netejada", és el moment de pujar-la al servidor perquè la reconegui com a vàlida quan vulguem autenticar-nos. Podem fer-ho amb scp o amb l'eina interactiva sftp: 
```
user@local:~$ sftp admin@server.host.name
Connected to server.host.name.
sftp> put .ssh/server-unlock_id-rsa.pub 
Uploading .ssh/server-unlock_id-rsa.pub to /home/admin/server-unlock_id-rsa.pub
.ssh/server-unlock_id-rsa.pub                                                                                                      100%  381     0.4KB/s   00:00    
sftp> bye
```
Un cop tenim la clau al servidor, ens connectem al servidor, canviem a root i afegim la clau al fitxer de claus de confiança del dropbear.
```
root@server:/home/admin# cat server-unlock_id-rsa.pub >> /etc/initramfs-tools/root/.ssh/authorized_keys
```
Configuració de initramfs
-------------------------
### Mòduls
Per tal que el sistema inicial arrenqui amb els programes que volem, li hem d'indicar. Afegim si falten dues línies a `/etc/initramfs-tools/initramfs.conf`
```
BUSYBOX=y
DROPBEAR=y
```
En el nostre cas només faltava la línia de dropbear.

### Xarxa
Normalment, el sistema operatiu configura la xarxa en un dels passos del sistema d'inici, i en el cas d'una FDE, això implica després d'haver desbloquejat el disc dur. Per tant, si volem accedir al nostre servidor abans que arrenqui el sistema operatiu sencer, hem de replicar la configuració de xarxa per al initramfs.

En cas que la IP del servidor l'adjudiqui un servidor de DHCP de la xarxa local, hem de posar en aquesta opció el nom de la interfície de xarxa per la qual ha de demanar el DHCP. En aquest cas, afegirem al mateix fitxer de configuració:

```
DEVICE=eth0
```
En cas que la configuració sigui estàtica, tant IP com DNS, llavors hem de posar l'opció en format de [paràmetre d'arrencada del kernel](https://www.kernel.org/doc/Documentation/kernel-parameters.txt). Aquests paràmetres són els que apareixen a `/boot/grub/grub.cfg`. En aquest cas, la configuració podria ser:
```
# https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt
# ip=<client-ip>:<server-ip>:<gw-ip>:<netmask>:<hostname>:<device>:<autoconf>:<dns0-ip>:<dns1-ip>
ip=192.0.2.100::192.0.2.1:255.255.255.0::eth0:off
```
En resum, li diem quina IP hem de tenir, el gateway o pasareŀla IP, la màscara i la interfície a la qual aplicar aquesta configuració, respectivament.

### Apliquem els canvis
Després de desar `initramfs.conf`, hem de tornar a generar la imatge amb la nova configuració.

Scripts i configuració d'usuari
-------------------------------

FIXME

Comprovació
-----------

FIXME

Comentaris

- certificat servidor ssh diferent
- FIXME