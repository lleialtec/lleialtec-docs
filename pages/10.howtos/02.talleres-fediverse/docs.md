---
title: 'Talleres fediverse'
published: false
taxonomy:
    category:
        - docs
visible: true
---

Objectivos:
* Aprender a instalar y mantener nodos
* Buscar maneras de compartir infraestructura
* Crear una red de apoyo mutuo.