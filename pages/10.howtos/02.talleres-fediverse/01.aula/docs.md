---
title: 'El Aula'
---

Los PCs del [aula informática](https://tec.lleialtat.cat/howtos/aula-informatica) están gestionados por el sistema de imágenes del fogProject. Esto nos permite guardar sistemas operativas instalados y configurados.

### DNS
Necesitamos que cada PC (que hará de nodo) tenga un domain name. En el fog server instalarmos un DNS.

```
apt-get install dnsmasq
```
edit `/etc/dnsmasq.conf`
```
domain-needed
bogus-priv

domain=lleialtat.dev
expand-hosts
local=/lleialtat.dev/

listen-address=127.0.0.1
listen-address=192.168.2.1
bind-interfaces
```


edit `/etc/hosts`
```
192.168.2.13    pc13
192.168.2.14    pc14
192.168.2.15    pc15
192.168.2.16    pc16
192.168.2.17    pc17
192.168.2.18    pc18
192.168.2.19    pc19
192.168.2.20    pc20
192.168.2.21    pc21
192.168.2.22    pc22
192.168.2.23    pc23
192.168.2.24    pc24
192.168.2.25    pc25
192.168.2.26    pc26
192.168.2.27    pc27
192.168.2.28    pc28
192.168.2.29    pc29
```

```
service dnsmasq restart
```

Ahora podemos `ping pc29.lleialtat.dev`

En los PC podemos crear un script `/usr/local/bin/mydomain`
```
#!/bin/bash

IP="$(ip route get 1 | awk '{print $NF;exit}')"

host="$(cut -d "." -f 4 <<< $IP)"
host="pc$host.lleialtat.dev"

echo $host > /proc/sys/kernel/hostname
# hostnamectl set-hostname $host # optionally if dbus is installed 
```
Y hacerlo ejecutable
```
chmod +x /usr/local/bin/mydomain.sh
```
