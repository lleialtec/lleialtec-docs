---
title: 'Wifi access points'
taxonomy:
    category: docs
    tag: ''
visible: true
---

*Este documento es un derivado de [Despliegue sencillo de wifi para un local](https://github.com/guifi-exo/wiki/blob/master/howto/wifi.md) de [guifinet/eXO](https://github.com/guifi-exo), usado bajo [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). "Wifi access points" está licenciado consecuentemente bajo [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) por [lleailtec](https://gitlab.com/lleialtec).*

## Cómo dar cobertura wifi en el edificio

Despliegue sencillo de wifi para un local.

Requisitos mínimos: un router con conexión a Internet, y unos cuantos wifi-routers diversos que cada uno tiene por casa.

Disclaimer: El modelo de routers reciclados es una forma de facilitar wifi, pero en las zonas de alta concentración conviene estudiar otras formas de wifi. Un router doméstico da servicio a unos 10-25 dispositivos de aquella manera. No se recomienda superar la regla de los 30 dispositivos por AP doméstico.

Para cada router:

* Instalar [OpenWrt](https://openwrt.org/), [LEDE](https://lede-project.org/) o [dd-wrt](https://www.dd-wrt.com) buscando el software del router en sus bases de datos: [OpenWrt](https://wiki.openwrt.org/toh/start), [LEDE](https://lede-project.org/toh/start), [dd-wrt](https://www.dd-wrt.com/site/support/router-database)
* Averiguar cuál es su IP de entrada y sus datos de acceso (más info: adslzone.net o bandaancha.eu), normalmente es http://192.168.1.1
* Cambiar la IP por una que no pertenezca a ninguna red existente. ej 192.168.10.10 y escribirlo en el router para no olvidarlo (si haces un segundo router, asígnalo a la 192.168.10.11 p.e.)
* El mismo ESSID, por ejemplo guifi.net-CentroSocialLaPalma El ESSID se comparte para que el sipositivo conectado pueda cambiar de un Access Point a otro de manera transparente
* Contraseña sí, no? Lo mismo para todos
* Seleccionar diferentes canales wifi libres (cuanto más espacio entre ellos, mejor). Hay 14, es posible [analizar niveles de señal con el móvil](https://github.com/VREMSoftwareDevelopment/WiFiAnalyzer)
* Deshabilitar el DHCP, y conectarlos al punto central que da Internet por la LAN: uno de los 4 conectores amarillos (puede haber variaciones, no son 4, no son amarillos, ...), pero no por el conector a Internet, WAN, Azul. Esto dificultará el acceso al wifi-router pero de esta forma no interferirá en absoluto con otras redes. Cuando nos haga falta acceder a este router en concreto necesitaremos direccionamiento estático para acceder, en GNU/Linux y por consola sería algo así:

Al cambiar de subred el router (y sin que éste pueda proporcionarte una IP por DHCP), dejarás de tener acceso. Deberás cambiarte a la subred del router:
```
sudo /etc/init.d/network-manager stop
sudo ip add add 192.168.10.X/24 scope global dev <interfaz>
```

Ahora podrás navegar al router http://192.168.10.10

Enchufa el router en uno de las bocas (en este caso) amarillas. No usamos la boca que normalmente se usa para la entrada de Internet.

Con este montaje, el encargado de dar IPs a los dispositivos wifi que se conectan a los APs es el router con salida a Internet. Conviene asegurarse de que es solvente como servidor DHCP y de que los rangos de IPs son suficientes.


## Utilidades

`iw` es el comando de mayor utilidad, tanto para [escanear](https://wireless.wiki.kernel.org/en/users/documentation/iw#scanning) como para [monitorear y debuggar](https://wireless.wiki.kernel.org/en/users/documentation/iw#modifying_monitor_interface_flags)

Ver si una tarjeta wifi soporta modo monitor:
```
iw list | grep monitor
```

Para evaluar la situación de las diferentes redes wifi usa la aplicación `linssid`

## ssh

Si dejaste habilitado el acceso ssh con contraseña (ten cudidado con esto) o bien añadiste tu clave ssh a las permitidas por el router:
```
$ ssh root@192.168.10.10


BusyBox v1.25.1 () built-in shell (ash)

     _________
    /        /\      _    ___ ___  ___
   /  LE    /  \    | |  | __|   \| __|
  /    DE  /    \   | |__| _|| |) | _|
 /________/  LE  \  |____|___|___/|___|                      lede-project.org
 \        \   DE /
  \    LE  \    /  -----------------------------------------------------------
   \  DE    \  /    Reboot (17.01.1, r3316-7eb58cf109)
    \________\/    -----------------------------------------------------------

root@LEDE:~# 

```

### LEDE config

**System -> Administration** 

Router Password

Save and apply.


**Network -> Interfaces**

Edit LAN

```
Cambia IPv4 address
```

DCHP Server
```
Disable DHCP for this interface.
```

IPv6 Settings
```
Router Advertisement-Service [Disable]
DHCPv6-Service [Disable]
NDP-Proxy [Disable]
```

Advanced settings
```
Use builtin IPv6-management [Disable]
```
Save and apply.

Ahora conecta de nuevo al IPv4 que has asignado


**Network -> Wireless**

Edit wireless network

Device Configuration -> Advancedd settings
```
Country code ES
```

Interface Configuration -> General setup
```
ESSID lleialtat-santsenca
```

Wireless security

Interface Configuration -> Encryption
```
unencrytped (porque no ponemos contraseña a la red)
```
Save and apply.

**Network -> DHCP and DNS**

Server Settings -> General settings
```
Domain required [Disable]
Authoritative [Disable]
Log queries [Disable]
Rebind protection [Disable]
Local Service Only [Disable]
Non-wildcard [Disable]
```
Server Settings -> Resolve and Hosts files
```
Use /etc/ethers [Disable]
```
Server Settings -> Advanced setting
```
Filter private  [Disable]
Localise queries  [Disable]
Expand hosts  [Disable]
```
Save and apply.

**Network -> Firewall -> Traffic Rules**
```
Allow-DHCP-Renew [Disable]
Allow-Ping [Enable]
Allow-IGMP [Disable]
Allow-DHCPv6 [Disable]
Allow-MLD [Disable]
Allow-ICMPv6-Input [Disable]
Allow-ICMPv6-Forward [Disable]
Allow-IPSec-ESP [Disable]
Allow-ISAKMP [Disable]
```
Save and apply.

**Network -> Wireless**
```
Wireless network is disabled [Enable]
```