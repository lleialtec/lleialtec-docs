---
title: 'ERP del Cafè'
taxonomy:
    category: docs
    tag: ''
visible: true
---

La documentación del TVP del Cafè de la Lleialtat está hecho con [Dolibarr](https://www.dolibarr.org/), un LAMP.

[Aquí la documentación](https://n.lleialtat.cat/s/y9HqosBZXfoFRKP) que estamos trabajando.

Actualmente valoramos la confección de un "meta" módulo Dolibarr que instale y configure la instalación.

Para saber más: info@tec.lleialtat.cat

