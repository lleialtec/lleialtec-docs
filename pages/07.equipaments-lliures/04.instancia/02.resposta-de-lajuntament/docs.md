---
title: 'Resposta de l''Ajuntament'
taxonomy:
    category:
        - docs
visible: true
---

Aquesta va ser la resposta de l'Ajuntament a la [instància](https://tec.lleialtat.cat/equipaments-lliures/instancia) presentada des de la CELS:

_Benvolgut ciutadà / benvolguda ciutadana,_

_Responem a la vostra comunicació rebuda el 16 de gener de 2019, amb codi 8113VVL sobre consultes sobre la web de l’IMI._

_Us informem que una gran part dels documents que es publiquen en els portals de l’Ajuntament de Barcelona estan en format PDF, format obert i inclòs a l’esquema nacional d’interoperabilitat._

_Actualment disposem d’un elevat enfoc d’orientació a la transparència, tot i que disposem d’un elevat nombre de documents i eines d’edició internes. Estem en un procés de millora continua per adaptar els documents i les eines a formats oberts, en aquest sentit, es fan auditories periòdiques per detectar millores i també, s’està redactant una instrucció de govern per garantir que la totalitat de les relacions d’Ajuntament amb la ciutadania es realitzin a través de documents oberts i estàndards._

_Us agraïm la vostra participació i restem a la vostra disposició per a futures comunicacions._

_Cordialment,_