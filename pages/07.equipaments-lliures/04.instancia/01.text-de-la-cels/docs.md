---
title: 'Text de la CELS'
media_order: 'Instancia Equipaments Lliures.pdf'
taxonomy:
    category:
        - docs
visible: true
---

[Instància de la CELS](https://tec.lleialtat.cat/equipaments-lliures/instancia) a l'Ajuntament de Barcelona en relació a les tecnologies lliures i ètiques:

_La Coordinadora d’Entitats per la Lleialtat Santsenca (CELS), és una entitat que aplega més de trentena associacions i col·lectius del barri de Sants amb l’objectiu de gestionar de forma comunitària de l’equipament municipal La Lleialtat Santsenca._

_El consum responsable és un dels pilars que mouen l’activitat de la CELS. Ens plantegem el consum en tots els àmbits, també el tecnològic. A la Lleialtat hem impulsat una comissió tecnològica, Lleialtec, que ens permeti avançar en la nostra sobirania tecnològica i en conseqüència  només treballem amb programari lliure._

_Per això hem impulsat el projecte Equipaments Lliures. Volem que la Lleialtat Santsenca ens serveixi per anar creant un model de bones pràctiques en el camp tecnològic. Creiem que el model d'organització i gestió de la informació en espais comunitaris, i més si són municipals, ha de ser ètic i això, avui dia, només es pot aconseguir fent ús de programari lliure._

_Volem que la Lleialtat sigui un espai de referència de les tecnologies lliures i ètiques, des de on es desenvolupi el pensament crític, i on es faci formació i divulgació. Un espai que garanteixi l'ètica tecnològica dels seus entorns d'intercanvi i aprenentatge, i les llibertats digitals de les persones que hi participen._

_Uns objectius que a més estan alineats amb la Mesura de govern cap a la Sobirania Tecnològica que dins del Pla Barcelona Ciutat Digital l’Ajuntament de Barcelona va impulsar l’octubre de 2016.  Concretament en un dels seus punts es planteja la Transformació digital del govern de la ciutat i programari de codi obert amb el següent redactat:_

_Transició cap a codi i estàndards oberts:_

_En aquest pla, Barcelona afronta amb decisió la transició a programari lliure i estàndards oberts, estudiant les millors pràctiques europees i espanyoles en l'àmbit aquest àmbit. Es per això que es dissenyarà un pla de migració i s'establirà un codi de bones pràctiques sobre tecnologia que guiï la transformació interna, la reutilització i compartició de codi amb i de tercers, i el desenvolupament i/o utilització de solucions de govern comuns._

_Tanmateix com a entitat implicada en aquest àmbit ens trobem amb dificultats en la nostra relació amb l’administració. Des de l’Ajuntament de Barcelona no es publiquen fitxers  descarregables en formats lliureses, de manera que per poder presentar memòries o justificacions de subvencions hem de renunciar als nostres principis, que representa que també han estat assumits per l’administració, i fer ús de programes com word o excel._

_Això és especialment complicat pel que fa a les fulles de càlcul de justificació de les subvencions, ja que es tracta de documents amb formules tancades incompatibles amb programes de codi obert._

_Per tot això instem a l’Ajuntament de Barcelona a presentar, com a mínim també, alternatives per poder presentar tota aquesta documentació amb programes de codi obert._

[Descarrega aquest text](Instancia%20Equipaments%20Lliures.pdf) des del núvol de la Lleialtat.