---
title: 'Instància '
taxonomy:
    category:
        - docs
visible: true
---

[L'àgora](https://tec.lleialtat.cat/infraestructura/agora) és la nostra eina de participació digital a la CELS. El 3 de gener de 2019, les persones tècniques del PAS, -que gestionen el dia a dia de l'equipament-, escrivien el següent: 

_Com molts sabreu a l’Ajuntament de Barcelona, contrariament al que diuen el seu pla: Barcelona Ciutat Digital, on plantejen una Transició cap a codi i estàndards oberts segueixen treballant exclusivament amb programari propietari._

_Això representa un problema per a aquelles entitats en que hem optat per treballar amb programari lliure. Sense anar més lluny a l’hora de justificar les subvencions, ja que fan servir excels amb formules que no funcionen quan es passen a format lliure._

_Per això a una de les darreres assemblees es va plantejar fer una instància demanant que, com a mínim, també tinguin els formularis en format lliure. He redactat un document per que és un problema que principalment ens trobem a l’equip tècnic. Us el comparteixo a veure que us sembla i en quant es vegi bé l’entrem a registre._

Als comentaris del fil de l'àgora, el projecte de [La Troca](http://latrocasants.org/) remarcava que es troben a la mateixa situació; i entre altres comentaris, hi va haver dues aportacions al text.

[Llegeix la instància](https://tec.lleialtat.cat/equipaments-lliures/instancia/text-de-la-cels) que es va enviar des de la CELS. 

La resposta va ser altament insatisfactòria. Altres equipaments, com es comentava a [la Trobada](https://tec.lleialtat.cat/equipaments-lliures/trobades/05-02-2019), tenen els mateixos problemes i no encoratja gens treballar amb programari lliure si l'administració no predica amb l'exemple. Com sempre apuntem a la comissió Lleialtec, no es pot implementar un programari i esperar a que les persones l'utilitzin: sense acompanyament, l'única cosa que aconseguirem és crear rebuig. 

[Llegeix la resposta](https://tec.lleialtat.cat/equipaments-lliures/instancia/resposta-de-lajuntament) de l'Ajuntament.

 