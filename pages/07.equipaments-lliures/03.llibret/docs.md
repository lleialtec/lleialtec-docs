---
title: Llibret
taxonomy:
    category:
        - docs
visible: true
---

Des de la **comissió d'Acció Comunitària** de la Coordinadora d'Entitats per la Lleialtat Santsenca ([CELS](https://www.lleialtat.cat/coordinadora/)) estem coordinant un llibret de bones pràctiques en relació a l'ús de les tecnologies digitals en ecosistemes cooperatius. La Lleialtat és un espai municipal de gestió comunitària i un dels seus tres eixos d'acció és el **cooperativisme i el consum responsable**. I és per això que volem donar la mateixa importància als productes que s'ofereixen al Cafè com als programes informàtics que usem en el nostre dia a dia. 

Tot i que a la Lleialtat Santsenca es duen a terme innombrables activitats relacionades amb l'Economia Social i Solidària (ESS), es dona la paradoxa que la majoria d'iniciatives de l'ESS no només fa ús de programari privatiu, sinó que el promociona entre les seves sòcies. 

Afortunadament, detectem que es fa per desconeixença, així que, en el context del projecte [Equipaments Lliures](https://tec.lleialtat.cat/equipaments-lliures), hem decidit escriure un llibret de bones pràctiques per tal de mostrar que **totes les lluites són una** i que, els valors de l'ESS estan més alineats amb els de les tecnologies lliures i ètiques que no pas amb els de les privatives, que bàsicament no en tenen sinó que s'aprofiten de les llibertats del programari per crear «walled gardens» (jardins emmurallats). Però, com deia en McLuhan, «el mitjà és el missatge», i en l'ús de tecnologies lliures i ètiques fomentem també valors com el cooperativisme i el consum responsable!

El llibret serà coordinat des de Lleialtec i [Trama Cultura](http://trama.coop/), amb la participació d'altres entitats, col·lectius i persones de la CELS. I s'intentarà crear-lo íntegrament amb programari lliure: els textos, per exemple, respectaran [estàndards oberts](https://tec.lleialtat.cat/equipaments-lliures/llibret/estandards-oberts). De moment, podeu consultar el [cronograma](http://documents.lleialtat.cat/LleialTec/llibret-cronograma-lleuger.png) provisional. Seguirem informant! 
