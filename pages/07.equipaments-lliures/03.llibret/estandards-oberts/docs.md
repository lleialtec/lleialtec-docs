---
title: 'Estàndards oberts'
taxonomy:
    category:
        - docs
visible: true
---

Els documents digitals de la Lleialtat Santsenca respecten estàndards oberts perquè estan creats amb eines que, per defecte, generen **documents en formats lliures**: Libre Office, en la seva eina Writer per exemple, genera .odt (Open Document Text), i és per això que fem servir aquesta _suite_ informàtica. Un document amb format .doc o .docx és el que genera per defecte la _suite_ Office de Micro$oft, i com és privativa i, a sobre, de pagament, preferim evitar aquests formats i eines. 

Pel [llibret](https://tec.lleialtat.cat/equipaments-lliures/llibret) de bones pràctiques farem servir Libre Office. Si voleu crear un document amb aquest programa i no el teniu instal·lat, el podeu descarregar des de la seva [pàgina oficial](https://www.libreoffice.org/), o bé podeu fer-lo servir a [l'aula informàtica](https://tec.lleialtat.cat/infraestructura/aules-lliures) de la Lleialtat, on ja està instal·lat a tots els ordinadors. A diferència de les eines privatives del magnat Bill Gates, Libre Office és lliure, ètic i, a sobre, gratuït. 

Un dels problemes que ens trobem els equipaments que fem servir programari lliure com Libre Office és que l'administració, -que encara no ha adoptat aquestes bones pràctiques-, envia documentació en formats privatius que no respecten els estàndards i es desquadren, o bé en .pdf que, si bé ja és lliure, no permet modificar paràmetres. Des de la Lleialtat ja vam fer una instància i la resposta, que apuntava a que ja es publica en .pdf, ha estat molt insatisfactòria. 

Per saber-ne més:

["Estándares Abiertos"](https://fsfe.org/activities/os/def.es.html), per la Free Software Foundation Europe (FSFe). Consulta de 20 de març del 2019  

["España, el país que apuesta por los estándares abiertos pero sólo usa PDF"](https://www.eldiario.es/turing/software_libre/estandar-documentos-Administraciones-Publicas-Espana_0_311469935.html), a El Diario (2014). Consulta de 20 de març del 2019