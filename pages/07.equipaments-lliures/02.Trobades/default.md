---
title: Trobades
---

Al barri de Sants (i arreu de la ciutat de Barcelona) comença a créixer el nombre de centres on s'implementen tecnologies lliures i ètiques. I és per això que hem creat les Trobades d'Equipaments Lliures, **espais d'intercanvi entre centres que gestionen diner públic**: centres cívics, iniciatives de gestió comunitària, biblioteques, escoles, instituts, casals de joves, etc. 

L'objectiu de les trobades és crear i fomentar una nova (i necessària) **cultura de l'acompanyament en transicions tecnosocials**, per tal d'apoderar els centres en quant a l'ús, formació i difusió de tecnologies lliures i ètiques.   


### Butlletí

Si voleu estar al dia del que fem, subscriviu-vos al nostre [**butlletí**](https://butlleti.lleialtat.cat/subscription/S1ohBFJ4V), que com està muntat amb programari lliure [Mailtrain](https://tec.lleialtat.cat/infraestructura/butlleti) i el gestionem des de la comissió de tecnologia, permet rebre les nostres informacions sense que terceres empreses es lucrin amb les vostres dades :) 