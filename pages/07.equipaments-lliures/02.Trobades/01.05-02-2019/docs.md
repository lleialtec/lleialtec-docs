---
title: 'Trobada 05-02-2019'
published: true
taxonomy:
    category:
        - docs
visible: true
---

### Invitació

El **5 de febrer de 2019** vam celebrar una Trobada Equipaments Lliures per tal de difondre l'ús de tecnologies lliures i ètiques entre les entitats que gestionen diner públic. Les convidavem així:

_"Benvolguts i benvolgudes,_

_Des de la comissió de tecnologia de la Lleialtat Santsenca i en el marc del projecte [«Equipaments Lliures»](https://tec.lleialtat.cat/equipaments-lliures), el dia 5 de febrer, de 10h a 13h us convidem a una **trobada d'equipaments municipals**, on podrem conèixer millor les tecnologies lliures i ètiques._

_Sota els principis de la iniciativa [«Public Money, Public Code»](https://publiccode.eu/) i des de la comissió de tecnologia, volem fomentar la implementació, ús i divulgació de tecnologies que respecten a les seves usuàries, especialment als espais que gestionen diner públic. Adjuntem [el programa](https://documents.lleialtat.cat/LleialTec/Equipaments-Lliures/Trobades/trobada-invitacio-programa.png) i agrairem que en feu difusió per les vostres xarxes._

_Per tal de calcular el menjar amb mesura, si us plau, confirmeu la vostra assistència i el nombre de persones que vindreu responent a aquest correu mateix o a info@tec.lleialtat.cat. Us hi esperem a totes i tots!_

_Salutacions i gràcies."_


### Participació

Lleialtat, Secretariat, Calàbria 66, La Prosperitat Cultura en Acció 2, Torre Jussana, Craj, El Sortidor, la Casa del Rellotge, la Casa del Mig, La Clau d'Hostafrancs, INS Lluís Vives, Institut Ausiàs March i la Casa Orlandai

### Ponències

* Des del **Districte de Sants-Montjuïc** s'ha anat implementant programari lliure a alguns equipaments durant el 2018 (sistemes operatius com Ubuntu i aplicacions com Libre Office). En són exemple El Sortidor i La Clau. Es parla de suport tècnic: els equipaments disposen de suport tècnic telefònic per part de l'IMI (Institut Municipal d'Informàtica) però també es comenta que aquest suport finalitzarà tard o d'hora. I també de reaprofitar maquinari amb sistemes lliures més lleugers

* **Linkat** comenta millores del seu sistema operatiu lliure (que neix d'Ubuntu i té diversos escriptoris). També contempla que la distribució Linkat permet la possibilitat de reaprofitar maquinària vella amb sistemes operatius lliures més lleugers. I també que, si bé s'implementa el sistema cada cop més, hi ha encara una manca generalitzada d'acompanyament als centres en la transició i canvis tecnològics

* **Apoderament digital** ens parla de la importància de reivindicar les tecnologies lliures des dels municipis, de prendre consciència de la sobirania tecnològica als barris, de l'obligació política de fer respectar els drets i llibertats digitals de les usuàries. I reprèn també el tema de la reutilització del maquinari per qüestions ètiques i ecològiques, com fan des del projecte eReuse (amb el que s'ha col·laborat des de la Lleialtat per crear l'aula lliure)

* Des de **l'equip tècnic de la Lleialtat**, es comenta que el tema de les tecnologies lliures està sent tota una descoberta, però que encaixa perfectament als valors que es fomenten des de la Lleialtat i que s'emmarquen en tres eixos: acció veïnal, cultura i cooperativisme i consum responsable. Tot és tot i, tal i com es volen sistemes operatius lliures als ordinadors, també es vol oferir refrescos no industrials al Cafè. Lleialtat i Coherència :)

* Des de **Lleialtec**, reivindiquem la rèplica del projecte arreu dels equipaments que gestionen diner públic. I destaquem la importància de l'acompanyament en la migració a sistemes lliures. No podem esperar a que algú usi una nova eina perquè sí: cal consciència i acompanyament. Sofrim una mena d'intoxicació tecnològica, i per això costa més desaprendre que aprendre... Cada centre és un món i la planificació de migració hauria de ser personalitzada. 

Us deixem aquí la [presentació de Lleialtec](https://documents.lleialtat.cat/LleialTec/Equipaments-Lliures/Trobades/trobada-presentacio.pdf) on es mostra l'evolució tecnològica de la Lleialtat de 2015 a 2018, es presenta el projecte Equipaments Lliures per al 2019, i es proposen eines alternatives i un itinerari de migració. 

### Inputs

* Comenten de Torre Jussana que estan conscienciadíssimes i que ho estan migrant tot :) Però que necessiten assessorament immediat
* El Sortidor, que usa Libre Office, comenta que el fet de rebre documentació en formats tancats dificulta la feina administrativa (en relació a aquest tema, des de la Lleialtat, hem enviat una instància i trobem que la resposta obtinguda és massa insatisfactòria...)
* Es parla de formació interna per a tècnics d'equipament o docents 
* Es parla de crear col·laboracions per difondre continguts relacionats amb tecnologies lliures: programació de videojocs, internet de les coses (IOT), etc.
* La Clau mostra especial interès en dinamitzar tallers per als joves
* La Casa del Rellotge i la del Mig proposen accions "maker" (impressió 3D, làser, IOT, etc.)
* Més enllà de les eines d'ofimàtica conegudes i l'àgora participativa, es mostra també interès en conèixer les xarxes socials lliures i ètiques
* Més enllà de sistemes operatius i eines de gestió integral, es parla d'infraestructures de telecomunicacions amb operadors de proximitat com el binomi guifi.net-eXO, nascut de la manca d'Internet a zones rurals: les operadores no hi arriben perquè no en treuen prou benefici
* INS Lluís Vives (amb qui ja es fa Acció Comunitària des de la CELS), troba interessant projectar el curs 2019-2020 amb més acció comunitària a través de la futura Snap!Arcade

**Vas venir a la Trobada i vols afegir un input a aquesta llista?**
Envia'ns un correu a info@tec.lleialtat.cat i el publicarem ;)

Gràcies a totes i tots per venir a la Trobada!  

