---
title: 'Equipaments Lliures'
taxonomy:
    category:
        - docs
child_type: docs
visible: true
---

**Equipaments Lliures** és un projecte proposat des de la [Lleialtec](https://tec.lleialtat.cat/) i que compta amb el suport de la [CELS](https://www.lleialtat.cat/coordinadora/), la Coordinadora d'Entitats per la Lleialtat Santsenca. El projecte té per objectiu **consolidar la Sobirania Tecnològica a La Lleialtat Santsenca**, i els seus tres eixos, -en sintonia amb els de Lleialtec-, són la implementació d'infraestructures lliures, la divulgació de tecnològies ètiques i la formació en nous conceptes i oficis. 


  Tenim un eix d'[**infraestructura**](https://tec.lleialtat.cat/infraestructura) perquè volem predicar amb l'exemple i dotar-nos d'eines tecnològiques autogestionades: lliures i ètiques; tenim un eix de [**divulgació**](https://tec.lleialtat.cat/divulgacio) perquè tan volem alertar sobre l'ús i abús de tecnologies que priven la llibertat de les seves usuàries, com volem donar a conèixer alternatives a aquests serveis privatius; i tenim un eix de [**formació**](https://tec.lleialtat.cat/formacio) perquè volem participar en l'apoderament digital de les nostres veïnes i ajudar a crear nous aprenentatges i oportunitats a través de les tecnologies lliures. 

  Per tot, ens emociona pensar que el model que estem creant sigui replicat per altres equipaments que, municipals o no, siguin vertebradors del teixit social dels seus barris. 
    
  Al setembre de 2018, l'Ajuntament de Barcelona ens ha atorgat una subvenció [**"Impulsem el que fas"**](https://tec.lleialtat.cat/equipaments-lliures/impulsem-el-que-fas) i, amb ella, continuarem treballant en aquests eixos per consolidar La Lleialtat com a el primer equipament lliure i ètic de la ciutat de Barcelona. Us fem cinc cèntims del que volem fer:


###     Infraestructura


  Adquirir **maquinari**: servidors, plaques, etc.; configurar-lo amb programari lliure; i mantenir-lo

  Continuar **implementant i mantentint** eines lliures i ètiques: xarxa social lliure, tallafocs, permisos, etc. i en desenvoluparem, si escau: ERP Cafè, nova web, etc.

  Continuar col·laborant amb el projecte [**"L'Àlbum familiar"**](https://www.lleialtat.cat/tria-una-foto-familiar-i-explicans-la-teva-historia-posem-en-marxa-lalbum-familiar-del-barri/) per tal d'ajudar a desenvolupar el projecte amb programari lliure.

  Continuar **documentant** la infraestructura implementada al [**HowTos**](https://tec.lleialtat.cat/howtos) per tal de formentar-ne la seva replicabilitat en altres espais d'acció comunitària que cerquin vetllar per la seva autogestió tecnològica.


###     Divulgació


   ##### BerenaTec
  
  Al desembre comencem els [**berenars tecnològics**](https://tec.lleialtat.cat/divulgacio/berenatec), on donarem a conèixer 1) projectes que fomenten la Sobirania Tecnològica i 2) despropòsits de les GAFAM (Google, Apple, Facebook, Amazon, Microsoft). 


   ##### Campanyes


  [**proFEDIVERSE**](https://tec.lleialtat.cat/divulgacio/fediverse): volem convidar a totes les persones o organitzacions horitzontals, especialment les que formen part de la CELS i/o d'iniciatives de l'ESS, a participar de les xarxes socials lliures i ètiques. Per fer-ho, primer, volem informar sobre què són les xarxes socials lliures i què les distingeixen de les xarxes asocials privatives. Ho expliquem al llibret [**FEDIVERSE, xarxes lliures i ètiques**](http://documents.lleialtat.cat/LleialTec/Fediverse/fediverse-xarxes-lliures-a4.pdf). 

  Un cop informades, posem a disposició de les veïnes de Sants el node [**Santsenques.cat**](https://santsenques.cat/). Allà, podran crear un compte d'usuària a un node de proximitat. Un cop registrades al node, ja podran contactar amb la resta d'usuàries de la FEDIVERSE. El node Santsenques l'autogestionem membres de Lleialtec i, en la mesura dels possibles, anirem donant suport tècnic i conceptual a les veïnes. Ens podreu trobar trastejant les xarxes socials lliures **cada primer i tercer dilluns de mes** a la nostra [aula lliure](https://tec.lleialtat.cat/infraestructura/aules-lliures) :) Totes convidades! 
    
  [**antiGAFAM**](https://tec.lleialtat.cat/divulgacio/gafam): volem informar a les veïnes de les males pràctiques dels gegants tecnològics. Al primer berenar, coneixerem aquests nous conceptes; i als següents, donarem a conèixer cadascuna de les lletres (Google, Apple, Facebook, Amazon, Microsoft) a través de la traducció al català de la campanya de l'associació francesa La Quadrature du Net.  
  
   ##### Donacions i matrocinis
  
  Volem ajudar a tirar endavant projectes relacionats amb el programari o el maquinari lliures 

   ##### Festes d'instal·lació
  
  En farem, mínim, dues l'any: pel [SobTec](http://sobtec.cat/) (març) i pel [Dia de la Llibertat del programari](https://ca.wikipedia.org/wiki/Dia_de_la_llibertat_del_programari) (setembre)

   ##### Càpsules Troca
   
  Seguirem col·laborant amb La Troca i les seves [càpsules informàtiques](https://latrocasants.org/programacio/)
  
   ##### Llibret de bones pràctiques
   
   Volem publicar un llibret de bones pràctiques que explori les tangents entre l'Economia Social i Solidària, i el programari lliure. 

   ##### Material gràfic
  
  Un cop vam fer xapes i en volem tornar a fer; però ens faria molta il·lusió tenir samarretes, adhesius i usb's amb les distros cremades ;) 


### Formació

   ##### Snap!Arcade

   Volem crear una [**Snap!Arcade**](https://snaparcade.cat/), una màquina recreativa amb programari lliure i, en la mesura que es pugui, el maquinari també serà lliure. A més, proposarem cursets per aprendre a programar jocs i animacions per la màquina. La temàtica del disseny i la seva execució es consensuarà a la comissió d'**acció comunitària**. I es proposa participar a les entitats de la CELS i les veïnes que així ho desitgin, a col·laborar en el muntatge i la creació del contingut digital de la màquina.

   ##### TeatreTec  
  
  Volem ajudar a crear una obra de teatre tecnològica, on petites i grans puguem gaudir de la computació sense ordinadors, de forma lúdica i gairebé màgica. Apunts per al [ **TeatreTec**](https://pad.lleialtat.cat/p/teatretec)

   ##### BiblioTec
  
  Volem configurar un espai d'autoaprenentatge en matèria de sobirania tecnològica. És per això que volem adquirir llibres físics i posar-los a disposició de les entitats de la CELS que els vulguin llegir. També es podria fer un club de lectura per comentar els llibres.

Aquestes són algunes de les nostres voluntats: seguirem informant! 

