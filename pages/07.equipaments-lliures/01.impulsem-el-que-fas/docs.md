---
title: 'Impulsem el que fas'
taxonomy:
    category:
        - docs
visible: true
---

#### Documentació de la subvenció

##### Abril 2019
Enviament del [resum de l'activitat](http://documents.lleialtat.cat/LleialTec/Equipaments-Lliures/Impulsem-2018/impulsem-2018-update-abril.pdf) realitzada fins ara (de desembre a abril) i de cara al proper trimestre (de maig a juliol) als tècnics de projecte

##### Febrer 2019
Reunió a Barcelona Activa amb els tècnics de projecte on s'exposa el [pla de comunicació](http://documents.lleialtat.cat/LleialTec/Equipaments-Lliures/Impulsem-2018/impulsem-pla-comunicacio-2.pdf)

##### Gener 2019
Recepció de la subvenció

##### Desembre 2018
Inici de l'activitat del projecte

##### Octubre-novembre 2018
[Planificació](https://tec.lleialtat.cat/equipaments-lliures) del projecte

##### Setembre 2018
Comunicació d'atorgació de subvenció

##### Maig de 2018
[Sol·licitud](http://documents.lleialtat.cat/LleialTec/Equipaments-Lliures/Impulsem-2018/document_basic_2_modalitats_1-2-3-4-5.pdf) per a la subvenció "Impulsem el que fas" 