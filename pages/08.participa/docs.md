---
title: Participa!
taxonomy:
    category:
        - docs
---

Convidem a altres persones i col·lectius de la comunitat del programari lliure i/o sensibilitzats amb la importància de l'apoderament digital a donar suport i/o contribuir en el desenvolupament d'[**Equipaments Lliures**](https://tec.lleialtat.cat/equipaments-lliures). I fem una crida a tots els espais d'acció comunitària a replicar el pla per tal de vetllar per la seva salut digital i la de les seves veïnes.

* Difoneu el projecte arreu. A les xarxes socials, farem servir l'etiqueta #EquipamentsLliures #laLleialtat #BerenaTec #FEDIVERSE #GAFAM ...

* Veniu a les activitats que organitzem, com trobades, cursets i taules obertes. Per més informació, **[subscriu-te al nostre butlletí](https://butlleti.lleialtat.cat/subscription/S1ohBFJ4V)**

**Ens trobem periòdicament**. Habitualment hi som tots dimecres al matí, i el primer i tercer dilluns a la tarda. Igualment, us podeu posar en contacte amb nosaltres per correu.  

**Correu electrònic per participar a la comissió ** [lleialtec@lleialtat.cat](mailto:lleialtec@lleialtat.cat)

**Correu electrònic per sol·licitar acompanyament ** [info@tec.lleialtat.cat](mailto:info@tec.lleialtat.cat)

**Xarxes socials** [@lleialtec@santsenques.cat](https://santsenques.cat/users/lleialtec)
No participem de xarxes asocials privatives però sí de les lliures i ètiques. Saludeu-nos a les xarxes lliures ;) Si sou veïnes de Sants, passeu qualsevol primer o tercer dilluns de mes i us ajudem a obrir un compte al vostre node de proximitat :)


