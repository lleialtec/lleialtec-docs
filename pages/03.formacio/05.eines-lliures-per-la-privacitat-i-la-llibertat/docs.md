---
title: 'Directori d''eines lliures i respectuoses'
published: false
taxonomy:
    category:
        - docs
visible: true
---

## Recursos externs

   * Directori d'alternatives d'aplicacions i serveis corporatius-privatius [https://prism-break.org/en/all/](https://prism-break.org/en/all/)
   * Directori d'alternatives lliures i privatives: [alternativeTo](https://alternativeto.net/). ([Exemple](https://alternativeto.net/software/google-drive/?license=opensource))
   * Servidors d'aplicacions o serveis lliures: [Framasoft](https://framasoft.org/?m=lite)
   * Directori d'aplicacions lliures per a Android: [Fossdroid](https://fossdroid.com/)


## Mòbil
* Mercat d'aplicacions lliures: [F-Droid](https://f-droid.org/)
* Descarregador d'aplicacions de Google Play: [Yalp store](https://fossdroid.com/a/yalp-store.html)
* Mercat d'aplicacions, lliures i no lliures: [Aptoide](http://www.aptoide.com/)
* Mapes: [OsmAnd](https://f-droid.org/en/packages/net.osmand.plus/) i [Maps.me](https://fossdroid.com/a/maps.html)
* Missatgeria
   * [Signal](https://signal.org/android/apk/). Segura però centralitzada, necessita nº telf.
   * [Conversations](https://fossdroid.com/a/conversations.html) (xmpp/jabber), federada i confidencial però revela metadades.
   * [Riot.im](https://fossdroid.com/a/vector.html) (matrix.org), federada i confidencial però revela metadades i xifrat en proves
   * [Kontalk](http://kontalk.org/) (basada en xmpp)
* Sistema operatiu Android sense Google: [LineageOS](https://wiki.lineageos.org/devices/) (successor de cyanogenMod)
* Sistema operatiu Android sense Google: [Omnirom](https://www.omnirom.org/) *fork* de cyanogenmod després de [*certs problemes*](https://www.theregister.co.uk/2013/10/16/android_custom_roms_splinter_over_openness/)
* El mòbil amb més hardware lliure i software lliure (KDE/GNOME/Debian/Matrix) [Librem 5](https://puri.sm/shop/librem-5/)

## Ordinador
* Sistemes operatius lliures
   * Linux Mint [https://linuxmint-installation-guide.readthedocs.io/ca/latest/](https://linuxmint-installation-guide.readthedocs.io/ca/latest/)
   * Ubuntu (edicions alternatives) [https://www.ubuntu.com/download/ubuntu-flavours](https://www.ubuntu.com/download/ubuntu-flavours)
   * ...
* Navegador
   * [Firefox](https://www.mozilla.org/firefox)
   * [Chromium](https://www.chromium.org/getting-involved/download-chromium)
* Edició d'imatge, disseny, creació 3D, publicació
   * [Gimp](https://www.gimp.org/)
   * [Inkscape](https://inkscape.org/)
   * [Krita](https://krita.org/)
   * [Blender](https://www.blender.org/)
   * [Scribus](https://www.scribus.net/)

## Eines i serveis de xarxa
_alternatives a la dominació de Google_

* [Mail](../mail-no-corporatiu)
* Drive
   * **Nextcloud**
     * Tria una proveïdora: [https://nextcloud.com/providers/](https://nextcloud.com/providers/)
     * [Disroot](https://disroot.org/es/services/nextcloud)
     * [Indie hosters](https://indie.host/)
   * **Owncloud** (versió no comunitària de nextcloud) [https://owncloud.org/hosting-partners/](https://owncloud.org/hosting-partners/)
* Docs
   * **JetPad**: amb formatat [https://jetpad.net/](https://jetpad.net/)
   * **Collabora** LibreOffice: amb formatat però sense instaŀlacions públiques. Info des de [Collabora](https://www.collaboraoffice.com/solutions/collabora-office/) i des de [Nextcloud](https://nextcloud.com/collaboraonline/)
   * **CryptPad**: xifrat però públic amb enllaç [https://cryptpad.fr/](https://cryptpad.fr/)
   * **ProtectedText**: xifrat amb una contrasenya separada [https://www.protectedtext.com/](https://www.protectedtext.com/)
   * Pads clàssics, fàcils, públics:
       * [https://pad.lleialtat.cat/](https://pad.lleialtat.cat/)		
       * [http://etherpad.guifi.net](http://etherpad.guifi.net)
       * [http://pad.lamardebits.org/](http://pad.lamardebits.org/)
       * [https://pad.frontlinedefenders.org/](https://pad.frontlinedefenders.org/)
       * [https://pad.riseup.net/](https://pad.riseup.net/)
       * [https://pad.codigosur.org/](https://pad.codigosur.org/)
       * [https://antonieta.vedetas.org](https://antonieta.vedetas.org)
       * [https://mensuel.framapad.org/](https://mensuel.framapad.org/)
       * ...
* Search
   * NO **Google**, trafica amb dades, és un règim dictatorial digital
   * NO **Bing**, Microsoft trafica amb dades, és un règim dictatorial digital
   * NO **Yandex**, trafica amb dades, és un règim dictatorial digital
   * NO **Ecosia**, planta arbres a canvi de vendre la teva identitat als mercats de tràfic de dades, màrketing i manipulació social
   * SÍ **Duckduckgo** [https://duckduckgo.com/](https://duckduckgo.com/) Cercador. És una empresa dels USA però respecta la privacitat dels usuaris. Guanya diners amb anuncis no personalitzats.
   * SÍ **StartPage/Ixquick** [https://www.startpage.com/](https://www.startpage.com/)  Metacercador sobre Google. És una empresa europea però respecta la privacitat dels usuaris. Guanya diners amb anuncis no personalitzats.
   * SÍ **Searx**. És un metacercador sobre 70 cercadors (codi lliure)
       * [https://searx.me](https://searx.me)
       * [https://searx.org](https://searx.org)
       * [https://searx.laquadrature.net/](https://searx.laquadrature.net/)
       * [https://framabee.org/](https://framabee.org/)
       * [https://search.disroot.org/](https://search.disroot.org/)
* Maps
   * **OpenStreetMaps**
       * Estàndard amb 3 capes: [https://www.openstreetmap.org/node/5426862347](https://www.openstreetmap.org/node/5426862347)
       * Opentopomap: corbes de nivell i muntanya [https://opentopomap.org/#map=13/41.34112/1.70151](https://opentopomap.org/#map=13/41.34112/1.70151)
       * Umap: crea un mapa personalitzat
           * [https://framacarte.org](https://framacarte.org)
           * [http://umap.openstreetmap.fr](http://umap.openstreetmap.fr)
   * **Institut Cartogràfic i Geològic de Catalunya**
       * Instamaps: crea un mapa personalitzat [https://www.instamaps.cat/](https://www.instamaps.cat/)
       * Mapa polític/topogràfic i de satèl·lit [http://www.icc.cat/vissir3/index.html?ryKiLHg78](http://www.icc.cat/vissir3/index.html?ryKiLHg78)
       * Altres [http://betaportal.icgc.cat/](http://betaportal.icgc.cat/) (ALERTA, xifrat mal configurat)
* Indicacions
   * Amb transport públic, a catalunya: [https://mou-te.gencat.cat](https://mou-te.gencat.cat)
   * A peu, bici o cotxe [https://www.openstreetmap.org/directions](https://www.openstreetmap.org/directions?engine=graphhopper\_bicycle\&route=41.3464%2C1.6995%3B41.5113%2C1.7016#map=11/41.4286/1.7104)

## Xarxes socials federades - Fedivers
* Introduccions generals
   *  [Presentació web](https://surtdelcercle.cat/presentacions/free-soft-day-2018/) de @surtdelcercle
   *  [Viquipèdia: Fedivers](https://ca.wikipedia.org/wiki/Fedivers)
* Punts d'entrada al Fedivers
   *  Introduccions als diversos programaris i notícies: [fediverse.party](https://fediverse.party/)
   *  Estadístiques d'instàncies: [fediverse.network](https://fediverse.network/)
   *  Estadístiques d'instàncies: [the-federation.info](https://the-federation.info/)
* semblant a Facebook
   * **Diaspora**
       * servidors disponibles [https://the-federation.info/diaspora](https://the-federation.info/diaspora)
       * web del projecte [https://diasporafoundation.org/](https://diasporafoundation.org/)
   * **Hubzilla**
       * servidors disponibles [https://the-federation.info/hubzilla](https://the-federation.info/hubzilla)
       * web del projecte [https://project.hubzilla.org/](https://project.hubzilla.org/)
* semblant a Twitter
   * **GnuSocial** / **Quitter**
       * servidors disponibles [https://fediverse.kranglabs.com/](https://fediverse.kranglabs.com/)
       * web del projecte [https://gnu.io/social/](https://gnu.io/social/)
   * **Mastodon**
       * servidors disponibles [https://instances.social/](https://instances.social/) i [https://joinmastodon.org/](https://joinmastodon.org/)
       * web del projecte [https://joinmastodon.org/](https://joinmastodon.org/)
* semblant a Instagram
   * **[Quit.im](https://quit.im/)**