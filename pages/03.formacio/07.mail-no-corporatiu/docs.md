---
title: 'Mail no corporatiu'
published: false
taxonomy:
    category:
        - docs
visible: true
---

## El problema del correu gratuït

* anuncis
* control social
* falta de privacitat
* funcionalitats no estàndards
* trenquen la federació amb servidors petits o alternatius

## Els diners

* subscripció periòdica
* donacions
* intercanvi no monetari

## Els estàndards

* penjar-enviar correus: SMTP
* descarregar-llegir correus: IMAP, POP
* xifrat client-servidor: TLS, STARTLS
* xifrat servidor servidor: DKIM, TLS
* xifrat extrem-extrem: PGP, DIME
* nova proposta: [darkmail](https://darkmail.info/)

## Usabilitat del xifrat

* Clients de correu amb extensions (thunderbird, mutt, k9mail, etc.)
* PGP amb [leap](https://leap.se/)
* PGP amb [p≡p](https://www.pep.security/)
* propostes de xifrat incompatibles entre elles: tutanota, protonmail, mail1click
* cerca local més difícil quan els correus estan xifrats

## Les proveïdores
_Llistem algunes que coneixem que tenen registre obert_

* Coŀlectius activistes, per donacions
    * [us] [RiseUp](https://riseup.net)
    * [it] [Autistici/Inventati](https://autistici.org)
    * [us] [Aktivix](https://aktivix.org/)
* Empreses cooperatives
    * [de] [Posteo](https://posteo.de/)
* Empreses centrades en la privacitat
    * [de] [Tutanota](https://tutanota.com/)
    * [ch] [Protonmail](https://protonmail.com/)
    * [us] [Lavabit](https://lavabit.com/)
* De proximitat
    * [cat] [Pangea](https://pangea.org)
    * [cat] [La mar de bits](https://lamardebits.org)

Llistes més completes:
* [Riseup: Radical servers](https://riseup.net/en/security/resources/radical-servers)
* [Privacy Tools](https://privacytoolsio.github.io/privacytools.io/#email)
* [Privacy-Conscious Email Services](https://www.prxbx.com/email/)