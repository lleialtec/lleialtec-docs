---
title: Formació
taxonomy:
    category:
        - docs
child_type: docs
visible: true
---

Trobem que la formació és clau per adaptar-se a **noves eines i conceptes**. Vivim en una era volàtil on la majoria dels infants no poden saber de què treballaran de grans perquè les seves professions encara estan per inventar.

És per això que creiem que l'**aprenentatge al llarg de la vida** és crucial: hem d'aprendre i desaprendre, però, sobretot, hem de sorprendre'ns a nosaltres mateixes en la descoberta de les nostres habilitats i la potenciació de competències transversals i imprescindibles d'aquest inici de segle.

Aprèn coses noves!, forma't en tecnologies lliures i ètiques!