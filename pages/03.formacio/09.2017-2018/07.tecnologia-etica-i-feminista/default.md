---
title: 'Tecnologia ètica i feminista'
---

## Introducció
La tecnologia se sol explicar des de lògiques comercials i capitalistes,
a banda que hi predomina la dimensió productivista i patriarcal. Volem
oferir una aproximació als diferents àmbits de la tecnologia per
adquirir coneixement i consciència crítica.

### Objectius i mètodes
Conceptes teòrics i dinàmiques participatives per a conèixer i fomentar
una elecció conscient de les tecnologies, aproximar-se a les
alternatives més ètiques i segures i conèixer les seves lluites i
reptes, des de la perspectiva ciberfeminista. En concret podràs
aprendre: com funciona internet, adquirir nocions de seguretat i
privacitat digital, ser més autosuficient en producció web, trobar
alternatives a les xarxes socials i plataformes de comunicació, aprendre
el funcionament bàsic de gnu/linux, iniciar-se en altres alternatives de
programari lliure, tenir un ús més ètic en la tecnologia mòbil i
smartphone, descobrir recursos, comunitats, etc.
Teoria i pràctica, des d’un diàleg o inèrcia de les motivacions de les
persones assistents i els objectius generals.

### Temari
- Introducció a les tecnologies lliures, conceptes de les màquines i les
xarxes, la internet feminista, gnu/linux.
- Seguretat digital des d'una autodefensa feminista
- gnu/linux, alternatives lliures a eines i programes usuals.
- Aproximació a la tecnologia que més ens agradi.

### A qui va dirigit
A persones que vulguin augmentar la seva sobirania tecnològica i
perspectiva de gènere digital, conèixer alternatives de programari
lliure i tenir més recursos i consciència de les tecnologies. Nivell
inicial / mig.

### Durada
4 sessions de 2hores, 8hores. Horari de matins i/o tardes.
