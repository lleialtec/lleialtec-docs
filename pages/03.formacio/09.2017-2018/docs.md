---
title: 2017-2018
taxonomy:
    category:
        - docs
visible: true
---

#### 2017
La Lleialtat obria les seves portes per la Festa Major de l'any 2017 amb unes jornades de portes obertes que es van organitzar des de la CELS, i on vam dur a terme un taller de creació de videojocs amb llenguatges de programació visual, entre altres activitats: xerrades/debat, la instal·lació de punts d'accés a Internet i una festa d'instal·lació de sistemes operatius lliures. 

Durant els mesos d'octubre a desembre, vam proposar diverses **accions formatives, algunes, amb perspectiva de gènere**: 

* curset de seguretat informàtica bàsica
* tallers intensius de seguretat holística
* curset de maquetació de continguts web 
* curset de sistemes operatius lliures
* curset d'edició de revistes
* curset de postals nadalenques animades

Readaptats després a:

* [Seguretat holística en els mòbils](https://tec.lleialtat.cat/formacio/2017-2018/seguretat-holistica-en-els-mobils)
* [Introducció a eines digitals lliures i ètiques](https://tec.lleialtat.cat/formacio/2017-2018/alternatives-lliures-als-serveis-privatius)
* [Autodefensa gràfica amb eines lliures digitals](https://tec.lleialtat.cat/formacio/2017-2018/autodefensa-grafica-amb-eines-lliures-digitals)
* [Crea videojocs amb Snap!](https://tec.lleialtat.cat/formacio/2017-2018/crea-videojocs-amb-snap)
* [Introducció a gnu/linux per dones](https://tec.lleialtat.cat/formacio/2017-2018/introduccio-a-gnu-linux-per-dones)
* [Seguretat i privacitat digital bàsica amb ordinador](https://tec.lleialtat.cat/formacio/2017-2018/alternatives-lliures-als-serveis-privatius)
* [Tecnologia ètica i feminista](https://tec.lleialtat.cat/formacio/2017-2018/tecnologia-etica-i-feminista)

#### 2018
Al primer trimestre de 2018 proposàvem tres cursets més sobre seguretat, continguts web i programació. A més, al acollir el projecte pilot de [La Troca](https://latrocasants.org/), des de la Lleialtec vam organitzar un taller formatiu per muntar l'[aula informàtica](https://tec.lleialtat.cat/infraestructura/aules-lliures) que el projecte requeria. És una aula que funciona íntegrament amb programari lliure, i els ordinadors són reutilitzats. 

A finals d'any, vam proposar formació per aprendre a crear un [ERP o programa de gestió](https://tec.lleialtat.cat/infraestructura/erp-del-cafe), tot aprofitant que en necessitàvem un pel nou Cafè de la Lleialtat. I també vam dur a terme un parell de càpsules informàtiques amb La Troca, una de conscienciació sobre els serveis privatius de Google; i l'altra sobre programació visual amb Snap!.
