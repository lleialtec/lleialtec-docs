---
title: 'Introducció a gnu/linux per dones'
---

## Introducció
La tecnologia pateix una mancança de dones que hi participen, i volem
ajudar a revertir aquesta tendència. Aprendre gnu/linux i alternatives
lliures ens apropa a l'autogestió i l'empoderament digital i per això
dediquem un curs introductori per a dones i persones de gènere discomforme.

### Objectius i mètodes
Ensenyar els conceptes bàsics de la informàtica, dispositius i internet
i les xarxes, conèixer les seves alternatives lliures i adquirir una
visió crítica i feminista. Aprendre els primers passos per a manejar un
sistema operatiu lliure (gnu-linux) per a una autosuficiència bàsica,
assentar una base per a indagar en altres vessants tecnològiques i
trobar suports i eines ciberfeministes per a les nostres accions i vida
virtuals.
Dinàmiques teorico-pràctiques, compartició d'experiència i coneixement
adquirit, volcatge de recursos i acompanyament en general, segons
motivació i objectius.

### Temari
- Introducció a les tecnologies lliures, conceptes de les màquines i les
xarxes, la internet feminista, seguretat i privacitat.
- Iniciació a gnu/linux
- Alternatives de programari lliures i ètiques.
- Instal.lació de gnu/linux.

### A qui va dirigit
A dones i persones trans o gènere discomforme que vulguin iniciar-se en
gnu/linux, aprendre alternatives més ètiques i reflexionar sobre la
tecnologia des d'una perspectiva de gènere per empoderar-se de manera
crítica en el món digital. Nivell inicial / mig.

### Durada
4 sessions de 2hores, 8hores. Horari de matins i/o tardes.