---
title: 'Seguretat i privacitat digital bàsica amb ordinador'
published: false
---

## Descripció
En la nostra cotidianitat està present la informàtica i tractem amb
dades digitals, de forma individual o col·lectiva. Per poder millorar la
seguretat en aquest àmbit i garantir la privacitat, és important
entendre el funcionament de les activitats bàsiques com la
navegació per internet, la gestió de contrasenyes o la missatgeria
instatània, i conèixer ferramentes i pràctiques que estiguen de la
nostra part.  
→ Porta el teu ordinador personal i una memòria USB.


### Format de l’activitat
Una sessió de dos hores una vegada a la setmana. A cada sessió
combinarem teoria i pràctica del tema de cada sessió. La pràctica
s'aplicarà sobre les dades i ferramentes personals de les participants o
sobre unes de prova creades durant la sessió.



### Objectius
Ser conscients de com funcionen les coses al món digital i què passa.
Conèixer i experimentar amb solucions i ferramentes de protecció digital
i de bones pràctiques.


### Col·lectiu destinatari principal o prioritari
Persones que no tinguen coneixements tècnics d'informàtica però que utilitzen l'oridinador i Internet en la seua cotidianitat personal i/o coŀlectiva.
Nombre òptim de participants: 16.


### Continguts que es tracten
Teoria bàsica sobre seguretat informàtica.  
Llicències de programari. Programari Lliure.  
Sobirania tecnològica.  
Internet. Xarxes de telecomunicacions (guifi.net). Xarxes socials alternatives (quitter, hubzilla, etc.).  
Seguretat al navegador web. Empremta digital.  
Bones contrasenyes. Gestió de contrasenyes. Altres tipus d'autenticació.  
Introducció a la criptografia.  
Xifratge de memòries USB. Xifrar i desxifrar arxius. Còpies de seguretat de les dades.  
Metadades.  
Recuperar informació perduda. Esborrament segur de dades sensibles.  
Correu electrònic xifrat. Missatgeria instantània (OTR, Retroshare, Matrix,
Signal).  
Sistema operatiu GNU/Linux.


### Metodologia de treball
Descripció de la dinàmica:
A cada sessió combinarem teoria, dinàmiques i pràctica del tema de cada
sessió. La pràctica s'aplicarà sobre les dades i ferramentes personals
de les participants o sobre unes de prova creades durant la sessió.


### Infraestructura que es requereix
Una sala amb capacitat suficient.  
Necessitats generals:  
18 cadires. Taules amb espai per a 18 persones. Pissarra i rotuladors de
varis colors i esborrador.
Necessitats tècniques:  
Tants endolls com persones, wifi, videoprojector, bona temperatura a la
sala (no fred), altaveus, un ordinador portàtil amb connexió compatible
amb el projector.
