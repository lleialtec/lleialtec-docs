---
title: 'Seguretat holística en els mòbils'
published: false
taxonomy:
    category:
        - docs
visible: true
---

## Introducció
La seguretat informàtica «ciutadana» no s'hauria de limitar a llistar un seguit d'eines que si les instaŀlem i les fem servir totes, aconseguirem estar «supersegurs». Nosaltres ampliem aquesta visió i reubiquem el centre d'atenció al benestar de les persones i la seva seguretat, tenint en compte les vessants digital, física, legal i psico-social. Per tant, reflexionarem sobre els usos que fem de les tecnologies de la informació i les necessitats a què responen. Pensem-hi: les eines que escollim ens ajuden a satisfer les nostres necessitats, o bé les amaguen darrere de succedanis? També aprendrem com funciona internet i la xarxa de telefonia, i amb tot aquest coneixement serem capaces de protegir els aspectes de la nostra vida que ens importin més, tant individuals com coŀlectius. Durant tot el curs anirem proposant usos més conscients de la tecnologia i eines més respectuoses amb les nostres llibertats.

![foto-skylock-localitzar-telefons](https://n.lleialtat.cat/s/JWGJg4LCab8ix5F/preview)

## Objectius
1. Entendre com funciona aquesta nova extremitat humana anomenada "smartphone". Quantes antenes té? De quines parts es composa? Quines implicacions té cada tecnologia que incorpora?
2. Visualitzar quines entitats participen en la gestió de la connectivitat, en la dels "núvols", en la fabricació dels dispositius o la programació de les aplicacions. Quins interessos tenen i si ens interessa coŀlaborar-hi. Com  protegir-nos-en.
4. Reflexionar sobre els canvis socials, psicològics, i de salut que implica l'ús dels mòbils.
3. Acompanyar a la presa de decisions sobre quines aplicacions, tecnologies, funcionalitats, volem i quines no, i com fer-les servir. Promoure l'ús crític de la tecnologia (de la informació-comunicació) i la recerca de solucions més ètiques.

## Metodologia de treball
Combinarem explicacions de conceptes amb material gràfic preparat o dibuixat, consulta de webs, etc. amb algun joc, i format de debat o pregunta-resposta. Instaŀlarem aplicacions i alternatives i jugarem amb algunes.

## Continguts

0. Presentació mútua (½ sessió) - Expectatives, marc teòric, adaptem objectius, debat.
1. La telefonia mòbil (1 sessió) - Infraestructura telefònica, amenaces, propostes i reflexió.
2. Ordinador personal (1 sessió) - Infraestructura d'internet, ecosistema d'aplicacions, missatgeria.
3. Dispositius «smart» (1 sessió) - "Xarxes socials", impacte psico-social, propostes i reflexions.
4. Clausura (½ sessió) - Repàs, jocs, vídeo o el que decidim, contactes i recursos d'ampliació.

## Públic destinatari

Persones de tots els perfils que vulguin conèixer els riscos de les TIC, aturar-se a decidir com relacionar-s'hi, i prendre aquestes decisions.
Dirigit a persones usuràries de smartphonens, telèfons mòbils no tàctils, o ordinadors portàtils, i també d'internet.

## Durada

Farem una sessió a la setmana, de 2 hores.

## Requeriments

Necessitem un espai amb taules i cadires, amb material de dibuix com una
pissarra i desitjablement projector.
Les persones participants podran portar el seu dispositiu (mòbil,
portàtil) en algunes sessions per a canviar el software que fan servir.
Recomanarem fer còpies de seguretat abans. Necessitarem endolls i
connexió a internet.
