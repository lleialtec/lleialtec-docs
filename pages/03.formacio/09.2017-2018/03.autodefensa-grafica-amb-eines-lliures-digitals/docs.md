---
title: 'Autodefensa gràfica amb eines lliures digitals'
published: false
taxonomy:
    category:
        - docs
visible: true
---

![](1024-724.png)
#### Introducció
Estem bombardejades amb imatges que transmeten normes als que ens hem d'adaptar.
El cos (especialment el femení) està fet un objecte per al desig aliè i poques vegades el trobem com un subjecte, centre que construeix la reflexió.
Per defensar-nos necessitem fer-li la volta a la truita publicitaria reutilitzant les imatges des de la nostra perspectiva. Denunciarem d'una manera trapella la violència simbòlica que patim, fonamentant així un discurs propi. Coneixerem com canviar la gramàtica cultural adaptant-la als nostres desitjos.

#### Objectius
A través de fer conèixer els programes gràfics de codi lliure, volem oferir un espai per a que les usuàries puguin fer una reflexió que porti a fer front de forma creativa a les normes i discriminacions, contribuint així a una societat més justa i transparent.

#### Metodologia de treball
Introducció a eines de disseny gràfic enfocada en l'expressió creativa.  
Ens reptarem per fer servir algunes de les següents estratègies d'autodefensa gràfica:
* subvertising - subvertir/modificar les imatges publicitaries
* collage - ajuntar imatges per aconseguir un efecte inesperat
* meme - fer servir una imatge coneguda de forma divertida per transmetre un pensament 
* sobreidentificació - exagerar la identificació amb la imatge per evidenciar la seva absurditat
* fake - fent uns canvis mínims en el disseny nociu fer-ho servir per transmetre una idea contrària

#### Continguts
En les sessions farem servir els següents programes de codi lliure:
* gimp
* inkscape
* hotglue (disponible a hotglue.me)

A tota l'obra feta durant les sessions aplicarem les llicències lliures CreativeCommons.

#### Públic destinatari
El curs està prioritzant les dones, persones no-binaries i altres persones submises a la discriminació. Es dóna benvinguda a totes les persones que vulguin contar les discriminacions que pateixen.
No calen coneixements d'aquests programares, però si - ús de l'ordinador (ratolí, teclat...)

### Requeriments:
- Ordinadors amb gimp i inkscape - programari lliure multiplataforma. Les usuàries poden fer servir les seves màquines o es demanarà l'ús de les màquines del centre.
- Sala amb connexió a la Internet
- Projector

#### Durada:
- Total: 4 sessions.
- Farem una sessió de 2 hores a la setmana, als matins.