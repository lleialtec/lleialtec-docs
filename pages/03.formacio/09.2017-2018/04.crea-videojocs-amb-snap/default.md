---
title: 'Crea videojocs amb Snap!'
---

#### Descripció

Saber programar comença a ser tan necessari com llegir i escriure. En aquest curset, ens introduirem a la programació tot creant videojocs amb Snap!, un senzill i potent llenguatge de programació visual amb el que podem crear tot allò que siguem capaços d'imaginar. 

#### Objectiu

Aquest curset vol demostrar que aprendre a programar amb llenguatges de programació visual és molt fàcil i, a més, estimula la creativitat i el pensament computacional. Un cop entès com funciona l'entorn, podem anar aprenent de forma autònoma i crear els nostres jocs o historietes personalitzades.


#### Metodologia

Per a les nostres creacions, farem servir el programari lliure [Snap!](https://snap.berkeley.edu/) un projecte vinculat a la Universitat de Berkeley, Califòrnia. 


#### Continguts

Anirem coneixent l'entorn i els blocs de programació tot creant petits jocs, com un laberint i un joc de persecució, entre d'altres.


#### Públic

Persones de més de 10 anys, especialment joves i adults que vulguin desenvolupar la seva creativitat amb la programació. 


#### Durada

Nº de sessions: 4  
Durada de la sessió: 1h30  
Horari: tardes a convenir  


#### Requeriments

- Projector 
- Ordinadors, teclats i ratolins
- Connexió a Internet o tenir el programa instal·lat a l'ordinador. Es pot descarregar des d'[aquí](https://github.com/jmoenig/Snap--Build-Your-Own-Blocks).
