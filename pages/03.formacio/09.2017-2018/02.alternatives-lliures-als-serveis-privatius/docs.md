---
title: 'Introducció a eines digitals lliures i ètiques'
taxonomy:
    category:
        - docs
visible: true
---

## Introducció

Fem servir cada dia dispositius que no sabem com funcionen i que hem sentit que "ens espien". Siguin aplicacions de mòbil, pàgines web, sistemes operatius o serveis de correu, tots tenen un aire de misteri que d'alguna manera ens intimida. Nosaltres us proposem començar a esvair aquestes boires aprenent com funciona internet, el "núvol", el whatsapp, les webs, la telefonia mòbil, etc. i així, poc a poc, anar guanyant capacitats per poder prendre decisions informades. Volem fer servir aquesta aplicació? De debò no es poden bloquejar els anuncis? Com puc fer perquè Google em deixi d'assatjar?! Abordarem aquestes inquietuds i les que sorgeixin en grup, i a mesura que avanci el curs, anirem aprenent eines i serveis més ètics i respectuosos amb la nostra llibertat. Ens aproparem a les tecnologies de les quals  l'eix central sigui el benestar individual, coŀlectiu, i mediambiental, i aprendrem a protegir-nos de les tecnologies fetes per al benefici econòmic corporatiu. Ens esforcem per mantenir-ho assumible per a tothom qui hagi fet servir mòbils o ordinadors! "Nivell 0"!


### Objectius

Aprendre a buscar les solucions tecnològiques adaptades a les necessitats de les persones, amb criteris ètics.
Dotar a la població d’eines lliures per facilitar la sobirania tecnològica.

## Metodologia

Combinarem explicacions teòriques, activitats participatives i treball pràctic d’instaŀlació i aprenentatge de noves eines.

En la part teòrica es facilitaran els coneixements amb ajuda de materials audiovisuals i acompanyarem coneixement amb dinàmiques participatives.
En la part pràctica farem presentacions de eines amb la creació de comptes o installacions del programari.

## Continguts

- Model econòmic en el capitalisme digital, les seves implicacions socials
- Criteris d’avaluació tecnològica segons responsabilitat mediambiental, social, privadesa i descentralització
- Com funciona internet i la xarxa telefònica
- Aprofundiment d’algun servei o aplicació, extensió d’altres alternatives (basic)
- Valors del programari lliure (bàsic)
- Alternatives a serveis corporatius: correu, mapes, espai “de núvol”, contactes, calendari


## Públic destinatari
Persones que facin servir serveis o programes de corporacions com Google, Facebook, Microsoft i Apple, i tinguin ganes de descobrir-hi alternatives i recuperar una mica de control en la seva vida digital.

Buscarem la diversitat del grup per afavorir les necessitats de les persones invisibilitzades en la tecnologia. No calen coneixements especifics, però si - ús de l'ordinador (ratolí, teclat...)

## Durada

4 sessions de 2 hores cadascuna.

## Requeriments

Necessitem una sala amb taules i cadires, connexió a internet, endolls, amb material de dibuix com una pissarra i desitjablement projector. Les persones participants podran portar el seu dispositiu (mòbil, portàtil) en algunes sessions per a canviar el software que fan servir.