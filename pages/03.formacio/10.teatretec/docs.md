---
title: TeatreTec
taxonomy:
    category:
        - docs
visible: true
---

Estem col·laborant amb la comissió de Teatre de la Lleialtat per dur a terme **una peça teatral sobre tecnologia**. En un primer moment, vam pensar d'adaptar continguts sobre pensament computacional per explicar, com si fos màgia, conceptes com el sistema binari. 

Però després d'unes reunions i reflexions, hem pensat que seria interessant fer una obra més generalista, on es faci una analogia entre la restauració o consum agroecològic i les tecnologies digitals. Per fer-ho, estem mirant d'adaptar l'article: ["¿Carne o pescado?"](https://creachispa.blogspot.com/2012/10/carne-o-pescado-la-metafora-del.html) i tenim previst estrenar-la pel març de les dones del 2020. 

Seguirem informant!