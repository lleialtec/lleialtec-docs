---
title: 'Taller de formació a Viquillibres'
taxonomy:
    category:
        - docs
visible: true
---

##### Títol
Taller de formació a Viquillibres
##### Dia
9 de juliol
##### Horari
De 10h a 13h
##### Descripció
Viquillibres és una biblioteca de llibres de text i manuals lliures, un projecte Wikimedia germà de la Viquipèdia. Farem un taller per saber com funciona i com es pot col·laborar amb el projecte Viquillibres.
##### Preu
Taller gratuït. Amb reserva prèvia. Apunteu-vos enviant un correu a: **info@tec.lleialtat.cat**

[Més informació](http://viquillibres.cat)