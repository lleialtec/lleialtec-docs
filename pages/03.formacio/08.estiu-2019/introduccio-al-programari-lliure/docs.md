---
title: 'Introducció al programari lliure'
taxonomy:
    category:
        - docs
visible: true
---

##### Títol
Introducció al programari lliure
##### Dia
3 de juliol
##### Horari
De 18h a 20h
##### Descripció
A la Lleialtec només fem servir programari lliure perquè vetllem pels drets digitals de les nostres veïnes. Vine a conèixer quines tecnologies fem servir i perquè és important que siguin lliures i ètiques!  
##### Preu
Xerrada gratuïta. Amb reserva prèvia. Apunteu-vos enviant un correu a: **info@tec.lleialtat.cat**

[Més informació](http://documents.lleialtat.cat/LleialTec/lleialtec-intro-pl.pdf).