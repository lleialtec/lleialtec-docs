---
title: 'Telecomunicacions neutrals'
published: true
taxonomy:
    category:
        - docs
visible: true
---

##### Títol
Com Lleialtat Santsenca participa de la xarxa de comuns guifi.net de la mà d'exo.cat
##### Dia
25 de juny
##### Horari
De 10h a 13h
##### Preu
Xerrada/debat gratuïta amb reserva prèvia. Apunteu-vos enviant un correu a: **info@tec.lleialtat.cat** 
##### Descripció
Es presentaran els projectes de Lleialtat amb guifi.net (wifi usuaris, antena al teulat i interconnexió amb mesh de Sants). El projecte de telecomunicacions present i futur a Lleialtat Santsenca té una mica de cada cosa: wifi per usuaris, infraestructures de servidors i aprofitament de fibra òptica per la
xarxa de comuns guifi.net. La xerrada-debat començarà per presentar com va ser el primer desplegament de wifi d'usuaris i com ha evolucionat a l'actual desplegament més corporatiu. També, sobre què es vol fer al terrat amb una antenna de guifi.net al teulat, que donarà cobertura a veïns, i com es pot aprofitar la fibra òptica present per interconnectar amb la xarxa mesh de Sants. La xerrada és per tots els públics, s'adaptarà als assistents, podent-se dirigir més a la part social-organitzativa i/o tècnica.

