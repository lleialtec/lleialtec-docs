---
title: 'Monedes Socials Digitals'
taxonomy:
    category:
        - docs
visible: true
---

##### Títol
Monedes Socials Digitals
##### Dia
26 de juny
##### Horari
De 16h a 19h
##### Descripció
Sabies que a la Lleialtat estem experimentant amb monedes socials digitals? Vine a conèixer què són, com funcionen i digues la teva en quin ús fer-ne en contextos d'Economia Solidària! Farem una xerrada a tres mans: la primera part, sobre monedes socials; la segona, sobre els experiments que estem duent a terme a la Lleialtat; i a la tercera part, farem una demo! :) Per cert, el programa informàtic que fem servir, es diu [Kvartalo](https://github.com/kvartalo) (barri en Esperanto). 
##### Preu
Xerrada/debat + Demo. Activitat gratuïta amb reserva prèvia. Apunteu-vos enviant un correu a: **info@tec.lleialtat.cat**

[Més informació](https://santsenques.cat/tag/kvartalo)

