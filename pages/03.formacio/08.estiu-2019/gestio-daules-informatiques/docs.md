---
title: 'Gestió d''Aules Informàtiques'
visible: true
---

##### Títol
Gestió d'Aules Informàtiques amb programari lliure
##### Dia
27 de juny
##### Horari
De 10h a 13h
##### Descripció
A la Lleialtat tenim una aula lliure, una sala amb ordinadors reutilitzats que funcionen amb programari lliure. Vine a conèixer com gestionar i mantenir una aula lliure amb el "Fog Server". 
##### Preu
Taller gratuït. Amb reserva prèvia. Apunteu-vos enviant un correu a: **info@tec.lleialtat.cat** 

[Més informació](https://tec.lleialtat.cat/infraestructura/aules-lliures)