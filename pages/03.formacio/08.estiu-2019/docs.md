---
title: 'Estiu 2019'
media_order: suport-formacio-graella.png
taxonomy:
    category:
        - docs
visible: true
---

Enguany concentrem activitats formatives des **del 25 de juny fins al 12 de juliol**. Hi haurà ponències, cursos intensius, tallers i xerrades. Hi ha un curs de pagament i un altre de taquilla inversa, la resta d'activitats són gratuïtes amb reserva prèvia. Consulta [el cartell](http://documents.lleialtat.cat/LleialTec/cartell-formacio-estiu-lleuger.png). 

**Inscripcions a: info@tec.lleialtat.cat**
![](suport-formacio-graella.png)
##### Infraestructura pròpia
Des de la Lleialtec promovem i implementem [infraestructures lliures i ètiques](https://tec.lleialtat.cat/infraestructura). A principis del 2018, vam muntar una aula lliure amb un sistema (fog server) de gestió d'equips informàtics des d'un servidor d'imatges. I gràcies al projecte Equipaments Lliures hem pogut desenvolupar un ERP de gestió per al Cafè amb Dolibarr i desplegar telecomunicacions neutrals amb Guifi.net. 

Ens ho explicaran millor companyes de Lleialtec, Guifi.net i Lliure TIC en aquestes activitats sobre infras pròpies.

* [Com Lleialtat Santsenca participa de la xarxa de comuns guifi.net de la mà d'exo.cat](https://tec.lleialtat.cat/formacio/estiu-2019/telecomunicacions-neutrals) 25 de juny. De 10h a 13h (activitat gratuïta amb inscripció prèvia)
* [Com hem creat el programa de gestió per al Cafè de la Lleialtat](https://tec.lleialtat.cat/formacio/estiu-2019/restauracio-i-programari-lliure) 26 de juny. De 10h a 13h (activitat gratuïta amb inscripció prèvia)
* [Gestió d'Aules Informàtiques](https://tec.lleialtat.cat/formacio/estiu-2019/gestio-daules-informatiques). 27 de juny. De 10h a 13h (activitat gratuïta amb inscripció prèvia)

##### Cursos intensius 
* [Libre Office Writer (nivell intermedi)](https://tec.lleialtat.cat/formacio/estiu-2019/curs-libre-office-writer) (activitat de pagament) 25h. Del dilluns 1 fins al divendres 5 de juliol, de 9h a 13h.

Així com a altres espais de transformació social, a la Lleialtat fem servir aplicacions com [Libre Office](https://www.libreoffice.org/), una solució d'ofimàtica lliure i ètica amb la que es poden crear documents de text, fulles de càlcul, presentacions, etc. Libre Office és una alternativa lliure i gratuïta als serveis privatius de Microsoft (M$) Office; una eina eficient i ètica, desenvolupada en comunitat per a la comunitat. 

Vine a conèixer l'eina més a fons en aquesta formació! Curs ideal per a docents i PAS (Personal d'Administració i Serveis). 

* [Snap! (nivell bàsic)](https://tec.lleialtat.cat/formacio/estiu-2019/aprenentatge-creatiu-amb-snap-i) (activitat de taquilla inversa, amb inscripció prèvia) 20h. Del dilluns 8 fins al divendres 12 de juliol, de 16h a 20h.

Sabies que estem muntant una [Snap!Arcade](https://snaparcade.cat/)? Aquesta màquina eduRecreativa funciona amb un llenguatge de programació anomenat Snap! i està sent creada per desencadenar sinergies entre persones i col·lectius del barri. 

Vine a aprendre com de fàcil és crear projectes per a la maquineta! Curs ideal per a docents de primària i secundària, monitores d'esplai, professorat d'idiomes i veïnes curioses. Aquest curs és una versió presencial d'un pilot formatiu online a l'xtec (Xarxa Telemàtica Educativa de Catalunya). 

##### Tallers introductoris
Des de l'Amical Wikimedia i la Universitat Lliure de Sants, ens arriben propostes creatives en relació a textos, gràfics i control de versions. Viquillibres és una iniciativa per crear llibres en línia, i facilitar així la compartició de coneixement sota llicències lliures i en català. Inkscape i GIMP són programes per crear i modificar gràfics: imatges, dibuixos, etc. Tots dos, programari lliure i ètic que no captura ni envia informació a cap empresa. Inkscape ens permet el dibuix vectorial; i GIMP, manegar mapes de bits. GIT és un sistema de control de versions tan per textos en una web com per a les versions dels programes. 

Vine als tallers i introdueix-te a eines lliures, ètiques i creatives!

* **GIT** (activitat gratuïta amb inscripció prèvia) 4h. Dimarts 25 (2h) i dijous 27 (2h) de juny, de 18h a 20h.
* **Inkscape i GIMP** (activitat gratuïta amb inscripció prèvia) 4h. Dimarts 2 (2h) i dijous 4 (2h) de juliol, de 18h a 20h. **ATENCIÓ: el dia 4 el taller es farà de 17h a 19h.** 
* [Viquillibres](https://tec.lleialtat.cat/formacio/estiu-2019/taller-de-formacio-a-viquillibres) (activitat gratuïta amb inscripció prèvia) 3h. Dimarts 9 de juliol, de 10h a 13h.

##### Xerrades divulgatives
Les tecnologies digitals són líquides, i es fiquen per tot arreu! Tot i usar-les però, -si ho pensem bé-, no les acabem d'entendre. Un problema d'aquest inici de segle és el model de negoci dels serveis digitals més coneguts, que solen guanyar molts diners amb les dades de les seves usuàries. Un altre problema relacionat, és que les empreses que desenvolupen aquests serveis anteposen els diners a l'ètica i provoquen danys irreparables, especialment en les persones que ja són vulnerables: col·lectius de dones, persones de raça no blanca, etc. 

La tecnologia no és neutra i som les persones que la usem les que hem de recolzar tecnologies que a més de lliures, siguin ètiques. Vine a descobrir perquè és important el programari lliure, a com defensar-nos digitalment, i a jugar amb monedes socials digitals!

* [Monedes socials digitals](https://tec.lleialtat.cat/formacio/estiu-2019/monedes-socials-digitals) (activitat gratuïta amb inscripció prèvia) 3h. Dimecres 26 de juny, de 16h a 19h.
* [Introducció al programari lliure](https://tec.lleialtat.cat/formacio/estiu-2019/introduccio-al-programari-lliure) (activitat gratuïta amb inscripció prèvia) 2h. Dimecres 3 de juliol, de 18h a 20h.
* [Introducció a la seguretat digital](https://tec.lleialtat.cat/formacio/estiu-2019/seguretat-digital-bàsica) (activitat gratuïta amb inscripció prèvia) 6h. Dimecres 10 (3h) i dijous 11 (3h) de juliol, de 10h a 13h.

**Col·laboren**: Barcelona Free Software, Unilliure, Guifi.net / eXO, Lliure TIC, Amical Wikimedia, Snap!, Dones Tech, i altres professionals i voluntaris de la comunitat del programari lliure. 
