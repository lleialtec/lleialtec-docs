---
title: 'FEDIVERSE, xarxes socials lliures'
published: false
taxonomy:
    category:
        - docs
visible: true
---

##### Títol
FEDIVERSE, xarxes socials lliures i ètiques
##### Dia
11 de juliol
##### Horari
De 10h a 13h
##### Descripció
Què és realment Internet? Què és realment la Web? Qué són, en veritat, les xarxes socials? Les tecnologies no són neutres i haurien de ser dissenyades per tal que respectin les seves usuàries. Les xarxes socials més conegudes no són, ni de bon tros, les més ètiques. Si vols conèixer què fa lliure i ètic un servei de xarxa social, t'esperem a la Lleialtat Santsenca. 
##### Preu
Xerrada/debat + demo. Activitat gratuïta amb reserva prèvia. Apunteu-vos enviant un correu a: **info@tec.lleialtat.cat**

[Més informació](https://tec.lleialtat.cat/divulgacio/fediverse)