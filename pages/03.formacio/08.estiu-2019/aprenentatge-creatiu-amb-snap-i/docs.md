---
title: 'Aprenentatge creatiu amb Snap! (I)'
taxonomy:
    category:
        - docs
visible: true
---

##### Títol
Aprenentatge creatiu amb Snap! (I). Nivell bàsic.
##### Dies
Del 8 al 12 de juliol
##### Horari
De 16h a 20h
##### Total
20 hores
##### Preu
Taquilla inversa (sense obligació, aporta el que puguis o vulguis)
##### Descripció
Aquest curs ofereix una introducció a la programació creativa mitjançant [el llenguatge de programació Snap!](https://snap.berkeley.edu/). Cada sessió constarà de dues pràctiques en les quals construirem un projecte complet des de zero. Els tipus de projectes que realitzarem aniran des de la generació de formes geomètriques fins a videojocs, passant per les històries animades, la música o el tractament de text. 

Apunteu-vos enviant un correu a: **info@tec.lleialtat.cat**

[Més informació](http://ateneu.xtec.cat/wikiform/wikiexport/materials/scl/scl1/index)
##### Temari
* 0) Començant amb Snap!. Introducció i presentació de l'entorn
* 1) Animacions. Primeres seqüències programades. Stop motion i seqüenciador
* 2) Geometria, formes i colors. Abstracció i creació de blocs
* 3) Històries i jocs. Acudit gràfic i el joc del gat i la rata
* 4) Llenguatge i música. Generador de frases i orgue amb escales
