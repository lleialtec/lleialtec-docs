---
title: 'Curs Libre Office Writer'
taxonomy:
    category:
        - docs
visible: true
---

##### Títol
Curs LibreOffice Writer, Nivell Intermedi.
##### Dies
Del 1 al 5 de juliol
##### Horari
De 9h a 14h
##### Total
25 hores
#### Preu 
100 euros
##### Descripció
Aquest curs es centra en el processador de textos Writer i va dirigit a tots aquells usuaris amb un nivell de coneixement bàsic que volen fer un ús més eficient de l’aplicació. 

Apunteu-vos enviant un correu a: **curs-libreoffice@bcnfs.org** o a **info@tec.lleialtat.cat**

[Més informació](https://bcnfs.org/curs-libreoffice/)

##### Temari
* Introducció al curs
* Repàs nivell bàsic
 * Personalització de la interfície
 * Formatació i ordenació de llistes
 * Taules
* Estils
* Plantilles
* Importar i Crear gràfics
* Formularis
* Documents mestres
* Combinació de Correspondència