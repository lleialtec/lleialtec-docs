---
title: 'Seguretat digital bàsica'
published: true
taxonomy:
    category:
        - docs
visible: true
---

##### Títol
Introducció a la seguretat digital
##### Dia
10 i 11 de juliol
##### Horari
De 10h a 13h
##### Descripció
El món de la tecnologia digital pot ser hostil i desconegut ja que no és d'entrada un espai de benestar en quant a la privacitat, els nostres drets o la seguretat, doncs hi ha molts actors i interessos creuats. Per tal d'encaminar una autosuficiència en el procés d'interacció digital i d'incorporar decisions més conscients i adaptades a cadascuna de nosaltres en els aspectes digitals, us animem a participar d'aquest taller. No és necessari un coneixement tècnic per a assistir-hi sinó que volem compartir reflexions ètiques i ciberfeministes, com l'ús del programari lliure o les estratègies de seguretat holística, per a crear un espai d'intercanvi i aprenentatge.
##### Preu
Xerrada/debat. Activitat gratuïta amb reserva prèvia. Apunteu-vos enviant un correu a: **info@tec.lleialtat.cat**

[Més informació](https://tec.lleialtat.cat/divulgacio/fediverse)