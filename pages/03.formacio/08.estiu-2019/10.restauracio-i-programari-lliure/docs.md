---
title: 'Restauració i programari lliure'
taxonomy:
    category:
        - docs
visible: true
---

##### Títol
Com hem creat el programa de gestió per al Cafè de la Lleialtat
##### Dia
26 de juny
##### Horari
De 10h a 13h
##### Preu
Xerrada amb demostració. Activitat gratuïta amb reserva prèvia. Apunteu-vos enviant un correu a: **info@tec.lleialtat.cat** 
##### Descripció
La gestió informàtica del Cafè de la Lleialtat Santsenca (La Capirota) s’ha desenvolupat amb el programari lliure ERP/CRM Dolibarr. [La seva implantació](/infraestructura/erp-del-cafe) s’ha fet en diferents fases: anàlisi de requeriments, configuració del sistema, formació, posada en marxa i seguiment. Tot aquest procés es vol compartir per a que sigui replicable per impulsar altres Cafès amb sistemes lliures, des de la seva gestió (productes, preus, stocks, tiquets, factures, proveïdors, ...) fins al maquinari utilitzat (pantalla tàctil, impresora de tiquets, calaix, ...). La xerrada és per tots els públics, s'adaptarà als assistents, podent-se dirigir més a la part social-organitzativa i/o tècnica.

[Més informació](https://tec.lleialtat.cat/infraestructura/erp-del-cafe)