---
title: 'Cursos i Tallers'
published: false
taxonomy:
    category:
        - docs
visible: true
---

Des de Lleialtec proposem diferents formacions en el àmbit digital i tecnopolític:

- [Tecnologia ètica i feminista](https://tec.lleialtat.cat/formacio/tallers/tecnologia-etica-i-feminista)
- [Introducció a gnu/linux per dones](https://tec.lleialtat.cat/formacio/tallers/introduccio-a-gnu-linux-per-dones)
- [Crea videojocs amb Snap!](https://tec.lleialtat.cat/formacio/tallers/crea-videojocs-amb-snap)
- [Autodefensa gràfica amb eines lliures digitals](https://tec.lleialtat.cat/formacio/tallers/autodefensa-grafica-amb-eines-lliures-digitals)
- [Seguretat i privacitat digital bàsica amb ordinador](https://tec.lleialtat.cat/formacio/tallers/seguretat-i-privacitat-digital-basica-amb-ordinador)
- [Seguretat holística en els mòbils](seguretat-holistica-en-els-mobils)
- [Alternatives lliures als serveis privatius](https://tec.lleialtat.cat/formacio/tallers/alternatives-lliures-als-serveis-privatius)
