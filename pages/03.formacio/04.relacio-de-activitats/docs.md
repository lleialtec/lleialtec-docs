---
title: 'Relació d''activitats'
published: false
taxonomy:
    category:
        - docs
visible: true
---

## 2018

### Cursos

**Introducció al programari lliure i tecnologia feminista**  
6 sessions  
La tecnologia s’explica des de lògiques comercials i capitalistes, a banda de la dimensió
productivista i patriarcal de l’internet. Volem acompanyar una aproximació a les tecnologies
lliures i feministes per adquirir coneixement i consciència crítica.

**Seguretat i privacitat digital bàsica**  
8 sessions  
En la nostra cotidianitat està present la informàtica i tractem amb dades digitals, de forma individual o col·lectiva. Per poder millorar la seguretat en aquest àmbit i garantir la privacitat, eś important entendre el funcionament de les activitats bàsiques com la navegació per internet, la gestió de contrasenyes o la missatgeria instatània, i conèixer ferramentes i pràctiques que estiguen de la nostra part.

**Introducció al desenvolupament web (per a dones)**  
6 sessions  
L’accés a les tecnologies per a les dones i sobretot les dones més grans encara és una dificultat. Poder oferir la oportunitat de conèixer aquest món i fins i tot animar-se a construir un lloc web propi ajuda a desmitificar les tecnologies i trencar barreres.


### Altres activitats 

**Muntatge Aula Informàtica**  
Dissabte 13/01

Activitat oberta  
Aprofitant el muntatge de l’Aula Informàtica de la Lleialtat es va fer un taller de la pròpia instal·lació. La idea era ajuntar gent amb coneixements mitjos de GNU/Linux amb gent que pogués tenir interès en aprendre a muntar una aula de PC’s (especialment gent d’altres espais del barri com el Casal de Joves, Can Batlló, entre d’altres). 

El plantejament del taller va ser el següent:
1. Planificar el disseny de la sala, distribució de les taules...
2. Parlar de la xarxa, trames ethernet, IP, DHCPTFTP
3. Què són imatges, perquè... . 
4. Dividir-se en grups i instal·lar sistema operatiu de servidor i software de gestió de PC’s.
5. Posar les taules, cablejar l’aula, instal·lar els PC’s
6. Instal·lar sistema operatiu en PC’s i crear imatges.
7. Programar manteniment desatès. 

**Install Party**  
Dissabte 03/02  
La idea és que qui vulgui pugui portar el seu PC o portàtil i, entre tots, s’instal·la una distribució de linux (com ara Debian, Ubuntu, Fedora, Archlinux, ...).

**Congrés SobTec**  
Dissabte 03/03  
El Congrés de Sobirania Tecnològica és un punt de trobada per discutir la sobirania tecnològica mitjançant xerrades i tallers. Un dels objectius és fer créixer la sobirania tecnològica.

## 2017

### Cursos

**Crea videojocs amb Snap!**  
23/08 a les 19h  
Snap! És un llenguatge de programació visual que permet crear projectes interactius molt fàcilment. Vine a crear el teu primer videojoc!
A càrrec de TicTacTep: Tecnologies creatives. 
En el marc de les portes obertes que es van fer per la Festa Major de Sants

**Seguretat Informàtica bàsica**  
Del 30 d’octubre a l’11 de desembre (dues vegades al mes)  
Introducció a la seguretat informàtica, i instal·lació d’un sistema tipus GNU/Linux, el Debian, amb les mesures de seguretat bàsiques. A cada classe començarem amb la teoria i acabarem amb la pràctica. Enfocat a alumnes que no sàpiguen d’informàtica.

**Dels bits a la tinta**  
Del 3 de novembre al 15 de desembre  
Curs d’apoderament tecnològic, comunitari i artístic. Les persones participants crearan una revista consensuant i elaborant-ne els continguts amb programari lliure! Aprendrem a fer anar programes d’edició gràfica (com GIMP, Inkscape o Krita) i de maquetació (com Scribus) però, sobretot, passarem una bona estona! Vine a crear la teva primera revista lliure en comunitat!

**Iniciació a Sistemes Operatius Gnu/Linux per a dones** (i des d’una perspectiva feminista)  
Del 9 al 30 de novembre  
Les tecnologies lliures estan més a prop d’una visió feminista de les tecnologies, doncs tenen unes opcions que permeten una relació més confiada i apoderada amb les eines, a banda de proporcionar uns usos més ètics. Aquest curs està orientat a dones i altres persones de gènere no masculí que hagin decidit utilitzar Gnu/Linux i vulguin un acompanyament i guia per a iniciar-s’hi!

**Introducció al desenvolupament i maquetació web per a dones**  
Del 10 de novembre a l’1 de desembre  
Per a fomentar l’apoderament de les dones en el món tecnològic volem proposar un curs per a generar autonomia i confiança en el món d’Internet i poder crear i mantenir els propis llocs web. Per personalitzar gràficament les nostres webs, farem una introducció al disseny css i maquetació html!

**Introducció a la seguretat holística per a activistes**  
22-23/12  
La concepció de la seguretat és una qüestió relativa però ens pot afectar de moltes maneres. Per a fer-hi front de manera global i millorar el benestar personal i col·lectiu, podem aproximar-nos-hi des de 3 esferes entre-relacionades: la seguretat digital, la social i la física. Proposem diverses dinàmiques i reflexions per a pensar i aprendre estratègies, prevenir riscs i mitigar danys.

**Taller Postals nadalenques interactives**  
12,13, 19 i 20 de desembre  
Aquest Nadal, sigues original! Regala postals interactives. T’ensenyem a crear-les i compartir-les!

### Xerrades/Taules debat

**La Sobirania Tecnològica a La Lleialtat**  
20/08  a les 12h  
En aquesta taula rodona, convidem a repensar el model tecnològic imperant (i privatiu) als entorns comunitaris. Per fer-ho, parlarem de les eines i accions imprescindibles per construir, de baix a dalt, una societat digital lliure.  
Amb la participació d’Infras i sistemes, SobTec, TicTacTep, guifi.net i Exo.cat.  
En el marc de les portes obertes que es van fer per la Festa Major de Sants  

**Economia Social i Solidària i programari lliure**  
Dissabte 26/08 a les 12h  
En aquesta taula rodona convidem a repensar el model tecnològic imperant (i privatiu) a l’Economia Social i Solidària. Per fer-ho parlarem de mancances i teixirem estratègies per prendre consciència digital i construir, de baix a dalt, una societat digital lliure. 
Amb la participació de SobTec i l’Impuls Cooperatiu de Sants.   
En el marc de les portes obertes que es van fer per la Festa Major de Sants

**Taula rodona Blockchain i criptomonedes**  
Dissabte 9/12 d’11h a 13h  
Amb refrigeri

### Altres activitats 

**Formatem la Lleialtat!**  
Diumenge 27/08 a les 11h  
En aquesta activitat coneixerem sistemes operatius lliures i ètics, aprendrem a instal·lar-los als ordinadors per començar a construir, de baix a dalt, una societat digital lliure i posarem en marxa la implementació del pla per la estructura lliure a la Lleialtat. 
A càrrec de TicTacTep: Tecnologies creatives.  
En el marc de les portes obertes que es van fer per la Festa Major de Sants

**ScratchEd Meetup**  
16/09 i 16/12  
Trobada oberta d’educadors i educadores que volen aprendre en comunitat, compartint idees i estratègies per promoure la creativitat computacional en totes les seves formes.
