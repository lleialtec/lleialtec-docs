---
title: 'Còpies de seguretat'
media_order: Placas_tectonicas_mayores.svg.png
---

Els serveis com [la web de la Lleialtat](https://www.lleialtat.cat/), [el pad](https://pad.lleialtat.cat/), [la web lleialtec](https://tec.lleialtat.cat/), [el núvol](https://tec.lleialtat.cat/infraestructura/nuvol), [el butlletí](https://tec.lleialtat.cat/infraestructura/butlleti), [l'ágora](https://tec.lleialtat.cat/infraestructura/agora), -més els fitxers de [l'Àlbum familiar](https://www.lleialtat.cat/tria-una-foto-familiar-i-explicans-la-teva-historia-posem-en-marxa-lalbum-familiar-del-barri/) i altres-, estan allotjats a Suècia, [amb una gent que respecten el medi ambient dins dels possibles](https://tranquillity.se/) (la informàtica és molt agressiva amb el Planeta). 

Fins ara, les dades d'aquests serveis es copiaven periòdicament i de manera automatitzada a un altre servidor que només servia per aquesta tasca: un lloc on desar les còpies. Aquest servidor de còpies també estava allotjat amb els suecs. 

¿Qué passaria si hi hagués un incendi a l'edifici on estan tots aquests servidors? Doncs que ho perdríem tot, còpies de seguretat incloses. És per això que hauria de ser de sentit comú el fet que el lloc es on guardin les còpies de seguretat estigui en un lloc geogràficament diferent. I, fins ara, no era el nostre cas.

Però ara sí: hem habilitat un servidor a la mateixa Lleialtat que **connecta cada nit amb els servidors de Suècia i s'emporta una còpia de tot** cap al nostre edifici. 

Com anècdota, explicar que la llei de protecció de dades alemanya diu que les còpies no només han d'estar en un lloc geogràfic diferent del de la font de les dades, sinó que també han d'estar en una altra placa tectònica.

![](Placas_tectonicas_mayores.svg.png)

La Lleitat no compleix la llei alemanya: si s'enfonsa Suècia, nosaltres amb ella :P

Consulta la [documentació tècnica](https://tec.lleialtat.cat/howtos/backups).


### ES

Los servicios como [la web de la Lleialtat](https://www.lleialtat.cat/), [el pad](https://pad.lleialtat.cat/), [la web lleialtec](https://tec.lleialtat.cat/), [la nube](https://tec.lleialtat.cat/infraestructura/nuvol), [el boletín](https://tec.lleialtat.cat/infraestructura/butlleti), [el ágora](https://tec.lleialtat.cat/infraestructura/agora), -más los ficheros del [Àlbum familiar](https://www.lleialtat.cat/tria-una-foto-familiar-i-explicans-la-teva-historia-posem-en-marxa-lalbum-familiar-del-barri/) y otros-, están alojados en Suecia con una gente que respetan el medio ambiente dentro de lo posible (la informática es muy agresiva con el Planeta). [https://tranquillity.se/](https://tranquillity.se/)

Hasta ahora los datos de estos servicios se copiaban periódicamente i de manera automatizada a otro servidor que solo servía para esta tarea: un sitio donde guardar las copias. Ese servidor de copias estaba también alojado con los suecos.

¿Qué pasaría si hubiera un incendio en el edificio donde están todos esos servidores? Pues que lo perderíamos todo, copas de seguridad incluidas. Por eso, debería ser de sentido común que el sitio donde se guarden las copias de seguridad esté en un lugar geográficamente distinto. Y hasta ahora no era nuestro caso.

Pero ahora sí: hemos habilitado un servidor en la misma Lleialtat que conecta cada noche con los servidores de suecia y se lleva una copia de todo hasta nuestro edificio. 

Como anécdota, explicar que la ley de protección de datos alemana dice que las copias no solo deben estar en un sitio geográficamente distinto que la fuente de los datos, sino que también deben estar en otra placa tectónica.

![](Placas_tectonicas_mayores.svg.png)

La Lleialtat no cumple con la ley alemana. Si se hunde suecia, nosotras con ella. :P

Consulta la [documentación técnica](https://tec.lleialtat.cat/howtos/backups).