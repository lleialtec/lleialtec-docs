---
title: GNGforms
media_order: gngforms.screen.png
published: true
taxonomy:
    category:
        - docs
visible: true
---

**En català**

Disminuir la quantitat de dades que obté g00gle de les persones no és fàcil. Però a la Lleialtat Santsenca fem tots els possibles per aconseguir-ho. Des dels inicis del projecte, hem fet ús de programari lliure i ètic i, per tant, el nostre rastre al món dels negocis de multinacionals és menor. Per seguir en aquesta línia i protegir-nos tant a nosaltres mateixes com a les persones que ens envolten, hem creat una alternativa als formularis de g00gle que hem anomenat [GNGforms](https://formularis.lleialtat.cat/).

A més de l'ús que en fa la Lleialtat, volem facilitar l'accés a aquest programa a altres veïnes per tal que puguin confeccionar formularis de manera soberana. Cada cop que una iniciativa de barri usa formularis de g00gle per recollir firmes, el nombre d'assistents a una activitat o per gestionar els correus de les persones interessades en alguna mobilització social, en realitat, està alimentant al monstre de g00gle amb més dades, dades que és millor que no tingui. 

La multinacional ja té prou dades nostres: sap on som, amb qui estem, sobre quins temes parlem, coneix les nostres preocupacions, alegries, interessos i plans. Sap qui és la nostra família, qui són els nostres amics i els que no ho són tant, i quina és la nostra opinió sobre qualsevol cosa. Amb tota aquesta informació, g00gle fa negoci venent espais de publicitat, venent la nostra atenció als seus clients, convertint-nos així en el seu producte. Ens deixen fer servir el seu correu electrònic (gMail), el seu núvol (gDrive), el seus mapes (gMaps), els seus formularis (gForms), el seu buscador (gSearch), el seu navegador (gChrome), els seus documents compartits (gDocs)... 

I tots aquests serveis funcionen a la perfecció i no els hem de pagar amb diners. En realitat, però, ens tracten com a un ramat i concentren les nostres dades a les seves granges de servidors per tal de vendre'ns al millor postor. No costa diners perquè paguem amb dades personals, les nostres i les de les persones amb qui interactuen.

Considerem que un negoci que enganya les persones no és ètic, que dissenyar eines que creen dependència per segrestar la nostra atenció no és ètic; que aprofitar-se d'aquesta concentració de dades per a possibilitar un control social sense precedents no és ètic. I no tenim per què vendre'ns: tenim totes les alternatives que necessitem, i si en falta alguna, simplement es demana i es crea. Així funcionem a la comunitat del programari lliure, i que va molt més enllà del codi obert, que només és la metodologia de poder llegir el codi d'un programa.

És per això que estem desenvolupant GNGforms, una eina lliure i ètica pensada per a persones, col·lectius i entitats més enllà de la pròpia Lleialtat, considerant aquesta infrastructura com a compartida:

* Una instal·lació de GNGforms és capaç d'abastir a múltiples dominis on cadascun d'ells (per exemple: forms.lamevamovida.net) té els seus propis administradores i usuàries
* No es dupliquen els mecanismes de còpies de seguretat i monitorització
* Es redueix el consum energètic
* I fem pinya :)

Si t'interessa saber més sobre el projecte, si us plau, posa't en contacte amb nosaltres. Participa de l'etiqueta [#GNGforms](https://santsenques.cat/tag/gngforms) a la Fediverse o envia'ns un correu a info@tec.lleialtat.cat 

Si vols muntar GNGforms per a la teva entitat o éssers estimats, fes una ullada al [codi aquí](https://gitlab.com/lleialtec/gngforms). Si formeu part d'una entitat que vol fer ús de formularis que respecten els drets digitals de les seves usuàries, feu una ullada als **[nostres serveis](https://serveis.tec.lleialtat.cat/)**.
![](gngforms.screen.png)

**En español**

Disminuir la cantidad de datos que tiene g00gle de las personas no es fácil. Pero en la Lleialtat Santsenca hacemos todos lo posible por conseguirlo. Desde los inicios del proyecto, hemos usado Software libre y ético y, por lo tanto, nuesto rastro dentro del mundo de los negocios multinacionales es menor. Para seguir en esa línea y protegernos tanto a nosotras mismas como a las personas que nos rodean, hemos creado una alternativa a los formularios de g00gle que hemos llamado [GNGforms](https://formularis.lleialtat.cat/).

Además del uso que hace la Lleialtat de este software, queremos facilitar el acceso a este programa a otras vecinas para que puedan confeccionar formularios de manera soberana. Cada vez que una iniciativa del barrio usa formularios de g00gle para recoger firmas, el número de asistentes de una actividad o para gestionar los correos de personas interesadas en alguna movilización social, en realidad, está alimentando al monstruo de g00gle con más datos, datos que es mejor que no tenga. 

La multinacional ya tiene muchos datos nuestros: sabe dónde estamos, con quién, sobre qué asuntos hablamos, conoce nuestras preocupaciones, alegrías, intereses y planes. Sabe quién es nuestra familia, quiénes son nuestros amigos, y los que no lo son tanto, y cuál es nuestra opinión sobre todo tipo de asunto. Con toda esta información g00gle hace negocio vendiendo espacios de publicidad, vendiendo nuestra atención a sus clientes, conviertiéndonos así en su producto. Nos dejan usar su correo electrónico (gMail), su nube (gDrive), sus mapas (gMaps), sus formularios (gForms), su buscador (gSearch), su navegador (gChrome), sus documentos compartidos (gDocs)... 

Y todos esos servicios funcionan a la perfección y no los tenemos que pagar con dinero. Pero, en realidad, nos tratan como ganado y concentran nuestros datos en sus granjas de servidores para venderlas al mejor postor. No cuesta dinero porque pagamos con nuestros datos personales, los nuestros y los de las personas con las que interactuamos.

Consideramos que un negocio que engaña a las personas no es ético, que diseñar herramientas que crean dependencia para secuestrar nuestra atención no es ético, que aprovecharse de esta concentración de datos para posibilitar un control social sin precedentes no es ético. Y no tenemos por qué vendernos: tenemos todas las alternativas que necesitamos y, si falta alguna, simplemente se pide y se crea. Así funcionamos en la comunidad del software libre y que va mucho más lejos del código abierto, que solo es la metodologia de poder leer el códido de un programa. 

Por eso estamos desarrollando GNGforms, una herramienta libre y ética pensada para personas, colectivos y entidades más allá de la propia Lleialtat, considerando esta infraestructura como compartida:

* Una instalación de GNGForms es capaz de abastecer a múltiples dominios donde cada uno de ellos (por ejemplo forms.miorganizacion.com) tiene sus propios admins y usuarios.
* No se duplican los mecanismos de copias de seguridad y monitorización.
* Se reduce el consumo energético.
* Hacemos piña.

Si te interesa saber más sobre el proyecto, por favor, ponte en contacto con nosotros. Participa de la etiqueta [#GNGforms](https://santsenques.cat/tag/gngforms) en el Fediverso o envianos un correo a info@tec.lleialtat.cat 

Si quieres montar GNGforms para tu entidad o seres queridos, échale un ojo al [código aquí](https://gitlab.com/lleialtec/gngforms). Si formáis parte de una entidad que quiere hacer uso de formularios que respeten los derechos digitales de sus usuarias, echad un ojo a **[nuestros servicios](https://serveis.tec.lleialtat.cat/)**.
![](gngforms.screen.png)