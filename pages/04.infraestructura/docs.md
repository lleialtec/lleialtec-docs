---
title: Infraestructura
taxonomy:
    category:
        - docs
visible: true
---

Les persones que participen a la CELS, -entitat gestora de l'equipament de La Lleialtat-, s'organitzen mitjançant **eines lliures i ètiques** que s'allotgen en servidors auto-gestionats. 

Dues eines principals d'organització són el [**núvol**](https://tec.lleialtat.cat/infraestructura/nuvol), on s'emmagatzemen les dades i es comparteixen arxius; i l'[**àgora**](https://tec.lleialtat.cat/infraestructura/agora), un espai de debat que serveix d'assemblea permanent online. 

L'accés al núvol es determina en funció de la pertinença: **tots els membres de la CELS tenen accés a llegir els documents del núvol**; i les persones que pertanyen a una o més comissions, tenen accés a les carpetes de núvol que li corresponen per comissió, on poden llegir i editar documents online.

A més, per a la comunicació interna i externa, es fa servir un **mail no corporatiu** i integrat al núvol; i per a la difusió d'esdeveniments, s'utilitza una pàgina web i un [**butlletí**](https://tec.lleialtat.cat/infraestructura/butlleti) digital respectuós amb les dades de les usuàries. 

A La Lleialtat tenim accés a Internet, la [**xarxa**](https://tec.lleialtat.cat/infraestructura/xarxa) de xarxes. I disposem d'una [**aula informàtica**](https://tec.lleialtat.cat/infraestructura/aules-lliures) que si bé va ser creada per donar suport al projecte de La Troca, dóna també servei a altres comissions i projectes de la CELS, així com a les persones, col·lectius i entitats del barri que la necessitin. 

Així mateix, donem servei de **pads** o documents col·laboratius.


#### Objectius

* Crear un entorn on, un ampli conjunt del veïnat, tingui coneixements sobre les instal·lacions de l'equipament, sent capaços de mantenir les instal·lacions i replicar-les a altres equipaments. 
* Procurar que les eines, la infraestructura i les tasques que falten per fer es realitzin acompanyades de documentació i tallers de formació per assegurar que les instal·lacions TIC de La Lleialtat són conegudes per persones del barri i per persones d'altres equipaments municipals, fent així replicable i sostenible el projecte. 
* Organitzar els tallers corresponents per a la formació en les competències tècniques que calguin
* Formar grups de treball per a realitzar i implentar tasques
* Donar suport a l'equip tècnic de l'equipament i a les persones associades a la CELS.

#### Fins ara, a la Lleialtat, s'han dut a terme les següent accions d'infraestructura:

* [Núvol per a emmagatzemar i compartir arxius](https://tec.lleialtat.cat/infraestructura/serveis-administratius) (NextCloud)
* [Âgora per a la comunicació interna](https://tec.lleialtat.cat/infraestructura/comunicacio) (Discourse)
* Gestor de continguts web (Wordpress-Grav) [https://tec.lleialtat.cat](https://tec.lleialtat.cat)
* Sistemes operatius lliures als ordinadors de l'equipament (Ubuntu Mate)
* [Abastiment de wifi](https://tec.lleialtat.cat/howtos/wifi-access-points) (cortesía de Guifinet)
* Carpetes compartides
* Configuració de llocs de treball
* Instal·lació i configuració del tallafocs
* Implementació de la infraestructura de Xarxa Guifinet
* Muntatge de l'[aula informàtica](https://tec.lleialtat.cat/infraestructura/aules-lliures). Un lloc de formació digital per als veïns i veïnes

#### En desenvolupament

* Creació d'un software de gestió integral de l'equipament (tipus ERP) que ens permeti gestionar socis, reservar sales, facturar activitats, fer seguiment d'inventari, etc.