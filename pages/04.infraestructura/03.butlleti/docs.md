---
title: Butlletí
---

Moltes associacions i col·lectius utilitzen llistes de distribució o butlletins informatius (newsletters) com a canal de comunicació per fer difusió de les seves activitats a les persones interessades. La Lleialtat Santsenca també.

La diferència amb Facebook o Twitter (que s'han convertit també en canals de difusió) és que el correu electrònic funciona amb estàndards oberts que permeten la lliure circulació de missatges entre servidors de correu independents, amb el que per usar-lo, no ens obliga a ser munyides i censurades per grans corporacions*. Tot i així, les llistes de distribució no són llistes de correu on es pugui llegir i respondre, són unidireccionals i, per tant, estan pensades per gestionar l'enviament dels butlletins informatius a milers de subscriptores.

A més d'associacions i col·lectius, els butlletins els usen molt les empreses per enviar publicitat periòdica, i hi ha serveis comercials que ajuden a aquestes empreses a treure el màxim rendiment. Aquests serveis són empreses com Mailchimp, MailJet i SendGrid, que poden permetre's oferir algunes tarifes gratuïtes perquè treuren benefici de la informació que gestionen. Mailchimp i companyia, gestionen les dades de les persones subscriptores dels butlletins com la seva direcció de correu electrònic, el seu nom, IPs que delaten la seva localitat geogràfica, els seus interessos (butlletins als que pertanyen), els enllaços que obren, quan i quantes vegades, etc. Qualsevol que hagi fet servir Mailchimp per enviar butlletins ha usat el seu panell de control, des d'on es pot consultar tota aquesta informació gratuïtament.

En una assemblea de la CELS es van considerar les implicacions d'usar Mailchimp, les de convertir les sòcies de la Lleialtat en la mercaderia d'aquesta empresa aliena i es va valorar que la Lleialtat adquirís responsabilitat sobre les dades dels seus socis. Es va entendre que entregar conscientment les dades de les persones, els seus hàbits i gustos, era nociu per no dir una traició de confiança.

En assemblea es va decidir no usar Mailchimp o similars, i esperar fins que tinguéssim a punt una solució o alternativa lliure.

Des del Lleialtec (comisió tecnològica de la Lleialtat), vam estudiar diverses opcions i, passats uns mesos, vam posar en marxa una llista de distribució lliure i ètica amb  [Mailtrain](https://mailtrain.org/), un programari que respecta a les seves usuàries perquè les dades les gestionem nosaltres, el que ve a ser sobirania tecnològica.

Mentre preparàvem la solució, la CELS va tenir paciència, fent front a les trampes capitalistes tecnològiques de disponibilitat gratuïta i d'ús immediat. Qué grans! 

*Les grans corporacions proveïdores de correu com Google o Microsoft (Gmail i Hotmail) consideren cada cop més els servidors autogestionats, corporatius i minoritaris com a spam (correu brossa). És un motiu més per no recolzar-los i usar un compte respectuós amb la teva privacitat i llibertat, i la llibertat de les altres internautes.


![](mailtrain.png)

Muchas asociaciones y colectivos san listas de distribución o boletines informativos (newsletters) como canal de comunicación para hacer difusión de sus actividades a las personas interesadas.  La Lleialtat Santsenca también.

La diferencia con Facebook o Twitter (que se han convertido también en canales de difusión), es que el correo electrónico funciona con estándares abiertos que permiten la libre circulación de mensajes entre servidores de correo independientes, por lo que para usarlo, no nos obliga a ser ordeñadas y censuradas por grandes corporaciones*. Sin embargo, las listas de distribución no son listas de correo donde se puede leer y responder, son unidireccionales, y por lo tanto están pensadas para gestionar el envío de los boletines a miles de suscritoras.

Aparte de asociaciones y colectivos, los boletines los usan mucho las empresas para enviar publicidad periódica, y hay servicios comerciales que ayudan a estas empresas a sacar el máximo de jugo. Estos servicios son empresas como Mailchimp, MailJet y SendGrid, que pueden permitirse ofrecer algunas tarifas gratuitas porque sacan provecho de la información que gestionan. Mailchimp y compañía gestionan los datos de las personas suscritoras de los boletines como su dicrección de correo, su nombre, IPs que delatan su localidad geográfica, sus intereses (boletines a los que pertenecen), los links que abren, cuándo y cuántas veces, etc. Cualquiera que haya usado Mailchimp para enviar boletines ha usado su panel de control desde donde se puede consultar toda esta información, gratis.

En una asamblea de la CELS se consideraron las implicaciones de usar Mailchimp, las de convertir las socias de la Lleialtat en la mercancía de esta empresa ajena y se valoró que la Lleialtat adquierese responsabilidad sobre los datos de sus socios. Se entendió que entregar conscientemente los datos de las personas, sus hábitos y gustos, era nocivo por no decir una traición de confianza.

En asamblea se decidió no usar Mailchimp, o parecidos, y esperar hasta que tuviéramos lista una solución o alternativa libre. 

Desde el Lleialtec (comisión tecnológica de la Lleialtat), estudiamos distintas opciones y, pasados varios meses, pusimos en marcha una lista de distribución libre y ética con [Mailtrain](https://mailtrain.org/), un software que respeta a sus usuarios/as porque los datos los gestionamos nosotras, lo que viene a ser soberanía tecnológica.

Mientras preparamos la solución, la CELS ejerció la paciencia, haciendo frente a las trampas capitalistas tecnológicas de disponibilidad gratuita y de uso inmediato. ¡Qué grandes!

*Las grandes corporaciones proveidoras del correo como Google y Microsoft, (GMail y Hotmail), cada vez más están considerando los servidores autogestionados, cooperativos y minoritarios como spam. Es un motivo más para no apoyarlos y usar una cuenta respetuosa con tu privacidad y libertad, y con la libertad de las otras internautas.