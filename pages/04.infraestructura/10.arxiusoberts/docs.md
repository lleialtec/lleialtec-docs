---
title: 'Arxius Oberts'
media_order: documents.png
taxonomy:
    category:
        - docs
visible: true
---

**En català**

Moltes organitzacions tenen la necessitat de publicar documentació. Encara que només sigui per acomplir amb la Llei de Transparència, la publicació de fitxers ajuda a totes les persones interesades a tenir accés a la informació que té un caràcter públic. 

Des de Lleialtec, [facilitem l'accés a la informació](https://tec.lleialtat.cat/transparencia) que té utilitat per a les persones sòcies de la CELS, Coordinadora d'Entitats per la Lleialtat Santsenca. En aquest cas, a través d'una presentació endreçada i un potent sistema de cerca.

Al desenvolupar el programari hem considerat important, -pensant en la perspectiva de l'equip tècnic de l'espai-, que la publicació de documents no sigui "una tasca més", i hem buscat la manera d'integrar aquesta publicació en el día a día del treball administratiu de la Lleialtat.

![](documents.png)
[Codi font](https://gitlab.com/arxius-oberts) del programari

És per això que hem creat una carpeta al nostre [Núvol](https://tec.lleialtat.cat/infraestructura/nuvol) que se sincronitza automàticament amb el nostre [portal de documents públics](https://documents.lleialtat.cat/). Això vol dir que, si una persona participant de les comissions de la CELS vol publicar un document, només l'ha de desar a la subcarpeta anomenada 'Públic' des del seu compte d'usuària al Núvol.

També és important que la documentació tingui un context (de fet la Llei de Transparència ho indica), així que hem inclòs mecanismes molt fàcils d'aprendre i usar que resolen la contextualització. 

D'altra banda, hem pensat també en com facilitar aquesta infraestructura per a que pugui ser compartida amb altres entitats. Per això es va desenvolupar pensant en col·lectius que no tinguéssin necessàriament un núvol Nextcloud on desar la seva documentació.

Si formeu part d'una entitat i voleu tenir un portal de transparència amb programari lliure, us podem proporcionar aquest servei des de la Lleialtat Santsenca. Feu una ullada als nostres [serveis](https://serveis.tec.lleialtat.cat/)!

**En español**

Muchas organizaciones tienen necesidad de publicar documentación. Aunque solo sea para cumplir con la Ley de Transparencia, la publicación de ficheros ayuda a todas las personas interesadas a tener acceso a la información que tiene un carácter público.

Desde Lleialtec [facilitamos el acceso a la informació](https://tec.lleialtat.cat/transparencia) que tiene utilidad para las personas socias de la CELS, Coordinadora d'Entitats per la Lleialtat Santsenca. En este caso, a través de una presentación ordenada y un potente sistema de busqueda.

Al desarrollar el software hemos considerado importante, pensando desde la perspectiva del Equip tecnic del espacio, que la publicación de documentos no sea "una tarea más", y hemos buscado la manera de integrar esa publicación en el día a día del trabajo administrativo de la Lleialtat.

![](documents.png)
[Código fuente](https://gitlab.com/arxius-oberts) del software

Por eso, hemos creado una carpeta en nuestro [Núvol](https://tec.lleialtat.cat/infraestructura/nuvol) que se sincroniza automáticamente con nuestro [portal de documentos públicos](https://documents.lleialtat.cat/). Es significa que si una persona partipante de las comisiones de la CELS quiere publicar un documento, tan solo lo tinen que guardar en la sub-carpeta llamada 'Públic' desde su cuenta de usuaria en el Núvol. 

También es importante que la documentación tenga un contexto (de hecho la Ley de Transparencia lo indica), así pues hemos incluido mecanismos muy faciles de aprender y usar para tal fin.

Y ya puestos, hemos pensado también en como facilitar esta infraestructura para que sea compartida con otros entidades. Por eso, se desarrollo pensando en colectivos que no tuvieran necesariamente un nube Nextcloud donde guardar su documentación.

Si formáis parte de una entidad y queréis tener un portal de transparencia con software libre, os podemos proporcionar este servicio desde la Lleialtat Santsenca. Echad un ojo a nuestros [servicios](https://serveis.tec.lleialtat.cat/)!