---
title: Àgora
taxonomy:
    category:
        - docs
visible: true
---

[L'àgora de la Lleialtat Santsenca](https://agora.lleialtat.cat) és privada. Funciona per invitació i només hi tenen accés les sòcies de la Lleialtat. Allà hi endrecem els temes per categories, que són les comissions. En un inici, hi havia sis categories: economia, acció comunitària, lleialtec, programació i comunicació. I una «General» per temes que no encaixessin a la resta de categories. Darrerament, s'han creat tres noves categories: teatre, consum responsable i memòria històrica. A més, tenim una categoria «Assemblea» per facilitar el seguiment de la governança.    

A cada categoria es poden anar creant temes, i així la informació queda endreçada. També podem buscar informació a través del buscador, que pot filtrar per missatge, data, usuària, etc. Si fa dies que no visites l'àgora, el programa t'envia un correu electrònic amb un resum del que t'has perdut. També ens arriba un correu quan algú ens menciona a l'àgora o quan estem seguint un tema (podem seguir els temes que ens interessen i rebre notificacions quan es diuen coses en aquell tema). A més, des del mateix correu electrònic podem contestar i la nostra resposta queda enregistrada a l'àgora, amb el que la informació no es dispersa i és fàcil de trobar.

![](screenshot-agora.png)

Per a facilitar la posada en marxa d'aquesta i altres eines, dedicàvem quinze, vint minuts d'assemblea per a provar com funcionava: entendre les categories, crear o comentar temes (anomenats també fils), adjuntar un arxiu, etc. igualment, les sòcies que volen participar-hi més o tenen problemes tècnics, ens contacten directament i, amb molt de gust, els hi donem un cop de mà. A l'àgora, les persones que tenen compte poden convidar-ne d'altres. I així, mica en mica, hem arribat a 103 usuàries, unes més actives que les altres. 

Com a bones pràctiques, es recomana a les noves usuàries que omplin el perfil per tal que les altres sòcies de la CELS puguin saber qui són. Quan et registres a l'àgora, has de triar un nom d'usuària i posar un correu electrònic. El correu no es mostra i el nom pot prestar a confusió, així que omplint el perfil, es facilita saber qui és qui. A més, per tal d'evitar malentesos, també es recomana crear un usuari per persona i no per entitat. I és per això que tot i ser una cinquantena d'entitats, a l'àgora de la Lleialtat hi ha més de cent usuàries.    

El programa que possibilita l'àgora és [Discourse](https://www.discourse.org/), una eina que permet escriure en [Markdown](https://victorhckinthefreeworld.com/2019/09/23/tutorial-de-markdown-en-espanol/), un llenguatge molt senzill i pràctic per donar format a un text molt fàcilment: negretes, llistes, etc. A més, és convertible a HTML i avui dia, és un llenguatge molt utilitzat en documentació científica. 

El llenguatge fou creat al 2004 per John Gruber i [Aaron Swartz](https://vidcommons.org/videos/watch/9952af68-0b30-4ea0-81ac-a26444541331), que va treballar en la sintaxi. Per últim, destacar els orígens i l'adopció de l'eina a nivell global. El Discourse va ser creat per un equip de persones experimentades en els fòrums clàssics dels noranta i altres eines d'intercanvi de coneixement, com Stack Overflow, una eina que va néixer al 2008 per tal que les i els professionals poguessin intercanviar coneixements en qüestió de programació informàtica. Avui dia, innombrables projectes han adoptat Discourse com a eina participativa comunitària. 

