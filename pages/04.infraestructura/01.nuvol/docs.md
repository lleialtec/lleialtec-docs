---
title: Núvol
published: true
taxonomy:
    category:
        - docs
visible: true
---

Així com altres organitzacions, La Lleialtat té **requeriments administratius**. Les persones que participem de la CELS, -i que inclou l'equip de cinc persones que gestionen el dia a dia de l'equipament-, ho fem amb eines lliures i ètiques. I el núvol, és una eina clau per crear, emmagatzemar i compartir els arxius que es generen des de qualsevol de les comissions de la CELS. 

##### Documents compartits

Per tal de gestionar els documents que es generen en la gestió de l'equipament, fem servir l'eina [Nextcloud](https://nextcloud.com/), a la que anomenem **"el núvol"**. Nextcloud és un programari lliure que serveix per emmagatzemar arxius (documents de text, imatges, fulles de càlcul, etc.). A través del navegador, aquelles persones que formen part d'alguna comissió, poden consultar i [editar](https://nextcloud.com/collaboraonline/) els arxius des de qualsevol lloc. L'eina, ens permet compartir:

* carpetes i documents entre administratives i sòcies de La Lleialtat
* documents amb persones alienes a l'equipament (fent servir enllaços públics)
* documentació de caràcter públic amb el veïnat [a través de la web](https://www.lleialtat.cat/documents/).


#### Entorn sincronitzat

Poder accedir al núvol des de qualsevol lloc és molt útil, però depenem de la connectivitat d'Internet. Un cop a La Lleialtat, és encara més pràctic treballar amb programari local (LibreOffice, Thunderbird, Scribus, etc.). 

[Hem sincronitzat els documents al núvol amb els llocs de treball del despatx](https://nextcloud.com/install/#install-clients), creant així un entorn eficient que s'espera de qualsevol oficina del segle XXI ;) 


#### Calendaris i agenda

A més de permetre emmagatzemar i compartir arxius, Nextcloud inclou altres eines, com una [app de calendari](https://apps.nextcloud.com/apps/calendar) que, a més de poder-se usar via web, sincronitza amb el gestor de correu [Thunderbird](https://www.mozilla.org/es-ES/thunderbird/features/). I així, es facilita l'ús del calendari entre els diversos llocs de treball. Es poden crear calendaris i agendes segons la necessitat, i es poden **compartir amb altres usuàries i/o grups d'usuàries** de Nextcloud. 

L'agenda d'activitats es publica via [una API](http://git.entitatstransparents.org/?p=nextcloud-fullcalendar;a=summary) per així poder ser visualitzada des de [la web de La Lleialtat](https://www.lleialtat.cat/agenda/) que també està creada amb programari lliure, concretament Wordpress. 


#### Correu electrònic

Les persones que treballen a La Lleialtat fan servir el gestor de correu Thunderbird als seus ordinadors (IMAP). Tot i no ser al despatx, però, poden igualment accedir al seu correu electrònic a través del núvol. Per fer-ho, hem activat l'aplicació de correu electrònic [Rainloop](https://apps.nextcloud.com/apps/rainloop) de Nextcloud.


#### Pads

Oferim servei de pads o documents col·laboratius amb [etherpad](https://pad.lleialtat.cat/).

