---
title: 'Aules lliures'
taxonomy:
    category:
        - docs
visible: true
---

La Lleialtat Sansenca dispone de un Aula Informática cortesía de [La Troca](https://latrocasants.org), Escola Comunitària de Formació Permanent als barris de Sants, que ha financiado su [instalación y mantenimiento](https://tec.lleialtat.cat/howtos/aula-informatica) durante el tiempo que dure el proyecto piloto.

El aula tiene doce PCs recuperados por el proyecto [ereuse](https://www.ereuse.org/), cada un de ellos con el sistema operativo Linux Mint, elegido por su escritio de fácil uso para prinicipiantes.

El mantenimiento de los PCs se lleva a cabo con [Fog Project](https://fogproject.org/), una solución libre para clonar y gestionar ordenadores.

La clonación de sistemas operativos y su despiegue automatizado nos permite asegurar que los PCs se encuentren en condiciones óptimas, clase trás clase.

Consulta la [documentación técnica](https://tec.lleialtat.cat/howtos/aula-informatica).
