---
title: 'ERP del Cafè'
media_order: 'TVP Capirota.jpg,POS Capirota.png,Backoffice Capirota.png'
taxonomy:
    category:
        - docs
visible: true
---

El Cafè de la Lleialtat Santsenca està gestionat per la cooperativa [La Capirota](http://cafe.lleialtat.cat). A mitjans del 2018 es va posar en marxa un **procés participatiu** per establir les bases del concurs, adjudicar la gestió del Cafè i acompanyar el nou projecte en la integració a l'ecosistema de la Lleialtat.

Entre octubre i desembre de 2018 es va començar a planificar i desenvolupar el programari de gestió, i La Capirota es va començar a integrar a l'equipament mentre gestionava la paperassa cooperativa i adequava l'espai. Al març del 2019, i en el marc de la setmana de la sostenibilitat, s'inaugurava oficialment el servei de cafeteria. 

Un dels requisits que es demanava a les [bases del concurs per a la gestió del Cafè de la Lleialtat Santsenca](http://documents.lleialtat.cat/Documents%20administratius/Bases%20Concurs%20Caf%C3%A8%20Lleialtat%20Santsenca.pdf), anava "Lligat amb els objectius i valors de la Lleialtat" i especificava "fer ús de programari lliure". Com entenem que encara resulta exòtic anar reclamant bits lliures a tort i a dret, també comprenem que les cooperatistes de La Capirota necessitin ajut en aquests temes. És per això que, a més de la condició es proposava acompanyament, "Des de la CELS garantim l’acompanyament si cal una adaptació en aquest àmbit".

En aquest sentit, no només hem acompanyat sinó que hem desenvolupat infraestructura pròpia: un **ERP de gestió fet a mida** amb tecnologia [Dolibarr](https://www.dolibarr.org/). L'hem triat per la seva àmplia trajectòria professional i documentació, per ser modulable, per conèixer experts en l'eina, però sobretot, per ser lliure, ètica, sostenible i fàcilment replicable. Avui dia només el Cafè de la Lleialtat fa servir aquest programari, però el nostre desig és fer-lo conèixer a iniciatives amb valors, com altres espais d'acció comunitària o als projectes de la [XAREC](https://www.xarec.coop/). 

En quant al desenvolupament, destaquem les bones sinergies que es van establir entre membres de **Lleialtec, Lliuretic i Capirota**, que conjuntament vam dissenyar l'eina per cobrir necessitats reals, a mida: facturació, control d'estocs, etc. Aprofitem el text per agrair de tot cor a la Susi i la Sílvia la paciència i bona predisposició respecte als aprenentatges que hem anat compartint tots aquests mesos. Quan s'entén que totes les lluites són una, tot és més fàcil! :) 

Destacar que la gran dificultat tècnica no va venir del programari sinó del maquinari: per trobar una **TPV compatible amb Gnu/Linux** vam tenir que fer venir el trasto de Shenzhen, el Silicon Valley del maquinari. La cosa ha portat molts maldecaps però hem fet el que bonament hem pogut. Conclusió: de moment no tenim maquinari de proximitat que faci de TPV i funcioni amb programari lliure.

![](TVP%20Capirota.jpg)

**POS**
![](POS%20Capirota.png)

**Backoffice**
![](Backoffice%20Capirota.png)



[Documentació de l'eina](https://tec.lleialtat.cat/howtos/erp-del-cafe) per a la seva replicabilitat.

