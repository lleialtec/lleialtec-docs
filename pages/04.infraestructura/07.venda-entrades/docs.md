---
title: 'Venda d''entrades'
published: true
taxonomy:
    category:
        - docs
visible: true
---

A la Lleialtat s'hi fan molts espectacles i, alguns, requereixen la compra d'entrades. Fins ara, però, només es podia reservar online i fer el pagament presencialment. 

A una assemblea, la comissió de tecnologia va rebre la petició de sistematitzar la cosa, tan per alleugerir la feina del personal tècnic com per donar l'opció a les assistents a l'espectacle a fer la compra per Internet. I és per això que hem implementat un sistema de venda d'entrades online. 

El programa informàtic que gestiona la compra-venda d'entrades es diu [Pretix](https://pretix.eu/about/en/), i és programari lliure, és a dir, tecnologia respectuosa amb les seves usuàries. 

Comentar que hem tingut una grata sorpresa: amb aquest programa, no només podem vendre entrades des de la Lleialtat! El sistema de gestió permet crear un servei de venda d'entrades, amb el que es podria **compartir infraestructura** i abastir a altres projectes. 

Imaginem que hi ha un altra entitat que necessita vendre entrades online. Doncs no cal que s'instal·li el seu propi Pretix i tota la pesca... Només caldria habilitar usuaris des del Pretix que tenim instal·lat a la Lleialtat, oferint un sistema de venda d'entrades que es pot configurar amb domini propi (si el teniu). 

**Cada entitat tindria el seu usuari** des d'on vendre les entrades, però la infraestructura seria compartida entre totes les entitats que utilitzessin el servei. Així que no només seria rentable, sinó que a més fariem xarxa i compartiríem recursos. 

D'altra banda, el sistema de venda d'entrades serà integrat a l'ERP o programa de gestió integral de l'equipament que estem desenvolupant en el marc del projecte [Equipaments Lliures](https://tec.lleialtat.cat/equipaments-lliures). 

Us interessa el servei de venda d'entrades online? Contacteu-nos a info@tec.lleialtat.cat

Consulta la [documentación técnica](https://tec.lleialtat.cat/howtos/venda-dentrades).