---
title: Replicabilitat
taxonomy:
    category:
        - docs
visible: true
---

Una de les pedres angulars del coneixement és la seva transmissió i millora, i és per això que aquest projecte es deu a la seva replicabilitat. Creiem que el model d'organització i gestió de la informació en espais comunitaris, -i més si són municipals-, ha de ser ètic per defecte i això, avui dia, només es pot aconseguir fent ús de programari lliure.

Amb aquest pla, doncs, proposem un model de bones pràctiques tecnològiques que altres espais poden replicar per tal de **garantir l'ètica tecnològica dels seus entorns d'intercanvi i aprenentatge, i les llibertats digitals de les persones que hi participen**. 

Per fer efectiva aquesta replicabilitat, anirem explicant i documentant les accions portades a terme a La Lleialtat a l'apartat de [**HowTo's**](https://tec.lleialtat.cat/howtos) per tal que qualsevol persona o entitat pugui implementar les infraestructures que anem desenvolupant.
