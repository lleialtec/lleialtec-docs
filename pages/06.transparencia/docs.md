---
title: Transparència
taxonomy:
    category:
        - docs
visible: true
---

La cura en la transparència sempre ha estat una màxima del Lleialtec. El dia a dia de la coordinadora és un treball continu d'equip que bascula entre l'ecosistema presencial i el digital, i **on la transparència és confiança**. Si bé hi ha persones alliberades, la majoria són voluntàries; i la diversitat dels col·lectius és tan gran que un dels reptes de tot espai comunitari és l'harmonia del propi ecosistema. 

És per això que, des del Lleialtec, creiem fermament que, garantint bones pràctiques de transparència, predisposem l'ecosistema de la CELS a organitzar-se de forma orgànica i harmònica en medis físics i virtuals que es complementen.  

Per tal visibilitzar altres iniciatives relacionades amb la transparència, en una assemblea es va consensuar donar suport a la campanya de la Free Software Foundation [Public money, public code](https://publiccode.eu/ca/) que demana a les Institucions alliberar el codi que es crea amb diners públics. 

Free Software, Free Society!

#### Transparència interna

Volem que les sòcies i administratives tinguin accés a la documentació i puguin participar del funcionament de La Lleialtat. I per això, per tal de contribuir a una comunitat més ben organitzada, busquem crear un **entorn inclusiu**, on no hi ha grups de persones amb més informació que d'altres. 

Les sòcies i administratives tenen accés a l'[**àgora**](https://tec.lleialtat.cat/infraestructura/agora), on s'exposen i es debaten temes relacionats amb La Lleialtat. L'àgora s'estructura per comissions, i cada comissió pot anar penjant fils de diverses temàtiques. A més, les persones que participen d'alguna comissió, tenen accés a la documentació interna al [**núvol**](https://tec.lleialtat.cat/infraestructura/nuvol) de La Lleialtat amb permisos de lectura, i de lectura i escriptura a les carpetes de la seva comissió. 


#### Transparència amb el veïnat

Des del Lleialtec, volem ajudar a La Lleialtat a ser una entitat transparent, més enllà del que la llei requereix. És per això, que implementem mecanismes que faciliten, de manera integrada, **la publicació de l'activitat diària** de l'equipament a la web.

Amb l'objectiu de mantenir actualitzades les dades publicades (i que es requereix per llei) busquem solucions que evitin tasques afegides i que suposen més treball. I per això, integrem la gestió del centre amb la publicació de les dades a la web. 

Un exemple, n'és la **sincronització de carpetes de documents**. Això, permet que el personal administratiu pugui gestionar la documentació de caràcter públic igual que qualsevol altra documentació. Es disposa d'una **carpeta "Públic"** (compartida entre administradores i sòcies) on tots els documents que s'hi posen [**es publiquen _automàgicament_**](https://documents.lleialtat.cat/) a la web de l'equipament. A més, incorpora un potent motor de cerca que facilita trobar la informació que necessitem.
